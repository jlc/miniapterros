# Projet Mini-Apterros
  
Ce dépôt contient le code pour entraîner un contrôleur (agent) sur plusieurs environments (_CartPole_, _Balancing robot_, Mini-Apterros...) en utilisant l'apprentissage par renforcement de réseaux de neurones (_Deeep Reinforcement Learning_ : _DRL_).

- Pour traiter l'__apprentissage par renforcement__, nous utilisons les programmes et la documentation du site  _stable-baselines3_ (<https://github.com/DLR-RM/stable-baselines3>).

- Pour la simulations des environnement contrôlés par DRL, nous utilisons au choix :

  - _OpenAI/Gym_ (anciennement <https://gym.openai.com/> puis <https://www.gymlibrary.dev/>) pour les simulations simples,
  - _Coppelia Sim_ (<https://www.coppeliarobotics.com/>) pour les simulations plus réalistes, avec un rendu graphique possible.

- Pour l'atelier de développement (_Integrated Developement Environment : IDE_), nous conseillons l'IDE __Visual Studio Code__ (_aka_ VSCode):

  - multi-plateforme : Windows, Mac & GNU/Linux,
  - code source libre distribué sous licence  MIT, disposant d'une communauté active de contributeurs,
  - faible empreinte mémoire et CPU,
  - compatible avec les _Environnements Virtuels Python_ et le système _git_ pour le stockage/versionnage du code source sur les plateformes Github ou GitLab,
  - compatible avec les fichiers _notebook jupyter*, avec rendu graphique,
  - doté d'un mécanisme d'extensions puissant simple à utiliser.

  Il existe d'autres IDE Python qui proposent des fonctionnalités analogues  (pycharm, atom...).
Le choix de VSCode est motivé par sa relative simplicité d'utilisation et sa faible empreinte mémoire.

## Racine du projet

La racine du projet (_root_) désigne le répertoire contenant ce README.

C'est le répertoire de plus haut niveau du projet : normalement c'est le répertoire nommé `miniapterros` obtenu par clonage du dépôt GitLab <https://gitlab.com/jlc/miniapterros.git>.

## Créer de l'environnement de travail

### 1) Environnement Virtuel Python (EVP) nommé __pyml__

- Télécharge et installe la dernière version de __miniconda3__ pour ton OS : <https://docs.conda.io/en/latest/miniconda.html>

Avec un terminal positionné dans le dossier de `Téléchargements` (_downloads_) tape la commande : 
`bash Miniconda3-latest-Linux-x86_64.sh`.<br>
Après avoir accepté la licence et le dossier d'installation `/home/<user>/miniconda3`, réponds `yes` à la question _Do you wish the installer to initialize Miniconda3 by running conda init?_ <br>
À la fin de l'installation, tape la commande `conda config --set auto_activate_base false` puis lance une nouvelle fenêtre terminal

  __Attention__ : le chemin complet du répertoire d'installation `miniconda3`  [ `C:\..chemin..\miniconda3` pour Wndows, ou `/..chemin../miniconda3` pour Mac & Linux ] doit être choisi de façon à ne comporter aucun caractère accentué ni espace !!!

- Crée l'EVP __pyml__ en tapant dans une console (un terminal Mac/Linux ou une fenêtre "Anaconda prompt" Windows) :

    `conda create -n pyml python=3.8`

  => Attention à bien utiliser le nom __pyml__ : la suite du tutoriel, les fichiers de configurations et les exemples fournis, sont basés sur ce nom.

### 2) Activer l'EVP __pyml__

Deux situations se rencontrent :

- Dans une console => on active l'EVP en tapant la commande :
  `conda activate pyml`

- Avec VSCode :
  
  Installe les extensions `Python` et `Jupyter` depuis la section `Extensions` à gauche dans VScode
  
  => on utilise le raccourci clavier `SHIFT+CTRL+P` puis on continue en tapant P`ython: Select Interp` :

  ![bbbb](docs/Images/VSC_selectEVP_3.png)

  puis __Find...__ pour sélectionner l'__interpréteur Python__ dans l'arborescence de l'EVP (par exemple pour Linux : `/..chemin../miniconda3/envs/pyml/bin/python3`)

  ![bbbb](docs/Images/VSC_selectEVP_2.png)

  Idem pour le chemin de l'__Interpréteur IPython pour le serveur jupyter__.

### 3) Compléter l'installation des modules Python

- Le plus simple est de taper les commandes dans la console __avec l'EVP pyml activé :

```bash
conda install numpy scipy matplotlib jupyter ffmpeg=4.2
pip install gym==0.21.0 box2d==2.3.10 pyglet==1.5.27 
pip install opencv-python==4.3.0.36
pip install torch==1.12.1 pybullet==3.2.5 stable-baselines3[extra]==1.8.0 
pip install pyyaml pyqtgraph qdarkstyle pyserial
```

### 4) Compléter l'installation du module _gym_

Le but est d'installer des liens dans l'arborescence du module __gym__ vers des fichiers Python du dossier `miniapterros` de façon à pouvoir utiliser les environnements Gym _Continuous CartPole_ et _MiniApterros_.

Avec le terminal intégré de VSCode, va dans le dosssier `<project_root>/docs` et lance le shell script `config_gym_miniapterros.sh` :

```bash
bash config_gym_miniapterros.sh
...
```

### 5) Tester le bon fonctionnement

- Lance VSCode et ouvre le __dossier__ du projet `miniapterros`.
- Charge les programmes Python du dossier `ai/src/simple_test` lance les avec `CTRL+F5` : un réseau PPO est entraîné à garder le carpole de l'environnement Gym à rester à l'équilibre...
- Les vidéos des programmes de test du réseau entraîné sont sauvegardées dans les répertoire `ai/out/quick_tests/test_ppo...`.

## Installer le simulateur CoppeliaSim

Pour installer CoppeliaSim (Linux):

- Va sur <https://www.coppeliarobotics.com/downloads> et télécharge la version Coppelia Sim __EDU__ pour Ubuntu 20.
- Dézippe le fichier téléchargé et extrait le dossier `CoppeliaSim_Edu_V4_2_0_Ubuntu20_04` à la racine du projet.
- si nécessaire (en cas d'utilisation d'une version différente), met à jour le chemin  `COPSIM_DIR` dans le fichier `copsim_env/constants.py`.

## Tester l'API Coppelia SIM - Python

- Ouvre les notebooks du dossier `coppelia_sim_api/GettingStarted_with_API`. Ils contiennent :
  - le lancement automatique du simulateur CoppeliaSIm (uniquement sous Linux)
  - la mise en oeuvre des fonctions de l'API Python.
L'exécution de ces notebook ne doit pas générer d'erreur.

## Utilisation des scripts Python

- les scripts d'entraînement et tests doivent être executés à partir de la racine du projet :
  - VSCode : vérifie que le fichier `<racine_projet>/.vscode/launch.json` contient :

  ```json
  {
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python : Fichier actuel",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "env": {"PYTHONPATH": "${cwd}" }
        }
    ]
  }
  ```

## Utilisation des notebooks

Les notebooks Jupyter peuvent être utilisés de 2 façons :

- Avec l'EDI Jupyter :
  - si besoin, installe `jupyter` en tapant dans le terminal (ou console "Anaconda prompt") la commande : `conda install jupyter`.

  Pour lancer jupyte, tape `jupyter notebook` dans le terminal (ou console "Anaconda prompt"), __avec l'EVP `pyml` activé__.

- avec VSCode : l'utilisation des notebooks est intégré à l'atelier.

Nouveau notebook : partir d'une copie du notebook `Template.ipynb` qui permet :

- de faire exécuter le notebook à la racine du projet (évite les problèmes d'import relatif des modules)
- de prendre en compte automatiquement toute modification des environnements ou des sources python du projet sans avoir à re-importer les packages et relancer le kernel.

## Configuration de l'EDI Viual Studio Code

- `Workbench`
  - `Apparence`
    - `Tree:indent` : 12
- `Features`
  - `terminal`
    - `External: Windows Exe`: C:\Windows\System32\cmd.exe
    - `Integrated: Inherit Env`: Ccoché
- `Extensions`
  - `Jupyter`
    - `Always trust Notebook`: coché


### Si erreur Qt plugin pour affichage des courbes :
 - ubuntu 20.04 : pip install opencv-python==4.3.0.36
