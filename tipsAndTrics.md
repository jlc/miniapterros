killall -I coppeliasim


python ai/src/run/train_model_simul.py --simulator copsim --vehicule balancingrobot

python ai/src/run/test_model_simul.py --simulator pybullet --configloc ai/models/balancingrobot/BalancingRobotEnv_PyBullet_PPO_22-06-11_18-27-13/ --displayplot --alwaystest

python ai/src/run/test_model_simul.py --simulator copsim --configloc ai/models/balancingrobot/BalancingRobotEnv_CopSim_PPO_22-06-12_09-38-36/ --displayplot --alwaystest

