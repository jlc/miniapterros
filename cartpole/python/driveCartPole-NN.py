# To be interpreted by Python3
import sys, os
import matplotlib.pyplot as plt
import numpy as np
from time import sleep, time
from random import randint
import math as m
from serial import Serial
from stable_baselines3 import PPO

# Connection to serial port:
# Serual port name  (the name is in the title bar of the monitor window):
#   Windows : "COM3" or "COM4" or ...
#   Linux   : "/dev/ttyACM0" (or see the monitor window title bar)
#   Mac OS X: see the monitor window title bar
#
# Parameters: # of data bits -> 8
#             # od STOP bit  -> 1
#             Parity         -> None
#             Speed          -> 9600, 14400, 19200, 28800, 38400,
#                                         57600, or 115200

# load a trained Reinforcment Learning and Proximal Policy Optimisation (PPO) model
model_path = "../../ai/models/CartPoleEnv_CopSim_PPO_21-04-01_17-49-30"
model_name = "rl_model_100000_steps.zip"
model_path_name= os.path.join(model_path, model_name)

model = PPO.load(model_path_name)

sys.exit()

serial_device = "/dev/ttyACM0"
try:
    serialPort   = Serial(serial_device, baudrate=500000, timeout=None)
except:
    serial_device = "/dev/ttyACM1"
    serialPort   = Serial(serial_device, baudrate=500000, timeout=None)
    
print(serialPort) # just to see...

# Open serial port:
sleep(0.5)
if not serialPort.is_open :
    serialPort.open()
    sleep(0.5)
sleep(0.5)

# wait for Arduino ready:
print("Waiting for ARDUINO ....", end="")
data = b""
while "Arduino OK" not in data.decode().strip() :
    data = serialPort.readline()
    print(data.decode().strip())
print(" OK !", flush=True)


data, M = b"", []
stateEnv =np.zeros((1,4))
while True:
    data = serialPort.readline()   # read serial port is a blocking operation...
    data = data.decode().strip()
    x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
    stateEnv[0,:] = [x_pos, x_lin_veloc, theta_, ang_veloc]

    step, M = 0, []
    while not done:
        t0 = time()
        # compute the PPO action:
        action, _ = model.predict(stateEnv, deterministic=True)

        # write action for Arduino CartPole environment:
        serialPort.write(bytes(action, encoding="ascii"))
        
        # now, wait for Arduino CartPole environment response:    
        data = serialPort.readline()   # read a line of data
        data = data.decode().strip()
        #print(action, data)
        x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
        stateEnv[0,:] = [x_pos, x_lin_veloc, theta_, ang_veloc]
        step += 1        
        M.append([step, x_pos, x_lin_veloc, theta_, ang_veloc])
        #print("T boucle: {:.3f}".format((time()-t0)*1000))
        
    M = []
    print("episode ended after {:03d} steps\n".format(step))
    
    
    
print(" OK !", flush=True)    

                                         
plotWanted = True # you can change this : True or False
    
M = np.array(M)
if len(M):
    np.set_printoptions(suppress=True)
    plt.figure(figsize=(12,6))
    plt.subplot(121)
    plt.plot(M[:,0],M[:,1],'.-b', label='MPU [°]')
    plt.xlabel('step')
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(M[:,0], M[:,2],'.-m', label="CODER [°]")
    plt.legend(loc='lower right')
    plt.show()

serialPort.flushOutput()
sleep(0.5)

serialPort.close()          # close serial port
