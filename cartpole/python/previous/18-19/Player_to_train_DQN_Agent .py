# Programme à exécuter avec Python3

import numpy as np
import os, sys
from time import strftime, sleep, time
import matplotlib.pyplot as plt
from random import randint
import math as m

#
# Use this programm to play cartpole learning with the real cartpole and a (partially)
# alredy trained  network (the DQN_Agent).
#

class Player():
    
    def __init__(self, env, agent):
        self.env        = env    # the environnement used to play
        self.agent      = agent  # the agent to be used
        self.batch_size = None   # size of the random samples used to train the network
        self.n_episode  = None   # the number of episodes to run
        self.max_trial  = None   # the max number of trials to try per episode
        
        self.epsilons    = []    # list of the values of epsilon for all the episodes
        self.times       = []    # list of the number of successfull steps for each episode


    def run(self, batch_size, n_epochs, n_episode=1000,
            max_trial=None, break_on_solved=True, weightFile=''):
        
        self.batch_size = batch_size
        self.n_epochs   = n_epochs
        self.n_episode  = n_episode
        self.weightFile = weightFile.replace(".h5","").replace(".hf5","")

        if max_trial is not None:
            self.env._max_episode_steps = max_trial
            self.max_trial  = max_trial

        self.epsilons, self.times = [],[]

        mess = "episode: {}/{} finished after {} time steps"

        # check wether ./AgentSaveWeights directory exists ?
        if not os.path.exists('AgentSaveWeights'): os.mkdir('AgentSaveWeights')


        from serial import Serial

        # Connexion au port série :
        # Nom du port série  (le nom est dans la barre de titre du 'Moniteur') :
        #   Sous Windows : "COM3" ou "COM4"....
        #   Sous Linux   : "/dev/ttyACM0" (ou voir barre de titre fenêtre Moniteur)
        #   Sous Mac OS X: voir barre de titre fenêtre Moniteur
        #
        # Paramètres : Nbre de bits de données -> 8
        #              Nbre de bit STOP        -> 1
        #              Parité                  -> Sans (None)
        #              Vitesses                -> 9600, 14400, 19200, 28800, 38400,
        #                                         57600, or 115200

        portSerie = "/dev/ttyACM1"
        try:
            serialPort   = Serial(portSerie, baudrate=250000, timeout=None)
        except:
            portSerie = "/dev/ttyACM0"
            serialPort   = Serial(portSerie, baudrate=250000, timeout=None)

        print(serialPort) # juste pour voir...

        # liste de lecture des données:

        sleep(0.5)
        if not serialPort.is_open :
            serialPort.open()
            sleep(0.5)
        sleep(0.5)

        # wait for Arduino ready:
        print("Waiting for ARDUINO ....", end="")
        data = b""
        while "Arduino OK" not in data.decode().strip() :
            data = serialPort.readline()
            print(data.decode().strip())
        print(" OK !", flush=True)                
            
        for e in range(n_episode):
            # read initial cartPole state: this is an equivalent step to the reset()
            # used with Gym environment.
            data = serialPort.readline()   # read serial port is a blocking operation...
            data = data.decode().strip()
            print(data)
            x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
            stateEnv = np.array([x_pos, x_lin_veloc, theta_, ang_veloc]).reshape(1,4) # make a matrix with one line, 4 columns
          
            step = 0
            
            done= False

            while not done :

                # Decide wich action take:
                act_values = self.agent.model.predict(stateEnv)
                action = np.argmax(act_values[0])
                
                # write action for Arduino CartPole environment:
                serialPort.write(bytes(str(action), encoding="ascii"))
                # now, wait for Arduino CartPole environment response:    
                data = serialPort.readline()   # read a line of data
                data = data.decode().strip()
                print(data)
                x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
                next_stateEnv = np.array([x_pos, x_lin_veloc, theta_, ang_veloc]).reshape(1,4)

                # Reward is 1 for every frame the pole survived -->"while not done"=+1
                # Remember the previous state, action, reward, and done
                if e >=1 :
                    self.agent.remember(stateEnv, action, 1, next_stateEnv, done)
                stateEnv = next_stateEnv

                # save weight file every 10 steps:
                if step % 10 == 0:  
                    uniq_name = self.weightFile+"_"
                    uniq_name += "_episod-{}_step-{}.hf5".format(e, step)
                    self.agent.save(uniq_name)
                    print("network weights saved as <{}>".format(uniq_name))

                step += 1

            print("episode: {:4d} -- score: {:3d} -- memory length: {}".format(e, step, len(self.agent.memory)))
            # store data values for plotting...
            self.times.append(step)
            self.epsilons.append(self.agent.epsilon)

            # let the neural network learn something from this run and save the current agent :
            if len(self.agent.memory) > batch_size :  self.agent.replay(batch_size, epochs=n_epochs)


                
    def plot(self):
        import matplotlib.pyplot as plt
        from matplotlib import rc
        rc('mathtext', default='regular')

        fig = plt.figure(figsize=(9,6))
        plt.title("[max_trial:{}, batch_size:{}, epochs:{}, activation:'{}', ]"\
                  .format(self.max_trial, self.batch_size, self.n_epochs, self.agent.activation))
        ax1 = fig.add_subplot(111)

        plt1 = ax1.plot(self.times,'-b',label='instant.  score', linewidth=.7)
        ax1.set_ylim(0, self.max_trial*1.2)
        ax1.set_xlim(0,self.n_episode)
        ax1.grid()
        ax1.set_ylabel('score for each episode', color='b')
        ax1.set_xlabel('episode')
        xmin,xmax = plt.xlim()
        deltax = xmax - xmin

        ax2 = ax1.twinx()
        ax2.set_ylim(0, self.max_trial*1.2)
        ax1.set_xlim(0,self.n_episode)

        labs = [p.get_label() for p in plt1]
        ax1.legend(plt1, labs, loc='upper left', fontsize=10)
        # check wether ./PNG directory exists ?
        if not os.path.exists("PNG"): os.mkdir("PNG")
        uniq_name = "./PNG/"+self.weightFile+"_"
        uniq_name += strftime("%Y_%m_%d_%H_%M")
        plt.savefig(uniq_name+".png")
        plt.show()


if __name__ == "__main__":

    from time import time
    import gym
    import numpy as np
    from time import sleep
    from DQN_Agent import DQN_Agent

    env_dims = (4,2)

    # transfer learning:
    agent = DQN_Agent(env_dims, 'tanh')
    weightFile = "/AgentSaveWeights/"
    weightFile += "cartpole-dqn_max_200-batch_16-epochs_5-acti_tanh-2018-07-13_18-08-25.hf5"
    agent.load(weightFile)
    
    player = Player(env_dims, agent)
    player.run(batch_size=32, n_epochs=5, n_episode=200, weightFile=weightFile)
