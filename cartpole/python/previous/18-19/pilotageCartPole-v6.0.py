# Programme à exécuter avec Python3
import sys
import matplotlib.pyplot as plt
import numpy as np
from time import sleep, time
from random import randint
import math as m
from serial import Serial

# Connexion au port série :
# Nom du port série  (le nom est dans la barre de titre du 'Moniteur') :
#   Sous Windows : "COM3" ou "COM4"....
#   Sous Linux   : "/dev/ttyACM0" (ou voir barre de titre fenêtre Moniteur)
#   Sous Mac OS X: voir barre de titre fenêtre Moniteur
#
# Paramètres : Nbre de bits de données -> 8
#              Nbre de bit STOP        -> 1
#              Parité                  -> Sans (None)
#              Vitesses                -> 9600, 14400, 19200, 28800, 38400,
#                                         57600, or 115200

portSerie = "/dev/ttyACM1"
try:
    serialPort   = Serial(portSerie, baudrate=500000, timeout=None)
except:
    portSerie = "/dev/ttyACM0"
    serialPort   = Serial(portSerie, baudrate=500000, timeout=None)
    
print(serialPort) # juste pour voir...


# le réseau de neuronesentrainné :
import gym
from DQN_Agent import DQN_Agent

weightFile = "../Calculs/CartPole/V-REP/"
#weightFile += "AgentSaveWeights/DQN-max_200-batch_32-epochs_10-lay_24x48-activ_relu-rate_0.01-seed_7-2019_01_29_08_31.h5"
weightFile += "AgentSaveWeights/DQN-max_200-batch_32-epochs_10-lay_24x48-activ_tanh-rate_0.01-seed_7-2019_01_24_13_36.h5"
env_dims = (4,2)
agent    = DQN_Agent(env_dims, 'relu')
agent.load(weightFile)


# Open serial port:
sleep(0.5)
if not serialPort.is_open :
    serialPort.open()
    sleep(0.5)
sleep(0.5)

# wait for Arduino ready:
print("Waiting for ARDUINO ....", end="")
data = b""
while "Arduino OK" not in data.decode().strip() :
    data = serialPort.readline()
    print(data.decode().strip())
print(" OK !", flush=True)    

plotWanted = False # you can change this : True or False
stateEnv =np.zeros((1,4))

# run some episodes:
for episode in range(10):
        
    # read initial cartPole state: this is an equivalent step to the reset()
    # used with Gym environment.
    data = serialPort.readline()   # read serial port is a blocking operation...
    data = data.decode().strip()
    print(data)
    x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
    stateEnv[0,:] = [x_pos, x_lin_veloc, theta_, ang_veloc]

    step, M = 0, []
    while not done:
        t0 = time()
        # compute the DQN action:
        act_values = agent.model.predict(stateEnv)
        action = str(np.argmax(act_values[0]))
        #print("fin DQN: {:.3f}".format((time()-t0)*1000))
        # write action for Arduino CartPole environment:
        serialPort.write(bytes(action, encoding="ascii"))        
        # now, wait for Arduino CartPole environment response:    
        data = serialPort.readline()   # read a line of data
        data = data.decode().strip()
        #print(action, data)
        x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
        stateEnv[0,:] = [x_pos, x_lin_veloc, theta_, ang_veloc]
        step += 1        
        M.append([step, x_pos, x_lin_veloc, theta_, ang_veloc])
        #print("T boucle: {:.3f}".format((time()-t0)*1000))
    print(M)
    M = []
    print("episode [{:02d}] ended after {:03d} steps\n".format(episode,step))

    if (not plotWanted): continue
    
    M = np.array(M)
    if len(M):
        np.set_printoptions(suppress=True)
        #print("time [ms]       y[mm]     Vy [m/s]  theta [°]   theta' [°/s]")
        #print(M)

        plt.figure(figsize=(12,6))

        plt.subplot(121)
        plt.plot(M[:,0],M[:,1],'.-b', label='x [m]')
        plt.ylim(-0.25, 0.25)
        plt.xlabel('step')
        plt.legend(loc='upper left')
        plt.twinx()
        plt.plot(M[:,0],100*M[:,2],'.-m', label="x' [cm/s]")
        plt.ylim(-21, 21)
        plt.legend(loc='lower right')
                 
        plt.subplot(122)
        plt.plot(M[:,0],np.degrees(M[:,3]),'.-r',label=r'$\theta$'+' [°]')
        plt.ylim(-13,13)
        plt.xlabel('step')
        plt.legend(loc='upper left')
        plt.twinx()
        plt.ylim(-180,180)
        plt.plot(M[:,0],np.degrees(M[:,4]),'.-g',label=r"$\theta'$"+ ' [°/s]')
        plt.legend(loc='lower right');
        plt.show()

serialPort.flushOutput()
sleep(0.5)


serialPort.close()          # close serial port
