# adapted from n1try :
# https://gist.github.com/n1try/2a6722407117e4d668921fce53845432#file-dqn_cartpole-py
#
import math, random, gym
import numpy as np
from collections import deque         # deque : double ended queue -> "file d'attente à 2 extrémités"

from keras.models import Sequential 
from keras.layers import Dense
from keras.optimizers import Adam

# Following lines are necessary to make the seed choice operate....
import tensorflow as tf
from keras import backend as K
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

class DQN_Agent:
    def __init__(self, env, activation='relu', seed=None, layers=(24,48), learning_rate=0.01):
        '''env parameter is :
           - either a tuple giving (observation space dim, action space dim)
           - aither a CartPole object.'''
        
        if isinstance(env, tuple):
            self.env         = None
            self.state_size  = env[0]
            self.action_size = env[1]
        else:           
            self.env         = env
            self.state_size  = env.observation_space.shape[0]
            self.action_size = env.action_space.n   # the number of discrete actions available
            
        self.activation  = activation # 'relu' or 'tanh'
        self.memory      = deque(maxlen=100000)

        # randomness management : self.epsilon (aka "exploration rate") is the rate in which an agent randomly  
        # decides its action rather than prediction. self.epsilon lowers down at each step 
        # (epsilon = epsilon*epsilon_decay) until epsilon_min is reached.
        self.epsilon       = 1.0           
        self.epsilon_min   = 0.01   
        self.epsilon_decay = 0.99   
        self.log_decay     = 1/25

        self.learning_rate = learning_rate  # how much neural net learns in each iteration (0.01 ou 0.001)
        self.learning_decay= 0.01   # decay of Adam optimiser
        self.gamma         = 0.95   # aka "decay" or "discount rate", to calculate the future discounted reward.
        
        self.model  = None      # this is the neural network, will be created soon... 
        self.seed   = seed      # the seed used to get repetable rondom sequences
        self.layers = layers
        
        # configure seed() for the differents components involved in randomness:
        if seed is not None:
            self.env.seed(seed)
            np.random.seed(seed)
            random.seed(seed)
            gym.spaces.prng.seed(seed)
            tf.set_random_seed(seed)

        self.__build_model(layers)        # creates the neural network in self.model
        
    def __build_model(self, layers):
        """Neural Net for Deep Q Learning"""
        # Sequential() creates the foundation of the layers.
        model = Sequential()  

        n1, n2 = layers        
        # Input Layer of state size(4) and 2 Hidden Layers with 'n1' and 'n2' nodes
        # 'Dense' is the basic form of a neural network layer
        # activation may be 'relu' or 'tanh'.

        # Hidden layer with 'n1' nodes, connected to the input (4 entries):
        model.add(Dense(n1, input_dim=self.state_size, activation=self.activation))        
        # Hidden layer 'n2' neurones:
        model.add(Dense(n2, activation=self.activation)) 
        # Output Layer with # of actions: 2 nodes (left, right)
        model.add(Dense(self.action_size, activation='linear')) 
        # Create the model based on the information above
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate, decay=self.learning_decay))
        self.model = model
        
    def remember(self, state, action, reward, next_state, done):
        '''Stores its 5 parameters as one tuple at right end of the deque'''
        self.memory.append((state, action, reward, next_state, done))

    def choose_action(self, state, epsilon):
        '''Our agent will randomly select its action at first by a certain percentage, 
           called ‘exploration rate’ or ‘epsilon’. This is because at first, it is better 
           for the agent to try all kinds of things before it starts to see the patterns. 
           When it is not deciding the action randomly, the agent will predict the reward value 
           based on the current state and pick the action that will give the highest reward. 
           np.argmax() is the function that picks the highest value between two elements in the
           act_values[0].'''
        # The agent acts randomly
        if np.random.rand() <= epsilon: 
            return self.env.action_space.sample()

        act_values = self.model.predict(state)
        '''act_values[0] looks like this: [0.67, 0.2], each numbers representing the reward 
           of picking action 0 and 1. And argmax function picks the index with the highest value. 
           In the example of [0.67, 0.2], argmax returns 0 because the value in the 0th index 
           is the highest.'''                
        return np.argmax(act_values[0])  # returns best action

    def replay(self, batch_size, epochs=1):
        '''In order for a neural network to understand and predict based on the environment data, 
           we have to feed it the information. fit() method feeds input and output pairs to the 
           model. 
           Then the model will train on those data to approximate the output based on the input.
           This training process makes the neural net to predict the reward value from a certain 
           state.'''

        x_batch, y_batch = [], []
        # minibatch is composed of 'batch_size' samples randomly extracted from the memory:
        minibatch = random.sample(self.memory, min(len(self.memory), batch_size))

        # make vectors x_batch and y_batch holding input and best output.
        # These vectors will be feed to model.fit(...) in order to train the network.
        for state, action, reward, next_state, done in minibatch:
            # keras model.predict(state) ouputs a tensor of 1 line x action_size cols.
            # => make a vector with the line [0] :
            
            # y_target gives the Q-values for the 2 possible actions:
            y_target = self.model.predict(state)[0]

            # set the Q-value for 'action' to 'r + gamma*max_over_a'[Q(s',a')]', where "max_over_a'[Q(s',a')]"
            # is the maximum of Q for the next state s' over all the possibles network outputs (actions) :
            y_target[action] = reward if done else reward + \
                               self.gamma*np.amax(self.model.predict(next_state)[0])
            
            x_batch.append(state[0]) # add input datas
            y_batch.append(y_target) # add targeted output 

        # Train the Neural Net (update the NN weights using back propagation):
        self.model.fit(np.array(x_batch), np.array(y_batch), batch_size=len(x_batch), epochs=epochs, verbose=0)
            
        # if possible, lower epsilon value
        if self.epsilon > self.epsilon_min: self.epsilon *= self.epsilon_decay

    def get_epsilon(self, t):
        return max(self.epsilon_min, min(self.epsilon, 1.0 - math.log10((t + 1)*self.log_decay)))
            
    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)
        
