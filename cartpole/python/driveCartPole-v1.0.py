# To be interpreted by Python3
import sys
import matplotlib.pyplot as plt
import numpy as np
from time import sleep, time
from random import randint
import math as m
from serial import Serial

# Connection to serial port:
# Serual port name  (the name is in the title bar of the monitor window):
#   Windows : "COM3" or "COM4" or ...
#   Linux   : "/dev/ttyACM0" (or see the monitor window title bar)
#   Mac OS X: see the monitor window title bar
#
# Parameters: # of data bits -> 8
#             # od STOP bit  -> 1
#             Parity         -> None
#             Speed          -> 9600, 14400, 19200, 28800, 38400,
#                                         57600, or 115200

serial_device = "/dev/ttyACM0"
BR = 115200
try:
    serialPort   = Serial(serial_device, baudrate=BR, timeout=None)
except:
    serial_device = "/dev/ttyACM1"
    serialPort   = Serial(serial_device, baudrate=BR, timeout=None)
    
print(serialPort) # just to see...

# Open serial port:
sleep(0.5)
if not serialPort.is_open :
    serialPort.open()
    sleep(0.5)
sleep(0.5)

# wait for Arduino ready:
print("Waiting for ARDUINO ....", end="")

data, M = b"", []
while True:
    data = serialPort.readline().decode().strip()
    if "Arduino OK" in data:
        break
    print(data)
    if "MPU_deg:" in data:
        t, coder_angle = float(data[1]), float(data[3])
        M.append([t, coder_angle])
    
print(" OK !", flush=True)    

                                         
plotWanted = True # you can change this : True or False
    
M = np.array(M)
if len(M):
    np.set_printoptions(suppress=True)
    plt.figure(figsize=(12,6))
    plt.plot(M[:,0], M[:,2],'.-m', label="CODER [°]")
    plt.legend(loc='lower right')
    plt.show()

serialPort.flushOutput()
sleep(0.5)

serialPort.close()          # close serial port
