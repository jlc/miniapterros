# To be interpreted by Python3
import sys, os
import matplotlib.pyplot as plt
import numpy as np
from time import sleep, time
from random import randint
import math as m
from serial import Serial

from stable_baselines3 import PPO

# Loading PPO model weights:
print("Loading PPO model weights... ", end="")

# ai/models/cartpole/CartPoleEnv_CopSim_PPO_21-07-21_04-02-38
# ai/models/cartpole/CartPoleEnv_CopSim_PPO_21-07-21_04-02-38/ZIP/model
# ai/models/cartpole/CartPoleEnv_CopSim_PPO_21-07-23_08-48-01/ZIP

# models_path = "/home/pi/miniapterros"
models_path = "/home/jlc/work/ENSAM/Projets/Mini-Apterros/gitlab.com/miniapterros"
model_rel_name = "ai/models/cartpole/CartPoleEnv_CopSim_PPO_21-07-23_08-48-01/ZIP/rl_model_40000_steps"
model = PPO.load(os.path.join(models_path, model_rel_name))
print("done.")

# Connection to serial port:
# Serial port name  (the name is in the title bar of the monitor window):
#   Windows : "COM3" or "COM4" or ...
#   Linux   : "/dev/ttyACM0" (or see the monitor window title bar)
#   Mac OS X: see the monitor window title bar
#
# Parameters: # of data bits -> 8
#             # od STOP bit  -> 1
#             Parity         -> None
#             Speed          -> 9600, 14400, 19200, 28800, 38400,
#                                         57600, or 115200
serial_device = "/dev/ttyACM0"
BR = 250000
try:
    serialPort   = Serial(serial_device, baudrate=BR, timeout=None)
except:
    serial_device = "/dev/ttyACM1"
    serialPort   = Serial(serial_device, baudrate=BR, timeout=None)
    
print(serialPort) # just to see...

# Open serial port:
sleep(0.5)
if not serialPort.is_open :
    serialPort.open()
    sleep(0.5)
sleep(0.5)

# wait for Arduino ready:
print("Waiting for ARDUINO ....", end="")

data, M = b"", []
while True:
    data = serialPort.readline().decode().strip()
    print(data)
    if "Arduino OK" in data: break
    if "MPU_deg:" in data:
        t, coder_angle = float(data[1]), float(data[3])
        M.append([t, coder_angle]) 
print(" OK !", flush=True)

plotWanted = False # you can change this : True or False

# run some episodes:
for episode in range(10):
    # read initial cartPole state: this is an equivalent step to the reset()
    # used with Gym environment.
    t0 = time()
    data = serialPort.readline()   # read serial port is a blocking operation...
    data = data.decode().strip()
    print(data)
    x_pos, x_lin_veloc, theta_, ang_veloc, done  = [float(x) for x in data.split()[1:]]
    stateEnv = [x_pos, x_lin_veloc, theta_, ang_veloc]

    step, M = 0, []
    while not done:
        
        t0 = time()
        
        # compute the Network action:
        action = model.predict(stateEnv, deterministic=True)
        action = float(action[0])
        # print("fin PPO: {:.3f} ms".format((time()-t0)*1000))
        # write action for Arduino CartPole environment:
        serialPort.write(bytes(f"{action:.2f}", encoding="ascii"))
        # now, wait for Arduino CartPole environment response:    
        data = serialPort.readline()   # read a line of data
        T = (time()-t0)*1000
        
        data = data.decode().strip()
        data = [float(x) for x in data.split()] # dt, x, x_dot, a, a_dot, done
        stateEnv = data[1:-1]
        done = bool(data[-1])
        step += 1        
        M.append([step, T, action, data])
        #print(f"{step:3d}, {action:.3f}, {data}")
        
##    data = serialPort.readline().decode().strip()
##    print(data)
    
    for m in M:
        print(f"step {m[0]:3d}, T{m[1]:6.1f} ms, action{m[2]:6.2f}, data {m[3:]}")
        
    print("episode [{:02d}] ended after {:03d} steps\n".format(episode,step))

    print("Waiting for ARDUINO ....", end="")
    data = b""
    while "Arduino OK" not in data.decode().strip() :
        data = serialPort.readline()
        print(data.decode().strip())
    print(" OK !", flush=True)

    if (not plotWanted):
        continue

    
M = np.array(M)
if len(M):
    np.set_printoptions(suppress=True)
    plt.figure(figsize=(12,6))
    plt.plot(M[:,0], M[:,2],'.-m', label="CODER [°]")
    plt.legend(loc='lower right')
    plt.show()

serialPort.flushOutput()
sleep(0.5)

serialPort.close()          # close serial port
