/* Encoder Library - TwoKnobs Example
 * http://www.pjrc.com/teensy/td_libs_Encoder.html
 *
 * This example code is in the public domain.
 */

// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>

// Change these pin numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability

const int pinA = 2; // Our first hardware interrupt pin is digital pin 2
const int pinB = 3; // Our second hardware interrupt pin is digital pin 3

Encoder myEnc(pinA, pinB); 
long int oldEncPos  = 0;
long int encoderPos = 0;

void setup() 
{
  Serial.begin(115200); // start the serial monitor link
  myEnc.write(0);
}

void loop() 
{
  encoderPos = myEnc.read()*0.25; 
  if(oldEncPos != encoderPos) 
  {
    Serial.println(encoderPos);
    oldEncPos = encoderPos;
  }
}
