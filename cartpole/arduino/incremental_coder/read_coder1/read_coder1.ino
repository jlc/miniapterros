//
// JLC v1.0  2021/01/18  Test program for the incremental coder ISC3806-003G-360BZ3-5-24C
//

//
// The coder outputs the 2 pulse signal A & B 
// For the encoder HEDSS: A is green and B is white
//

int pinA = 2;          // Signal A : digital input 2 is associated to interrupt #0
int pinB = 3;          // Signal B
volatile byte pos = 0;  // Coder position (# of steps)

void clearSerialBuffer()
{
  // read characters from the serial line until there is nothing to read:
  while(Serial.available() > 0) 
  {
    char t = Serial.read();
  }
}

void setup()  
{
   Serial.begin(115200);
   Serial.println("Test the incremental coder ISC3806-003G-360BZ3-5-24C");
   
   pinMode(pinA, INPUT_PULLUP);   // pullup is necessary for the coder to work properly...
   pinMode(pinB, INPUT_PULLUP);

   // attach the function 'front' to the falling edge of signal A:
   attachInterrupt(digitalPinToInterrupt(pinA), front, RISING);  

   pos = 17;
   Serial.print("Let the pole be verticaly up and hit ENTER when ready....");
   while (Serial.available() <= 0) {;}  // wait until user press a key...    
   clearSerialBuffer();                 // clear the buffer
}

void loop()   
{
   Serial.println(pos);   
   delay(100);
}

void front()   
{
   if ((PIND & B00001000) == 8)   
   {
      pos++;
   }
   else   
   {
      pos--;
   }
}
