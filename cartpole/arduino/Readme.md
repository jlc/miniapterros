Updated: JLC 2021/05/12

Description of files in cartpole/arduino/: 

* Incremental_coder/
    * `read_coder1`: example to read coder with one interrupt function on signal A
    * `read_coder3`: example to read coder with 2 interrupt functions on signal A & signal B
    * `read_lib_encoder`: example to read coder with the `encoder`library: works !!

* MPU
    * MPU_9250
        * `from_hideakitai-1`: example to read  MPU and print yaw angle

    * MPU_6050
        * `GetAngle`: simple example to read MPU and print yaw angle

* stepper_motor/
    * `continuousSpeed`: driver for the CartPole experience with continuous speed.  
    => Do not run this programme with the motor of the CartPole bench,  it wil move beyond the bench limits! Use this programme with a free stepper moteur (linked to nothing).
    * `continueousSpeed_bench_test`: Make the cart move along the silder. Run test with the carpole stepper-motor: the motor moves right over 400 steps, then it moves left until it reaches 400 steps, and the moves right to reach the position 0 step. The program xaits for the user to hit the ENTER key to start.
    * `stepperMotor_test`: Programm to test the A4988 driver. This chip is devoted to drive one stepper motor. 
