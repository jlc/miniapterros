//
// JLC v1.0  2018/05/25  Driver for the CartPole experience.
// JLC v1.3  2018/06/07  Communication protocol with Python pn laptop to read/write data on USB link
// JLC v1.3  2018/07/10  Adding learning 
// MP  v2.0  2018/11/28  Updating state machine
// MP  v2.1  2018/11/28  Adding incremental coder support: see "comptage" function
// MP  v2.2  2018/11/18  Filling "TESTMOTEUR" state: the mobile moves left and right; adding important line for security (SEE #1)
// MP  v2.3  2018/11/18  Updating curr_xpos_m calculation, using it to replace time calculation system in "TESTMOTEUR".
// MP  v2.4  2018/12/01  Updating "ESSAI" state, adding an ANTICOLLISION function, made to avoid collision of the mobile with chassis extremities (SEE #2)
// MP  v2.5  2018/12/01  "ESSAI" state now working with a serial attached computer
// MP  v2.6  2018/12/03  Deleting "delta" calculation function at the beging of loop function, replacing LEDWaitEsai by blinking LEDCalib
// MP  v2.6  2018/12/03  SENDINFOS function is now executing every looptime if the automation state is ESSAI
// MP  v2.7  2018/12/11  SENDINFOS function is now sending curr_angle_position only if it's changing
// MP  v2.8  2019/01/11  Adding the "C Channel" of the HEDS-9040 rotary encoder: the angular position is reset when the axis is vertical
// MP  v2.9  2019/14/11  Finally, validating the program with NO step loss: speeding up the external interruption subroutine by splitting it into two independant routines
// MP  v2.10 2019/16/11  Deleting every "C Channel" Related part of the program because of the imprecision of this sensor (SEE #3)
// JLC v3.0  2019/01/24  New material architecture after the work of Marc PERIER

#include <assert.h>

/*
  port D : broches numériques 0 à 7 de la carte Arduino.  
  port B : broches numériques 0 à 7 de la carte Arduino
  Les 2 broches 6 et 7 du port sont connectées au quartz et ne sont pas utilisables
  
  PORTD,2 -> D2   PORTB,0 -> D8
  PORTD,3 -> D3   PORTB,1 -> D9
  PORTD,4 -> D4   PORTB,2 -> D10
  PORTD,5 -> D5   PORTB,3 -> D11
  PORTD,6 -> D6   PORTB,4 -> D12
  PORTD,7 -> D7   PORTB,5 -> D13
*/

///////////////////////////////////////////////////
///////      Pins Layout          /////////////////
///////////////////////////////////////////////////
#define pinDir    5   // DIR  
#define pinStep   6   // STEP 
#define pinEnable 7   // ENABLE MOTOR

#define LEDWait   8   // red LED
#define LEDMotor  9   // yellow LED
#define LEDCalib 10   // blue LED

#define pinPB     12   // PushButton (PB)

#define pinSM1    11    // servomotor L
#define pinSM2    4     // servomotor R

#define AchannelPin  2    //the three rotary encoder channels
#define BchannelPin  3
#define CchannelPin 13

#define FORWARD  1
#define BACKWARD -1 

///////////////////////////////////////////////////
/////// stepper motors parameters /////////////////
///////////////////////////////////////////////////
#define stepAngle_deg 1.8                               // the Left Right motors step angle
#define zeroAngleMargin 3                               // the Left Right motors step angle

const float stepAngle_rad = stepAngle_deg*M_PI/180.;    // the Left Right motors step angle
const int nbStepPerRevol  = int(360./stepAngle_deg);    // number of steps for a full revolution

///////////////////////////////////////////////////
//////////    Wheel  parameters   /////////////////
///////////////////////////////////////////////////
#define wheelDiameter_cm 2.235                                  // Wheel diameter [cm]
const float perimeter_cm = M_PI*wheelDiameter_cm;               // Wheel perimeter [cm]
const float perimeter_m  = perimeter_cm*0.01;                   // Wheel perimeter [m]
const float stepDisp_cm  = stepAngle_rad*wheelDiameter_cm/2.;   // step distance [cm]
const float stepDisp_m   = 0.01*stepDisp_cm;                    // step distance [m]

//
// global variables related to the stepper motor:
//
#define benchLimit_m 0.2            // the maximum displacement that the mobile can do on one side
int motor_dir;                      // rotation direction FORWARD or BACKWARD
int torque_enabled;                 // used par ISR interrupt fucntion to allow nbStep incrent/decrement.
const float carSpeed_cm_s  = 20;    // car speed [cm/s]
const float motorSpeed_rps = carSpeed_cm_s/perimeter_cm;
const long int wanted_dt_ms   = 75 - 2;    // wanted time step (minus some ms for Serial.print compensation)

const float angle_limit_12deg_in_radians = 12.*M_PI/180.;

//
// global variables used by the timer/counter to generate the step signal
//
uint16_t counterPeriod_musec;       // the period of the timer counter in micro-secondes.
volatile uint16_t counter;          // counters for periods
volatile long int nbStep;           // the number of steps done, to measure distance, duration...
uint16_t targetCount;               // period to count for Left and Right motors

//
// Automaton states
//
enum AutomatonState
{
  WAIT,    // started by pushing PB 
  CALIB,   // calibrate rotation sensor to get the zero angle
  START,   // starts the cartPole
  STOP,    // stops the cartPole, ready to start
};
AutomatonState state; // the state of the driver autmaton

//
// glob vars to manage CartPole state :
//
float curr_xpos_m;            // current cart position [m]
float curr_xpos_cm;           // current cart position [cm]
float curr_lin_velocity;      // current cart linear velocity [m/s]
float curr_angle_deg;         // current pole angle [deg]
float old_angle_deg;          // current pole angle [deg]
float curr_angle_rad;         // current pole angle [radian]
float curr_ang_velocity_rad;  // current pole angle velocity [ rad/s]

float pprev_xpos_m;           // previous-previous cart position [m]
float prev_xpos_m;            // previous cart position [m]
float pprev_angle_rad;        // previous-previous pole angle [radian]
float prev_angle_rad;         // previous pole angle [radian]

float observation[4];               // to send a blok data over the USB link

bool done;
bool test_done;
bool calib_done;                         // reference position computed

//
// miscellanous global variables:
//
byte oldPBstate;      // to memorize push button state from one lopp step to anotehr
byte debug;           // the debug level (0 ... 20)
String data = "";     // data received by ARDUINO on the USB link

//
// global vars to manage time values:
//
long int dt;                           // the loop time step
long int currMillis;
long int prevMillis;
int timeDelay;

//
// global vars for angular wheel position calculation:
//
int volatile stepWheelCount = 0;     

// Forward declartion of run_test function to make move the stepper motor for testing...
void run_test();

inline
void setMotorDir(int dir)
{
  // To set the direction of the motor:
  // TODO : must be configured following V-REP learning....

  assert(dir== FORWARD || dir == BACKWARD);
  
  if (dir == FORWARD)
  {
    digitalWrite(pinDir, HIGH);
  }
  else
  { 
    digitalWrite(pinDir, LOW);
  }
  motor_dir = dir;
}

inline
void setMotorSpeed(float speed_RPS)
{
  if (speed_RPS == 0.)
  {
    targetCount = 0;
  }
  else
  {
    // convert "revolution per second" into the number of tics timer to count 
    targetCount = uint16_t(1000000./(speed_RPS*nbStepPerRevol)/counterPeriod_musec);   // count step period 
  }
}

//
// To control the stepper motors we use Timer1 interrupt running at 25Khz.
//
#define CLR(x,y) (x &= (~(1 << y)))
#define SET(x,y) (x |= (1 << y))
#define LF String("\n")

ISR(TIMER1_COMPA_vect)
{
  // Interrup function to generate the tops that drive the rotation speed of the stepper motor.
  
  counter++;
  if (counter >= targetCount)
  {
    counter = 0;
    if (targetCount != 0)
    {
      if (torque_enabled) nbStep += motor_dir;
      SET(PORTD,6);         // STEP Motor
      delayMicroseconds(5);
      CLR(PORTD,6);
    }
  }
}

inline void emptyUSB()
{
  // empty USB buffer, if needed:
  if (Serial.available())   // Data are available on the serial line (USB)
  {
    while (Serial.available() > 0) 
    {
      Serial.read();
    }    
  }
}

void setup()
{  
  Serial.begin(500000);
  
  // STEPPER PINS 
  pinMode(pinEnable,OUTPUT);      // ENABLE MOTOR 
  pinMode(pinStep,  OUTPUT);      // STEP 
  pinMode(pinDir,   OUTPUT);      // DIR  
  
  pinMode(LEDWait, OUTPUT);       //
  pinMode(LEDMotor, OUTPUT);
  pinMode(LEDCalib, OUTPUT);
  pinMode(pinPB,  INPUT);         // the push button

  pinMode(AchannelPin, INPUT);    // incremental encoder outputs
  pinMode(BchannelPin, INPUT);

  digitalWrite(pinEnable,HIGH);   // Disable motor torque
  
  // STEPPER MOTORS INITIALIZATION
  
  cli(); // desactivate global interruption
  
  // TIMER1 CTC MODE (Clear Timer on Compare Match Mode)
  TCCR1A &= ~(1 << WGM10);   // set bit WGM10 to 0 in TCCR1A
  TCCR1A &= ~(1 << WGM11);   // set bit WGM11 to 0 in TCCR1A 
  TCCR1B |=  (1 << WGM12);   // set bit WGM12 to 1 in TCCR1B
  TCCR1B &= ~(1 << WGM13);   // set bit WGM13 to 0 in TCCR1B

  // output mode = 00 (disconnected)
  TCCR1A &= ~(3 << COM1A0); 
  TCCR1A &= ~(3 << COM1B0); 
  
  // Set the timer pre-scaler : we use a divider of 8, resulting in a 2MHz timer on 16MHz CPU
  TCCR1B &= ~(0x07 << CS10);  // clear CS12, CS11 & CS10
  TCCR1B |=  (2 << CS10);     // set "CS12 CS11 CS10" to "0 1 0"

  // initalize counter value
  TCNT1 = 0;      

  // OCRIA with frequency 2 MHz:
  // 1000 ->   2 kHz : 0.500  ms
  //  500 ->   4 kHz : 0.250  ms
  //  250 ->   8 kHz : 0.125  ms
  //  125 ->  16 kHz : 0.0625 ms
  //  100 ->  20 kHz : 0.050  ms
  //   50 ->  40 kHz : 0.025  ms
  
  OCR1A = 250;   
  counterPeriod_musec = OCR1A/2;    // OCR1A / 2 MHz -> counter period in micro-sec

  // Enable Timer1 interrupt
  TIMSK1 |= (1 << OCIE1A);  

  sei(); // Activate global interrupt

  debug = 1;
  setMotorSpeed(motorSpeed_rps);
  if (debug)
  {
    Serial.print("\n cart speed [cm/s]       : "); Serial.print(carSpeed_cm_s);
    Serial.print("\n motor speed [rps]       : "); Serial.print(motorSpeed_rps);
    Serial.print("\n nbStep per revolution   : "); Serial.print(nbStepPerRevol);
    Serial.print("\n counter period [micro_s]: "); Serial.print(counterPeriod_musec);
    Serial.print("\n targetCount             : "); Serial.print(targetCount);
    Serial.print("\n pulse period       [ms] : "); Serial.print(counterPeriod_musec*targetCount*1.e-3, 3);
    Serial.print("\n wanted_dt_ms       [ms] : "); Serial.print(wanted_dt_ms);
  }
  setMotorSpeed(0);
  delay(100);

  //
  // Initialise the automaton state :
  //

  calib_done   = false;
  
  targetCount  = 0;
  counter      = 0;
  nbStep       = 0;       // cumulated step number
  timeDelay    = 200;
  
  state        = WAIT;
  oldPBstate   = LOW;

  prevMillis   = 0;
  
  curr_xpos_m       = 0.;      // current cart position [m]
  curr_lin_velocity = 0.;      // current cart linear velocity [m/s]
  curr_angle_deg    = 0.;      // current pole angle [deg]
  curr_angle_rad    = 0.;      // current pole angle [radian]
  curr_ang_velocity_rad = 0.;  // current pole angle velocity [ rad/s]

  done = false;
  test_done = false;     // JLC : false to run the displacement test 

  if (false)
  {
    //
    // make the platform vibrate to say : "I'm alive...":
    //
    digitalWrite(pinEnable,LOW);   // Enable motor
    for (uint8_t i=0; i<2; i++)
    {
      setMotorDir(FORWARD);
      setMotorSpeed(0.1);
      delay(50);
      setMotorDir(BACKWARD);
      setMotorSpeed(0.1);
      delay(50);
    }
  }
  digitalWrite(pinEnable,HIGH);   // Disable motor
  torque_enabled = 0;

  emptyUSB();

  // switch leds on or off as required:
  digitalWrite(LEDWait,HIGH);
  digitalWrite(LEDCalib,LOW);
  digitalWrite(LEDMotor,LOW);

  delay(500);

  // attach inputs to interrup functions o acquire rotation of the wheel:
  attachInterrupt(digitalPinToInterrupt(AchannelPin),comptagepos,RISING);  // was 0
  attachInterrupt(digitalPinToInterrupt(BchannelPin),comptageneg,RISING);  // was 1

  delay(500);
}

///////////////////
// The Main loop //
///////////////////


void loop() 
{   

  currMillis = millis();
  if (prevMillis == 0)
  {
    dt = 0;
    prevMillis = currMillis;
  }
  else
  {
    dt = currMillis - prevMillis;
  }
  
  //////////////////////////////////////
  //   2. Processing Events           // 
  //////////////////////////////////////

  byte PBstate = digitalRead(pinPB);
  
  //
  // PushButton was pushed ?
  //
  
  if (PBstate == HIGH && oldPBstate == LOW)
  {
    // Disabling the motor to avoid damaging the mobile while exiting "TEST" or "START" states 
    digitalWrite(pinEnable, HIGH); 
    torque_enabled = 0;
    setMotorSpeed(0.);

    switch(int(state))
    {
      case WAIT:
        //
        // just switch on the blue LED, and change the automaton state to CALIB.
        //        
        digitalWrite(LEDMotor,LOW);
        digitalWrite(LEDWait,LOW);
        digitalWrite(LEDCalib,HIGH);        

        Serial.println("\n    Move the at equilibrium then...");
        Serial.println(">>> Press button when ready to go to CALIB...\n");

      
        state = CALIB;
        timeDelay = 200;

        break;
    
      case CALIB:
      
        digitalWrite(LEDMotor,HIGH);
        digitalWrite(LEDWait,LOW);
        digitalWrite(LEDCalib,LOW);
  
        if (!calib_done)
        {
          timeDelay = 300;
          stepWheelCount  = 0;
          calib_done = true;          
          Serial.println("\n    You can move pole and check angle....");
          Serial.println(">>> Press button to START...");
        }
        else
        {          
          
          state = START;
          timeDelay = 0;
          done = false;
          
          curr_xpos_m           = 0.;
          curr_lin_velocity     = 0.;
          curr_angle_rad        = stepWheelCount*M_PI*0.002;        ; // reseting ditance and angle position to calib values.
          curr_ang_velocity_rad = 0.;
   
          pprev_xpos_m    = prev_xpos_m;
          prev_xpos_m     = curr_xpos_m;
          pprev_angle_rad = prev_angle_rad;
          prev_angle_rad  = curr_angle_rad;
          
          //
          // send first cartPole observation (like the reset with Gym):
          //
          Serial.print("Arduino OK\n");    // the key word to start episode!          
          setMotorSpeed(motorSpeed_rps);
          writeObservation();          
          dt = 0.;
          prevMillis = millis();   
        }
        break;
          
      case STOP:
      
        state = START;        
        timeDelay = 0;        
        done = false;      

        digitalWrite(LEDMotor,HIGH);
        digitalWrite(LEDWait,LOW);
        digitalWrite(LEDCalib,LOW);
  
        //
        // send first cartPole observation (like the reset with Gym):
        //
                
        curr_xpos_m           = 0.;
        curr_lin_velocity     = 0.;
        curr_angle_rad        = stepWheelCount*M_PI*0.002;        
        curr_ang_velocity_rad = 0.;
        
        pprev_xpos_m    = prev_xpos_m;
        prev_xpos_m     = curr_xpos_m;
        pprev_angle_rad = prev_angle_rad;
        prev_angle_rad  = curr_angle_rad;

        setMotorSpeed(motorSpeed_rps);
        dt = 0.;
        writeObservation();        
        prevMillis = millis();  
        break;
    }
  }
 
  //////////////////////////////////////
  // 3. Processing automaton state    // 
  //////////////////////////////////////

  if (state == START)
  {
    //
    // read until data (0 or 1) is available...
    //
    while (!Serial.available())   
    { ; }

    // we get the action given by the agent to the environment:
    // 0 -> FORWARD; 1 -> BACKWARD
    
    char inChar = Serial.read(); // should be '0' or '1'
    emptyUSB();       //char c = Serial.read(); // read the '\n' 
    
    // set the CartPole velocity according to the action given by the agent:
    assert(inChar == '0' || inChar == '1')
    if (inChar == '0')
    {
       setMotorDir(FORWARD);
    }
    else
    {
       setMotorDir(BACKWARD);
    }

    digitalWrite(pinEnable, LOW);   // Enable motor torque
    torque_enabled = 1;     

    //
    // let the system run for a specified time (ms) :
    //
    long int time_loop = millis()-currMillis ;
    while(time_loop < wanted_dt_ms) 
    {
      time_loop = millis()-currMillis ;
    }

    //
    // get the new system state:
    //
    // 0.36/180 -> 0.002 so : stepWheelCount*0.36*M_PI/180 -> stepWheelCount*M_PI*0.002
    
    curr_angle_rad = stepWheelCount*M_PI*0.002;  
    curr_xpos_m    = nbStep*stepDisp_m;
    
    done = false;
    if ( (fabs(curr_angle_rad) >= angle_limit_12deg_in_radians) || (fabs(curr_xpos_m) >= 0.2) ) 
    { 
      digitalWrite(pinEnable,HIGH);   // Disable motor torque
      torque_enabled = 0;
      done = true; 
    } 

    if (dt) 
    { 
      //curr_lin_velocity = 1000.*(curr_xpos_m - prev_xpos_m)/dt;      
      curr_lin_velocity = 500.*(pprev_xpos_m - 4.*prev_xpos_m + 3.*curr_xpos_m)/dt;  // 500 = 1000.*0.5, ordrer 2 finite diff
      //curr_ang_velocity_rad = 1000.*(curr_angle_rad -prev_angle_rad)/dt;
      curr_ang_velocity_rad = 500.*(pprev_angle_rad - 4.*prev_angle_rad + 3.*curr_angle_rad)/dt;
    }

    pprev_xpos_m    = prev_xpos_m;
    prev_xpos_m     = curr_xpos_m;
    pprev_angle_rad = prev_angle_rad;
    prev_angle_rad  = curr_angle_rad;

    writeObservation();

    if (done)
    {
      return_to_zero(curr_xpos_m);
      setMotorSpeed(0.);  
          
      state = STOP;   
      done = false;
      timeDelay = 200;

      digitalWrite(LEDMotor,LOW);
      digitalWrite(LEDWait,HIGH);
      digitalWrite(LEDCalib,LOW);
    }
  }
  
  else if (state == WAIT)
  {
    if (test_done == false)
    {
      test_done = true;  // single shot !
      run_test();    
      Serial.print("Motor test done. \n>>> Press button to go to CALIBRATE...\n");
    }
  }
  
  else if (state == CALIB)
  {    
    SENDINFOS();
  }

  oldPBstate = PBstate;  
  prevMillis = currMillis;   
  delay(timeDelay);
}

//first interruption attached function: counts positively:
void comptageneg() 
{                        
  if (digitalRead(AchannelPin) == LOW) 
  {
    ++stepWheelCount;
  }
}

//second interruption attached function: counts negatively:
void comptagepos() 
{
  if (digitalRead(BchannelPin) == LOW) 
  {        
    --stepWheelCount;
  }
}

inline void writeObservation()
{
    Serial.print(dt);
    Serial.print(" ");
    Serial.print(curr_xpos_m,3);
    Serial.print(" ");
    Serial.print(curr_lin_velocity,3);
    Serial.print(" ");
    Serial.print(curr_angle_rad,3);
    Serial.print(" ");
    Serial.print(curr_ang_velocity_rad,3);
    Serial.print(" ");
    Serial.print(done);
    Serial.print("\n");
    Serial.flush();
}

//Function that sends curr_angle_cm only if needed:
inline void SENDINFOS() 
{                          
   curr_angle_deg = stepWheelCount*0.36;
   if (old_angle_deg != curr_angle_deg) 
   {
      Serial.print("stepWheelCount: ");
      Serial.print(stepWheelCount);
      Serial.print("\tangle: ");
      Serial.print(curr_angle_deg,3);
      Serial.println(" °");
   }
   old_angle_deg = curr_angle_deg;
}  

inline void return_to_zero(float current_x)
{ 
  setMotorSpeed(motorSpeed_rps);
  int dir = FORWARD;
  if (current_x > 0) dir = BACKWARD;     
  setMotorDir(dir); 

  digitalWrite(pinEnable,LOW);   // Enable motor torque  
  torque_enabled = 1; 
  while(nbStep != 0)
  {
    delay(1);
  }
  digitalWrite(pinEnable,HIGH);   // Disable motor torque
  torque_enabled = 0;
  nbStep = 0;
  setMotorSpeed(0);
}

void
run_test()
{
  long int t0, t1;
  float x0;
  const long int nb_step_10cm = 10/stepDisp_cm;
  const long int nb_step_5cm  = 5/stepDisp_cm;
  
  torque_enabled = 0;
  SET(PORTD,7);         // Disable motor torque, pin_7 -> HIGH
  //digitalWrite(pinEnable,HIGH);   // Disable motor torque
  
  setMotorSpeed(0.);
  nbStep = 0;      

  // forward 0.1 meter:
  
  setMotorDir(FORWARD);
  setMotorSpeed(motorSpeed_rps);

  nbStep = 0; 
  t0 = millis();    
  
  torque_enabled = 1;
  CLR(PORTD,7);         // ENable motor torque, pin_7 -> LOW
  //digitalWrite(pinEnable,LOW);   // Enable motor torque        
  while(nbStep < nb_step_10cm)       // curr_xpos_cm < 10
  {
    delay(1);
  }        
  SET(PORTD,7);         // Disable motor torque, pin_7 -> HIGH
  //digitalWrite(pinEnable,HIGH);   // Disable motor torque
  torque_enabled = 0;
  
  x0 = 0.;
  curr_xpos_cm = nbStep*stepDisp_cm;
  t1 = millis();

  assert(nbStep == nb_step_10cm);
  Serial.print("\n x position     : "); Serial.print(curr_xpos_cm*10, 3); Serial.print(" [mm], ");    
  Serial.print(" elapsed time   : "); Serial.print((t1-t0)*1.e-3, 4); Serial.print(" [s], ");    
  Serial.print(" computed speed : "); Serial.print(1.e3*(curr_xpos_cm-x0)/(t1-t0)); Serial.print(" [cm/s]");    
  delay(500);
  
  // backward -0.1
  
  setMotorDir(BACKWARD);
  setMotorSpeed(motorSpeed_rps);

  t0 = millis();
  x0 = curr_xpos_cm;

  torque_enabled = 1;
  CLR(PORTD,7);         // ENable motor torque, pin_7 -> LOW
  //digitalWrite(pinEnable, LOW);   
  while(nbStep > -nb_step_10cm)    //curr_xpos_cm > -10
  {
    delay(1);
  }
  SET(PORTD,7);         // Disable motor torque, pin_7 -> HIGH
  //digitalWrite(pinEnable,HIGH);   // Disable motor torque
  torque_enabled = 0;
  
  curr_xpos_cm = nbStep*stepDisp_cm;
  t1 = millis();

  assert(nbStep == -nb_step_10cm);
  Serial.print("\n x position     : "); Serial.print(curr_xpos_cm*10,3); Serial.print(" [mm], ");    
  Serial.print(" elapsed time   : "); Serial.print((t1-t0)*1.e-3, 4); Serial.print(" [s], ");    
  Serial.print(" computed speed : "); Serial.print(1.e3*(curr_xpos_cm-x0)/(t1-t0)); Serial.print(" [cm/s]");    
  delay(500);


  // forward
  setMotorDir(FORWARD);
  x0 = curr_xpos_cm;
  t0 = millis();
  
  torque_enabled = 1;
  CLR(PORTD,7);         // ENable motor torque, pin_7 -> LOW
  //digitalWrite(pinEnable,LOW);       
  while(nbStep < 0)   // curr_xpos_cm < 0
  {
    delay(1);
  }
  SET(PORTD,7);         // Disable motor torque, pin_7 -> HIGH
  //digitalWrite(pinEnable,HIGH);   // Disable motor torque
  torque_enabled = 0;   
  
  curr_xpos_cm = nbStep*stepDisp_cm; 
  t1 = millis();
  
  Serial.print("\n x position     : "); Serial.print(curr_xpos_cm*10,3); Serial.print(" [mm], ");    
  Serial.print(" elapsed time   : "); Serial.print((t1-t0)*1.e-3, 4); Serial.print(" [s], ");    
  Serial.print(" computed speed : "); Serial.print(1.e3*(curr_xpos_cm-x0)/(t1-t0)); Serial.println(" [m/s]");    
  assert(nbStep == 0); 
}

