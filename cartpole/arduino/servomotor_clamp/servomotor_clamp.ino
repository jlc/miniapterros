/////////////////////////////////////////////////////////////////////////////////
// 
// Copyright Jean-Luc CHARLES (aka JLC) jean-luc.charles@member.fsfs.org
//
// JLC 2021/06/10 v1.0  JLC Initial version
//

/* This grograms runs on an Arduino NANAO. It waits for the digital input on "pin_start" 
 *  - if it is HIGH, the clamp is quicky openned 
 *  - if it is LOW, the clamp slowly close to keep the cartpole vertically.
*/

#include <Servo.h>

#define pin_servo_R 10
#define pin_servo_L 11
#define pin_start   5

Servo servo_L;   // create servo object to control a servo
Servo servo_R;  // create servo object to control a servo

#define step 1
#define tempo1 50
#define tempo2 5
#define pos_min 18
#define pos_max 108

int old_start = HIGH;

void setup() 
{
  servo_L.attach(pin_servo_L);  
  servo_R.attach(pin_servo_R);  
  pinMode(pin_start, INPUT_PULLUP);

  // open the clamp
  servo_L.write(pos_max);              
  servo_R.write(pos_min);
}


void loop() 
{
  //
  // wait for digital input on "pin_star" is LOW to start the 2 servos:
  //
  int start = digitalRead(pin_start);

  if (start != old_start)
  {
    if (start == HIGH)
    {
      // open the clamp quickly:
      
      servo_L.write(pos_max);              
      servo_R.write(pos_min);
    }
    else if (start == LOW)
    {
      // close the clamp slowly:
      
      for (int pos = pos_min; pos < pos_max; pos += step) 
      { 
        servo_L.write(pos_max+pos_min-pos+1);
        servo_R.write(pos+3);              
        
        delay(tempo1);                      
      }
      delay(1000);
    }
    delay(500); 
    old_start = start; 
  }
  
}
