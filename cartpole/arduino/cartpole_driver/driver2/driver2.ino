/////////////////////////////////////////////////////////////////////////////////
// 
// Copyright Jean-Luc CHARLES (aka JLC) jean-luc.charles@member.fsfs.org
//
// 
// JLC v1.0  2021/01/18  Program do drive the cartpole:
//                       - drives the stepper motor speed continuously in [Smin, Smax]
//                       - reads the incremental coder (interruption mode)
//                       - exchange data with the master
// JLC v1.1  2021/04/31 Using  Encoder Library : http://www.pjrc.com/teensy/td_libs_Encoder.html
//

#include <assert.h>

// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>

/*
  port D : digital pins #0 to #7  on Arduino UNO board
  port B : digital pins #8 to #13 on Arduino UNO board
    (bits 6 & 7 of port B map to the crystal pins and are not available)
  
  PORTD,2 -> D2   PORTB,0 -> D8
  PORTD,3 -> D3   PORTB,1 -> D9
  PORTD,4 -> D4   PORTB,2 -> D10
  PORTD,5 -> D5   PORTB,3 -> D11
  PORTD,6 -> D6   PORTB,4 -> D12
  PORTD,7 -> D7   PORTB,5 -> D13
*/

///////////////////////////////////////////////////
///////      Pins Layout          /////////////////
///////////////////////////////////////////////////

/////////// STEPPER MOTOR //////////
#define pinDir    5   // DIR  
#define pinStep   6   // STEP 
#define pinEnable 7   // ENABLE MOTOR

/////////// LEDS ///////////////////
#define LEDWait   8   // green LED
#define LEDCalib  9   // yellow LED
#define LEDMotor  10   // red LED


/////////// PUSH BUTTON ////////////
#define pinPB     12  // PushButton (PB)

/////////// INC. CODER /////////////
#define CoderApin  2  // A signal
#define CoderBpin  3  // B signal

#define FORWARD   1
#define BACKWARD -1 

///////////////////////////////////////////////////
/////// incremental coder parameters //////////////
///////////////////////////////////////////////////
#define coderTotNbSteps 360.
// Library "encoder" Encoder::read() return must be multiplied by 0.25 to give angle in degree:
const float coderConvStepRadians = 0.25*2.*M_PI/coderTotNbSteps;

///////////////////////////////////////////////////
/////// stepper motors parameters /////////////////
///////////////////////////////////////////////////
#define stepAngle_deg 1.8                                           // the step angle of the stepper-motor
#define step_mode 1.                                                // used for 1/2, 1/4, 1/8.... step mode
const float stepAngle_rad = stepAngle_deg*(step_mode)*M_PI/180.;    // the Left Right motors step angle
const int nbStepPerRevol  = int(360./(stepAngle_deg*(step_mode)));  // number of steps for a full revolution

///////////////////////////////////////////////////
//////////    Wheel  parameters   /////////////////
///////////////////////////////////////////////////
#define wheelDiameter_cm 2.28                                   // Wheel diameter [cm]
const float perimeter_cm = M_PI*wheelDiameter_cm;               // Wheel perimeter [cm]
const float stepDisp_cm  = stepAngle_rad*wheelDiameter_cm/2.;   // step distance [cm]
const float stepDisp_m   = stepDisp_cm*0.01;                    // step distance [m]

//
// global variables related to the stepper motor:
//
#define benchDisplacementLimit_m 0.2                              // the maximum displacement that the mobile can do on each side
int MOTOR_DIR;                                                    // rotation direction FORWARD or BACKWARD
int TORQUE_ENABLED;                                               // used par ISR interrupt function to allow MOTOR_NB_STEP incrent/decrement.
const float max_carSpeed_cm_s   = 23 ;                             // max car speed [cm/s]
const float max_motorSpeed_rps  = max_carSpeed_cm_s/perimeter_cm; // max motor rps
const unsigned long int loop_dt_ms = 100;                          // wanted time loop
const unsigned int loop_epsilon_ms = 5;                           // time lost in loop for calculus and  Serial.print

const float angle_limit_12deg_in_radians = 12.*M_PI/180.;

//
// global variables used by the timer/counter to generate the step signal
//
uint16_t counterPeriod_musec;       // the period of the timer counter in micro-secondes.
volatile uint16_t COUNTER;          // counters for periods
volatile long int MOTOR_NB_STEP;    // the number of steps done, to measure distance, duration...
uint16_t TARGET_COUNT;               // period to count for Left and Right motors

//
// Automaton states
//
enum AutomatonState
{
  WAIT=0,     // started by pushing PB 
  TEST,       // test motors, 
  CALIB,      // calibrate MPU, reset Coder
  READY,      // automaton is ready to start
  START,      // starts the cartPole
};
AutomatonState STATE; // the state of the driver autmaton
String stateLabel [] {String("[WAIT ]"), String("[TEST]"), String("[CALIB]"), String("[READY]"), String("[START]")};

//
// glob vars to manage CartPole state :
//
float curr_xpos_m;            // current cart position [m]
float curr_lin_velocity;      // current cart linear velocity [m/s]
float curr_angle_rad;         // current pole angle [rad] from Coder or MPU...
float old_coder_angle_deg;    // old pole angle [deg]
float curr_coder_angle_rad;   // current pole angle [radian]
float curr_ang_velocity_rad;  // current pole angle velocity [ rad/s]
float prev_xpos_m;            // previous cart position [m]
float prev_angle_rad;         // previous pole angle [radian]

float observation[4];         // Defines the cartpole state : [x, x', theta, theta']
bool DONE;                    // flag: used to end the episode

//
// miscellanous global variables:
//
byte oldPBstate;              // to memorize push button state from one lopp step to anotehr
byte debug;                   // the debug level (0 ... 20)
String data = "";             // data received by ARDUINO on the USB link

//
// global vars to manage time values:
//
unsigned long int dt;                  // the loop time step
unsigned long int t0;
int timeDelay;

//
// global vars for incremental coder position :
Encoder myEnc(CoderApin, CoderBpin); 
long int oldEncPos = 0;
long int encoderPos = 0;

#define CLR(x,y) (x &= (~(1 << y)))
#define SET(x,y) (x |= (1 << y))

// Forward declaartion of run_test function to make the stepper motor move for testing...
void run_test();
inline void writeObservation();

inline
void setMotorDir(int dir)
{
  // To set the direction of the motor:
  // TODO : must be configured following CopSim learning....
  // assert(dir== FORWARD || dir == BACKWARD);
  
  if (dir == FORWARD)
  {
    SET(PORTD, pinDir);
  }
  else
  { 
    CLR(PORTD, pinDir);
  }
  MOTOR_DIR = dir;
}

inline void
setMotorSpeed_RPS(float speed_RPS) 
{
  // this function computes the value of 'TARGET_COUNT' necessary to make the stepper modor
  // rotate at the given speed (in Rotation Per Second)

  MOTOR_DIR = FORWARD;
  if (speed_RPS == 0.)   
  {
    TARGET_COUNT = 0;
  }
  else 
  {
    if (speed_RPS < 0) 
    {
      speed_RPS = -speed_RPS;
      MOTOR_DIR = BACKWARD;
    }
    // convert "revolution per second" into the number of tics timer to count 
    TARGET_COUNT = uint16_t(1.e6/(speed_RPS*nbStepPerRevol)/counterPeriod_musec);   // count step period 
    setMotorDir(MOTOR_DIR);
  }
}

inline void
enableTorque()
{
  CLR(PORTD, pinEnable);         // Enable motor torque, pin_7 -> LOW
  TORQUE_ENABLED = 1; 
}

inline void
disableTorque()
{
  SET(PORTD, pinEnable);         // Disable motor torque, pin_7 -> HIGH
  TORQUE_ENABLED = 0; 
}

ISR(TIMER1_COMPA_vect) 
{
  //
  // Interrup function to generate the tops that drive the rotation speed of the stepper motor.
  //

  if (TARGET_COUNT == 0 || TORQUE_ENABLED == 0)
  {
    COUNTER = 0;
  }
  else
  {
    COUNTER++;
    if (COUNTER >= TARGET_COUNT)
    {
      SET(PORTD, pinStep);         // STEP Motor
      delayMicroseconds(1);
      CLR(PORTD, pinStep);
      MOTOR_NB_STEP += MOTOR_DIR;
      COUNTER = 0;
    }
  }  
}

inline void 
emptyUSB()
{
  // empty USB buffer, if needed:
  if (Serial.available()) {  
    // Data are available on the serial line (USB)
    while (Serial.available() > 0) {
      Serial.read();
    }    
  }
}

void setup() 
{  
  //
  // To control the stepper motors we use Timer1 interrupt running at 25Khz.
  //
    
  cli(); // deactivate interruptions
  
  // TIMER1 CTC MODE (Clear Timer on Compare Match Mode)
  TCCR1A &= ~(1 << WGM10);   // set bit WGM10 to 0 in TCCR1A
  TCCR1A &= ~(1 << WGM11);   // set bit WGM11 to 0 in TCCR1A 
  TCCR1B |=  (1 << WGM12);   // set bit WGM12 to 1 in TCCR1B
  TCCR1B &= ~(1 << WGM13);   // set bit WGM13 to 0 in TCCR1B
  
  // output mode = 00 (disconnected)
  TCCR1A &= ~(3 << COM1A0); 
  TCCR1A &= ~(3 << COM1B0); 
  
  // Set the timer pre-scaler : we use a divider of 8, resulting in a 2MHz timer on 16MHz CPU
  TCCR1B &= ~(0x07 << CS10);  // clear CS12, CS11 & CS10
  TCCR1B |=  (2 << CS10);     // set "CS12 CS11 CS10" to "0 1 0"
  
  // initalize counter value
  TCNT1 = 0;      
  
  // OCRIA with frequency 2 MHz:
  // 1000 ->   2 kHz : 0.500  ms
  //  500 ->   4 kHz : 0.250  ms
  //  250 ->   8 kHz : 0.125  ms
  //  125 ->  16 kHz : 0.0625 ms
  //  100 ->  20 kHz : 0.050  ms
  //   50 ->  40 kHz : 0.025  ms
  
  OCR1A = 250;   
  counterPeriod_musec = OCR1A/2;    // OCR1A / 2 MHz -> counter period in micro-sec
  
  // Enable Timer1 interrupt
  TIMSK1 |= (1 << OCIE1A);  
  
  sei(); // Activate interrupts
    
  // STEPPER PINS 
  pinMode(pinEnable,OUTPUT);        // ENABLE MOTOR 
  pinMode(pinStep,  OUTPUT);        // STEP 
  pinMode(pinDir,   OUTPUT);        // DIR  

  disableTorque();   // Disable motor torque
  setMotorSpeed_RPS(0);

  pinMode(LEDWait,  OUTPUT);        //
  pinMode(LEDMotor, OUTPUT);
  pinMode(LEDCalib, OUTPUT);
  pinMode(pinPB,    INPUT_PULLUP);     // the push button

  pinMode(CoderApin, INPUT_PULLUP);    // pullup is necessary for the coder to work properly...
  pinMode(CoderBpin, INPUT_PULLUP);

  //
  // start serial bus USB
  //
  Serial.begin(115200);
  if (true)
  {
    enableTorque();
    for (uint8_t i=0; i<4; i++)
    {
      setMotorSpeed_RPS(0.1);
      delay(50);
      setMotorSpeed_RPS(-0.1);
      delay(50);
    }
  }
  disableTorque();
  setMotorSpeed_RPS(0.);
  myEnc.write(0);
   
  TARGET_COUNT  = 0;
  COUNTER       = 0;
  MOTOR_NB_STEP = 0;       
  DONE      = false;
  
  timeDelay     = 200;  
  oldPBstate    = LOW;

  Serial.print("\n");             
  Serial.println(">>> Place cart at x=0 then push button");
}


///////////////////
// The Main loop //
///////////////////

void loop() 
{      
  //////////////////////////////////////
  //   2. Processing Events           // 
  //////////////////////////////////////

  // Disable the motor torque to avoid damaging the mobile while exiting "TEST" or "START" states 
  disableTorque();
  setMotorSpeed_RPS(0.);

  //
  // At the end of an episod, DONE is set to rue.
  //
  
  if (DONE) 
  {    
    //
    // switch to  READY state
    //
    STATE = READY; 
    timeDelay = 200;              

    digitalWrite(LEDWait,  LOW);
    digitalWrite(LEDCalib, LOW);
    digitalWrite(LEDMotor, LOW);
    DONE  = false;         

    //
    // make the cart move to its start position (x=0)
    // return_to_zero(curr_xpos_m);

    MOTOR_NB_STEP = 0;
    myEnc.write(0);  
    
    String m(">>> Move cart at x=0 and the pole vertically...then push button to START");     
    Serial.println(m);
  }
  else 
  {    
    //
    // Read the push button state
    //      
    byte PBstate = digitalRead(pinPB);     
    
    if (PBstate == HIGH && oldPBstate == LOW) 
    {
      //
      // the event PB has been detected
      //
      switch(int(STATE)) 
      {
        
        case WAIT:
        {
          //
          // change state to TEST
          //            
          STATE = TEST;
          
          //JLC run_test();    // make the cart move forward and backward from x=0           

          digitalWrite(LEDWait,  HIGH);
          digitalWrite(LEDCalib, LOW);
          digitalWrite(LEDMotor, LOW);

          String m(">>> Place the pole vertically then push button to calibrate encoder");
          Serial.println(m);

          break;
        }
        
        case TEST:
        {
          //
          // change state to CALIB
          //
          STATE = CALIB;
          timeDelay = 100;
      
          digitalWrite(LEDWait,  LOW);
          digitalWrite(LEDCalib, HIGH); 
          digitalWrite(LEDMotor, LOW);

          Serial.println(" Done !!!\n");      

          myEnc.write(0); 
          
          String m(">>> Move the pole to see angles... push button when ready\n");
          Serial.print(m);

          t0 = millis();
          break;
        }
        
        case CALIB:
        {
          //
          // switch state to READY.
          //        
          STATE = READY;
          timeDelay = 200; 
          
          digitalWrite(LEDWait,  LOW);
          digitalWrite(LEDCalib, LOW);
          digitalWrite(LEDMotor, LOW);

          myEnc.write(0); 

          String m(">>> Move cart at x=0 and pole vertically... then push button");
          Serial.println(m);
          
          break;
        }
         
        case READY:
        {
          //
          // switch state to START (start the cartpole)
          //
          STATE = START;
          timeDelay = 0.;
          
          digitalWrite(LEDWait,  LOW);
          digitalWrite(LEDCalib, LOW);
          digitalWrite(LEDMotor, HIGH);          
          
          disableTorque();
          setMotorSpeed_RPS(0.);

          //
          // initialize the cartpole state
          //
          curr_xpos_m           = 0.;  // current cart position [m]
          curr_lin_velocity     = 0.;  // current cart linear velocity [m/s]
          old_coder_angle_deg   = 0.;  // old pole angle [deg]
          curr_coder_angle_rad  = 0.;  // current pole angle [radian]
          curr_ang_velocity_rad = 0.;  // current pole angle velocity [ rad/s]
          prev_xpos_m           = 0.;  // previous cart position [m]
          prev_angle_rad        = 0.;  // previous pole angle [radian]                

          DONE          = false;
          TARGET_COUNT  = 0;
          COUNTER       = 0;
          MOTOR_NB_STEP = 0;   
          myEnc.write(0);     
          
          //
          // send first cartPole observation (like the reset with Gym):
          //
          Serial.print("Arduino OK\n");    // the key word to start episode!                    
          t0 = millis();
          dt = 0.;                              
          writeObservation();                            
          break;
        }
      }
    }
    oldPBstate = PBstate;   
  }
 
  //////////////////////////////////////
  // 3. Processing automaton state    // 
  //////////////////////////////////////

  if (STATE == WAIT) 
  {
    //
    // make the LED blink
    //
    digitalWrite(LEDWait, 1-digitalRead(LEDWait));    
    
  }
  else if (STATE == TEST)
  {    
  }
  else if (STATE == CALIB) 
  {  
    //
    // Displays coder angles
    //
    SENDINFOS();
  }
  else if (STATE == READY) 
  {    
    //
    // Make the LED blink, waiting for the user to push the button
    //
    digitalWrite(LEDMotor, 1-digitalRead(LEDMotor));    
  }
  else if (STATE == START) 
  {
    enableTorque();
    
    Serial.setTimeout(5);
    
    while( ! DONE) 
    {
      t0 = millis();
      DONE = false;
      
      //
      // read until data (motor speed) is available...
      //
      while (!Serial.available())   
      { ; }
  
      // we get the action given by the agent to the environment:
      // action is a float in the interval [-1. ; 1. ] must be multiplied by Vmax

      float action = Serial.parseFloat();   // read the neural neutwork answer : action in [-1., 1.]
      emptyUSB();
      
      setMotorSpeed_RPS(action*max_motorSpeed_rps);
      
      //
      // let the system run for a specified time (ms) :
      //
      while (millis() - t0 < loop_dt_ms -loop_epsilon_ms) 
      {
        delay(1);
      }  
      
      //
      // get the new system state:
      //
      curr_coder_angle_rad = myEnc.read()*coderConvStepRadians;   // the angle measured with the coder
      curr_xpos_m    = MOTOR_NB_STEP*stepDisp_m;
      curr_angle_rad = curr_coder_angle_rad;
      
      if ( (fabs(curr_angle_rad) >= angle_limit_12deg_in_radians) || (fabs(curr_xpos_m) >= benchDisplacementLimit_m) )  { 
        disableTorque();
        DONE = true; 
      }

      dt = millis()-t0;  
      curr_lin_velocity = 1000.*(curr_xpos_m - prev_xpos_m)/dt;      
      curr_ang_velocity_rad = 1000.*(curr_angle_rad -prev_angle_rad)/dt;
      
      writeObservation();     // Send observation (dt,x,x',a,a',DONE) to the AGENT
      
      prev_xpos_m     = curr_xpos_m;            // x_i-1 cart position [m]
      prev_angle_rad  = curr_angle_rad;         // a_i-1 pole angle [radian]      
    }
    Serial.setTimeout(1000);
  }

  delay(timeDelay);
}

inline void writeObservation()
{
  Serial.print(dt); 
  Serial.print(" ");
  Serial.print(curr_xpos_m, 3); Serial.print(" ");
  Serial.print(curr_lin_velocity, 3); Serial.print(" ");
  Serial.print(curr_angle_rad, 3); Serial.print(" ");
  Serial.print(curr_ang_velocity_rad, 3); Serial.print(" ");
  Serial.print(DONE); 
  Serial.print("\n");Serial.flush();
}

inline void SENDINFOS() 
{
  //
  // Sends curr_angle_cm only if needed:
  //
  float curr_coder_angle_deg = myEnc.read()*0.25; 
  
  if (old_coder_angle_deg != curr_coder_angle_deg)
  {
    Serial.print("t_ms: ");  Serial.print(millis()-t0);
    Serial.print(" CODER_deg: "); Serial.println(curr_coder_angle_deg, 1);
    old_coder_angle_deg = curr_coder_angle_deg;
  }
}  

inline void 
return_to_zero(float current_x) 
{ 
  //
  // make the cart return to the start position (x=0)
  //
  if (MOTOR_NB_STEP != 0) {
    float motor_speed_rps = max_motorSpeed_rps/3.;
    if (current_x > 0) motor_speed_rps *= -1.;
    
    setMotorSpeed_RPS(motor_speed_rps);
    enableTorque();
    
    while(MOTOR_NB_STEP != 0) {
      delay(1);
    }
    assert (MOTOR_NB_STEP == 0);
    disableTorque();
  }    
  setMotorSpeed_RPS(0);
}

void run_test() 
{
  long int t_start, t_stop;
  float x0;
  const long int nb_step_10cm = 10/stepDisp_cm;
  String m[3] = {"x [cm]: ", ", time [s]: ", ", speed [cm/s]: "};
  
  disableTorque();
  setMotorSpeed_RPS(0.);

  // forward 0.1 meter:
  setMotorSpeed_RPS(max_motorSpeed_rps/2.);
  MOTOR_NB_STEP = 0; 
  t_start = millis();    

  enableTorque();
  while(MOTOR_NB_STEP < nb_step_10cm) 
  {       
    delay(1);
  }        
  assert(MOTOR_NB_STEP == nb_step_10cm);
  disableTorque();
    
  x0 = 0.;
  float curr_xpos_cm = MOTOR_NB_STEP*stepDisp_cm;
  t_stop = millis();

  Serial.print(m[0]); Serial.print(curr_xpos_cm, 3);
  Serial.print(m[1]); Serial.print((t_stop-t_start)*1.e-3, 4);
  Serial.print(m[2]); Serial.println(1.e3*(curr_xpos_cm-x0)/(t_stop-t_start));
  delay(200);
  
  // backward
  setMotorSpeed_RPS(-max_motorSpeed_rps/2.);
  x0 = curr_xpos_cm;
  t_start = millis();

  enableTorque();
  while(MOTOR_NB_STEP > -nb_step_10cm) 
  {
    delay(1);
  }
  //assert(MOTOR_NB_STEP == -nb_step_10cm);
  disableTorque();
    
  curr_xpos_cm = MOTOR_NB_STEP*stepDisp_cm;
  t_stop = millis();

  Serial.print(m[0]); Serial.print(curr_xpos_cm*10,3); 
  Serial.print(m[1]); Serial.print((t_stop-t_start)*1.e-3, 4);
  Serial.print(m[2]); Serial.println(1.e3*(curr_xpos_cm-x0)/(t_stop-t_start));  
  delay(500);

  // forward
  setMotorSpeed_RPS(max_motorSpeed_rps/2.);
  x0 = curr_xpos_cm;
  t_start = millis();
  
  enableTorque();
  while(MOTOR_NB_STEP < 0) 
  {
    delay(1);
  }
  disableTorque();
  setMotorSpeed_RPS(0.);
    
  curr_xpos_cm = MOTOR_NB_STEP*stepDisp_cm; 
  t_stop = millis();
  
  Serial.print(m[0]); Serial.print(curr_xpos_cm*10,3); 
  Serial.print(m[1]); Serial.print((t_stop-t_start)*1.e-3, 4); 
  Serial.print(m[2]); Serial.println(1.e3*(curr_xpos_cm-x0)/(t_stop-t_start)); 
  assert(MOTOR_NB_STEP == 0); 
}
