# CartPole

## Training strategy:

_When swinging_ 
The goal is to get the pole in vertical position up: to swing th pole, the cart must have a speed opposite to the speed of the pole

One the module of the pole angle is less than 45°, the cart speed must have the same sign as the pole speed in order to push the pole to a vertical up position.

_When tracking the pole equilibrium_:

The training goal is to make the pole stand upright without falling, now the cart speed must have the opposite sign of the pole speed.