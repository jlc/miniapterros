############################################################
# v1.1 2018-05_17 JLC 'shutdown' method is renamed 'close'
# v1.2 2018-06_29 JLC scene path is now given as an argument
# v1.3 2019-03-13 JLC displacement control does not work :
#                     instead we must control with velocity and d =velocity*dt
# v1.4 2019-05-21 JLC Update fot DDPG
# v2.0 2020-04-02 JLC Migration to PPO
# v2.0 2023-04-09 JLC Flag for swing training
##########################################################

import numpy as np
from math import pi, radians
import gym

from coppeliasim_api.env.CopSim import Simulator
from coppeliasim_api.env import sim
from coppeliasim_api.env.constants import COPSIM_DIR

blocking = sim.simx_opmode_blocking
dir_copsim = COPSIM_DIR

# rewards.py define the functions that can be used as methods in the  CartPoleEnv_CopSim cless:
import cartpole.copsim.rewards as rewards 

class CartPoleEnv_CopSim(gym.Env):
    def __init__(self,
                 vers: str,
                 scene: str,
                 dt: float,
                 theta_lim:int=12,
                 x_lim:int=0.2,
                 reward:str="reward",
                 swing:bool=False,
                 theta0_deg:float=None,
                 clip_action:bool=False,
                 veloc_mag:float=None,
                 displ_mag:float=None,
                 coppelia_sim_port:int=20000,
                 headless:bool=True,
                 verbose:int=1):
        
        '''
        Parmeters of the constructor CartPoleEnv_CopSim:
            vers:         "v0","v1" or "v2", to specify the to specify the CartPole version
                          ('v2' for continuous CartPole).
            scene:        the CopSIM scene to load (file *.ttt)
            dt:           timestep in seconds for the CopSIM simulation
            theta_lim:    limit angle in degrees beyond which the training is over.
            x_lim:        limit displacement beyond which the training is over.
            reward        name of the reward function to use as a method in the class.
            swing:        Wether the cartpole swings or not
            theta0_deg:   If swing is True : the pole angle value when resetting the cartpole
            veloc_mag:    linear velocity max in m/s for driving the cartpole.
            displ_mag:    displacement increment in meters for disp. driving.
            verbose:      verbosity (displaying info), possible values: 0,1,2 (Default: 1).
            clip_action:  wether to clip 'action' given by the neural network to keep its 
                          value in the 'action space' interval.
            headless:     wether to display 3D rendering or not.
                          For computing only headless=True (default value: True).'''

        # initialise the base class:
        super().__init__()

        self.dt = dt
        self.scene = scene
                                  
        # create a Simulator object through the Coppeliasim API:
        self.venv = Simulator(coppelia_sim_port, dir_copsim, headless=headless, verbose=verbose)
        self.venv.start()    # start the CopSIM simulation
        self.venv.load_scene(scene)

        # get the handles on useful objects in the simulation:
        self.slider = self.venv.get_object_by_name('Slider')
        self.cart   = self.venv.get_object_by_name('Cart')
        self.joint  = self.venv.get_object_by_name('Joint')
        self.pole   = self.venv.get_object_by_name('Pole')

        # Angle at which to fail the episode (12°):
        self.theta_threshold_radians = radians(theta_lim)
        
        # Position at which to fail the episode (Gym: 2.4 m):
        self.x_threshold = x_lim      # [m]
                
        # flag for wing
        self.swing = swing
        if self.swing:
           assert theta0_deg is not None
           # pole angle for restting the cartpole:
           self.theta0_deg = theta0_deg
    
        # the min and max values for the agent action:
        self.min_action = -1.0
        self.max_action =  1.0
        self.prev_action = None
        
        # Set the action space as a scalar in the interval [min_action, max_action]:
        self.action_space = gym.spaces.Box(low=self.min_action, high=self.max_action, shape=(1,))

        # Set the observation space as an array of 4 floats, with 4 min values and 4 max values:                                       
        # X and Angle limit set to 2*their threshold so failing observation is still within bounds:
        high = np.array([self.x_threshold*2, np.finfo(np.float32).max,
                         self.theta_threshold_radians*2, np.finfo(np.float32).max])
        self.observation_space = gym.spaces.Box(-high, high, dtype=np.float32)

        self.nb_step = 0                  # the number of steps done
        self.steps_beyond_done = None
        self.viewer  = None
        self.state   = None
        self.clip_action = clip_action
        
        # Set the max number of steps based on the environment version:
        self.vers    = "EnvSpec(CartPole-"+vers.lower()+")"
        if self.vers == "EnvSpec(CartPole-v0)" :
            self._max_episode_steps = 200
        elif self.vers == "EnvSpec(CartPole-v1)" :
            self._max_episode_steps = 500
        elif self.vers == "EnvSpec(CartPole-v2)" :
            self._max_episode_steps = 500
        else:
            raise RuntimeError("spec must be 'v0', 'v1' or 'v2'")

        if verbose >= 0 :
            print(f'[CartPole-{vers}] initialized, _max_episode_steps:{self._max_episode_steps}')
            print("action_space:", self.action_space)
            print("observation_space:", self.observation_space)

        if veloc_mag is None and displ_mag is None:            
            raise RuntimeError("You must specify one of 'veloc-mag' or 'displ_mag' !")
        elif displ_mag is not None  and veloc_mag is None:
            self.displ_mag = displ_mag
            self.step = self.step_displacement      # the method step() is  set to step_displacement()
        elif veloc_mag is not None and displ_mag is None:
            self.veloc_mag = veloc_mag              # the method step() is  set to step_velocity()
            self.step = self.step_velocity
        
        # The reward function tu use as the method reward:
        self.reward_func = reward

    def seed(self, s): self.seed = s

    def getState(self, step=False):
        '''Get the cartpole state [x, x', theta, theta'], and
           update the attribute 'state'.
           If step is True, request the CopSIM simulation to make a time step.'''

        if step:
            self.venv.step_synchro_simulation()
            self.nb_step += 1
            
        cartpos              = self.slider.get_joint_position()
        cartvel, cart_angvel = self.cart.get_object_velocity()
        theta                = self.joint.get_joint_position()
        polvel, pole_angvel  = self.pole.get_object_velocity()
        
        self.state = [cartpos, cartvel[1], theta, pole_angvel[0]]

        return np.array(self.state).astype('float32')


    def imposeState(self, state):
        '''To set positions and velocities for the bodies in CopSIm'''

        assert len(state) == 4
        x, x_dot, theta, theta_dot = state
        self.slider.set_joint_position(x)
        self.slider.set_joint_target_velocity(x_dot)
        self.joint.set_joint_position(theta)
        self.joint.set_joint_target_velocity(theta_dot)

        self.state = state
        
        # request the CopSim simulation to make a time step:
        self.venv.step_synchro_simulation()
        self.nb_step += 1
        
    def step_velocity(self, action):
        '''Make one time step in the CopSim simulation under the input 'action'.'''
        try :
            assert self.action_space.contains(action), \
                "%r (%s) invalid" % (action, type(action))
        except :
            if self.clip_action: np.clip(action, self.min_action, self.max_action)
               
        veloc = self.veloc_mag*float(action)
        
        # step
        self.slider.set_joint_target_velocity(veloc)
        self.venv.step_synchro_simulation()
        self.nb_step += 1
        
        # observe (sets the attribute self.state):
        self.getState()
        done, reward = self.compute_done_reward(action)
        return np.array(self.state), reward, done, {}

    def step_displacement(self, action):
        '''Make one time step in the CopSIM simulation under the input 'action'.'''
        try :
            assert self.action_space.contains(action), \
                "%r (%s) invalid" % (action, type(action))
        except :
            if self.clip_action: np.clip(action, self.min_action, self.max_action)

        displ = self.displ_mag*float(action)
        
        # step
        x = self.slider.get_joint_position()
        self.slider.set_joint_target_position(x+displ)
        self.venv.step_synchro_simulation()
        self.nb_step += 1
        
        # observe (sets the attribute self.state):
        self.getState()    
        done, reward = self.compute_done_reward(action)
        return np.array(self.state), reward, done, {}

    def compute_done_reward(self, action):
        
        x, x_dot, theta, theta_dot = self.state

        done = abs(x) > self.x_threshold
        if not self.swing:
            # angle limit only if the initial position of the pole is vertical position up:
            done = done or abs(theta) > self.theta_threshold_radians 
        # limit the total number of steps:
        done = done or (self._max_episode_steps is not None and self.nb_step >= self._max_episode_steps)

        if self.prev_action is None: self.prev_action = action

        #JLC: improved reward computation:
        if not done:
            reward_instr = f"rewards.{self.reward_func}(self, action)"
            reward = eval(reward_instr) # eval() generates Python code
            #print(f"{reward_instr} -> {reward}")

        else:
            # pole has fell or cart is outside limits:
            if self.steps_beyond_done is None:
                # Pole just fell!
                self.steps_beyond_done = 0
                reward = 1.0
            else:
                if self.steps_beyond_done == 0:
                    print("You are calling 'step()' even though this environment "+\
                          "has already returned done = True. \nYou should always "+\
                          "call 'reset()' once you receive 'done = True' "+\
                          "-- any further steps are undefined behavior.")
                self.steps_beyond_done += 1
                reward = 0.0

        self.prev_action = action
        return done, reward


    def reset(self):
        '''To reset the CartPole to its inital state'''
        
        print(f"Cartpole reset - dt: {self.dt}, nb_steps: {self.nb_step}")
        self.venv.stop_simulation()
        self.nb_step = 0
        self.prev_action = None

        self.venv.start_synchro_simulation(self.dt)
        
        if not self.swing:
            self.state = np.random.uniform(low=-0.05, high=0.05, size=(4,))
        else:
            self.state = [0, 0, np.radians(self.theta0_deg), 0]
            
        x, x_dot, theta, theta_dot = self.state
        self.state = 0, 0, theta, 0
        # set positions and velocities in CopSIM:
        self.imposeState(self.state)
        self.steps_beyond_done = None
        
        return np.array(self.state)
        

    def close(self):
        self.venv.stop_simulation()
        self.venv.end()

    def runDispl(self, nb_step=200, dx=0.005, dt=None):
        '''To run the CartPole with displacement imposed.'''

        if dt is not None : self.dt = dt
        print(f"Ready to run {nb_step} steps of displacement with timestep={self.dt}")
        States = []
        state = self.reset()
        print("state:", state)

        x,d = 0,0
        v = dx/self.dt # we choose to drive with velocity
        for _ in range(nb_step):

            x = self.slider.get_joint_position()
            print(f"x={x:.4f}")

            self.slider.set_joint_target_velocity(v)
            self.venv.step_synchro_simulation()
            self.nb_step += 1
            # observe (sets the attribute self.state):
            state = self.getState()            
            States.append(state)            
        return np.array(States)

    def runVeloc(self, nb_step=200, velocity=0.1, dt=None):
        '''To run the CartPole with velocity imposed.'''

        if dt is not None : self.dt = dt
        print(f"Ready to run {nb_step} steps of velocity with timestep={self.dt}")
        States = []
        state = self.reset()
        print("state:",state)

        x =0
        for _ in range(nb_step):

            x = self.slider.get_joint_position()
            self.slider.set_joint_target_velocity(velocity)
            self.venv.step_synchro_simulation()
            self.nb_step += 1            
            # observe (sets the attribute self.state):
            state = self.getState()            
            States.append(state)
            
        return np.array(States)


    def runFree(self, nb_step=100, dt=None):
        '''To run the CartPole with no external load, just the pole balancing'''

        if dt is not None : self.dt = dt
        print(f"Ready to run {nb_step} steps of free run with timestep={self.dt}")
        States = []
        self.swing = True
        self.theta0_deg = 90
        state = self.reset()
        
        print("state:",state)

        for _ in range(nb_step):
            state = self.getState(step=True)
            States.append(state)
            
        return np.array(States)


    def plot(self, data, y1_max=None, y2_max=None, y3_max=None, y4_max=None, title=""):

        M = np.array(data)
        if M.ndim != 2 or M.shape[1] !=  4 :
            print("plot :data must be a numpy.array with shape=(x,4)... sorry!")
            return

        import matplotlib.pyplot as plt
        
        nb_step = M.shape[0]
        T = range(nb_step)

        fig = plt.figure(title, figsize=(13,6))
        plt.subplots_adjust(wspace=0.4)
        
        #fig.suptitle(title)
        ax1 = plt.subplot(121,label='ax1')
        plt1 = ax1.plot(T, M[:,0]*1000,'.-b', label='x [mm]')
        y1max = np.abs(M[:,0]*1000).max()*1.1 if y1_max is None else y1_max
        ax1.set_ylim(-y1max, y1max)
        ax1.set_xlabel('step')
        ax1.tick_params(axis='y', labelcolor='b')
        ax1.set_title('Linear displacement')
        ax1.grid()
        ax2 = ax1.twinx()
        plt2 = ax2.plot(T,M[:,1]*1000,'.-m', label="x' [mm/s]")
        y2max = np.abs(M[:,1]*1000).max()*1.1 if y2_max is None else y2_max
        ax2.set_ylim(-y2max, y2max)
        ax2.tick_params(axis='y', labelcolor='m')
        all_plots = plt1+plt2
        labs = [p.get_label() for p in all_plots]
        ax1.legend(all_plots, labs, loc='best', fontsize=10)

        ax3 = plt.subplot(122,label='ax1')
        plt3 = ax3.plot(T,np.degrees(M[:,2]),'.-g',label=r'$\theta$'+' [°]')
        y3max = np.abs(np.degrees(M[:,2])).max()*1.1 if y3_max is None else y3_max
        ax3.set_ylim(-y3max, y3max)
        ax3.set_xlabel('step')
        ax3.tick_params(axis='y', labelcolor='g')
        ax3.set_title('Angular displacement')
        ax3.grid()
        ax4 = ax3.twinx()
        plt4 = ax4.plot(T,np.degrees(M[:,3]),'.-r',label=r"$\theta'$"+ ' [°/s]')
        y4max = np.abs(np.degrees(M[:,3])).max()*1.1 if y4_max is None else y4_max
        ax4.set_ylim(-y4max, y4max)
        ax4.tick_params(axis='y', labelcolor='r')
        all_plots = plt3+plt4
        labs = [p.get_label() for p in all_plots]
        ax3.legend(all_plots, labs, loc='best', fontsize=10);
        
        plt.show()
    
        
if __name__ == '__main__':

    # since september 2020 the source tree is stored on the ENSAM gitlab server:
    # the editor VisualStudio code is configured tu run python programms from the <project_root> directory : 
    # all the pathes are relative to this directory.

    # scena path relative to the 'project root' directory:
    scene = "./cartpole/copsim/CartPole-v3.1_2021.ttt"
    wheel_diameter = 0      # not used for the cartpole
    dt=0.075,

    env   = CartPoleEnv_CopSim('v2', scene, wheel_diameter, dt,
                               headless=False, 
                               veloc_mag=0.2)    
    
    print(f"starting simulation with <{scene}>")

    # 1/ run in the 'free run' mode:
    data = env.runFree(nb_step=100, dt=0.07)
    title = '100 steps of free run with dt:70 ms'
    env.plot(data, y1_max=1, y2_max=1, title=title)

    # 2/ run in displacement mode: displ. step=0.2 cm, dt=0.02s => speed=10cm/sec
    data = env.runDispl(nb_step=250, dx=0.002, dt=0.02)
    title = '250 steps of displ. run with dx:0.2 cm and dt:20 ms'
    env.plot(data, title=title)

    # 3/ run in velocity mode
    data = env.runVeloc(nb_step=100, velocity=0.15, dt=0.1)
    title = '100 steps of velocity run with velocity:15 cm/sec and dt:100 ms'
    env.plot(data, title=title)
    
    env.close()
