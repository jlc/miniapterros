import numpy as np

def reward(self, action):
    raise Exception("do not use this reward")
    return 1

def reward1(self, action):
    reward = 1.0-0.5*float(action**2)
    return reward

def reward2(self, action):
    
    x, x_dot, theta, theta_dot = self.state
            
    a = abs(x)*np.pi/(self.x_threshold)
    reward_x = (np.cos(a)+1)/2*(abs(x)<=self.x_threshold)

    a = abs(theta)*np.pi/(self.theta_threshold_radians)
    reward_theta = (np.cos(a)+1)/2*(abs(theta)<=self.theta_threshold_radians)

    reward_action = 0
    if abs(self.prev_action - action) <  0.3 : 
        reward_action = 1

    reward = (reward_x + reward_theta + 3*reward_action)/5

    return reward        

def reward2_1(self, action):
    
    x, x_dot, theta, theta_dot = self.state
            
    a = abs(x)*np.pi/(self.x_threshold)
    reward_x = (np.cos(a)+1)/2*(abs(x)<=self.x_threshold)

    a = abs(theta)*np.pi/(self.theta_threshold_radians)
    reward_theta = (np.cos(a)+1)/2*(abs(theta)<=self.theta_threshold_radians)

    reward = (reward_x + reward_theta)/2

    return reward        

def reward3_1(self, action):
    """
    Use this reward to make the CartPole swing and maintain the pole vertically.
    """
    
    x, x_dot, theta, theta_dot = self.state
    
    a = abs(theta)*np.pi/(self.theta_threshold_radians)
    #reward_theta = (np.cos(a)+1)/2*(abs(x)<=self.theta_threshold_radians)
    #reward_theta = (360 - abs(np.degrees(theta)))/360

    theta_deg = np.degrees(theta)
    if theta_deg > 190:
        reward_theta = 1- (360 - theta_deg)/180
    elif theta_deg < 170:
        reward_theta = (180 - theta_deg)/180
    else:
        reward_theta = 0   
    r1 = reward_theta**2
    r2 = x_dot*theta_dot <= 0
    
    return (r1 + r2)/2  

def reward3_2(self, action):
    """
    Use this reward to make the CartPole swing and maintain the pole vertically.
    """
    
    x, x_dot, theta, theta_dot = self.state
    
    theta_deg = np.degrees(theta)
    if theta_deg > 180: theta_deg -= 360
    reward_theta = np.cos(np.radians(theta_deg/2))

    return reward_theta**2

 
 