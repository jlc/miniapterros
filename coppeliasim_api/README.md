This directory contains all the CoppeliaSim material :
* directory `docs`: holds docs usefull to understand how to use the CoppeliaSim simulator.
* directory `env`: holds Python modules and dynamic libraries needed to run CoppeliaSim and use it's Python API.
* directory `GettingStarted_with_API`: Jupyter notebooks showing how to use the CoppeliaSim Python API.
* directory `images` : holds images used in the jupyter notebooks of the directory `GettingStarted_with_API`.