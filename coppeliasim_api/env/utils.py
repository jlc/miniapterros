import sys, os, time

def find_root_dir():
    # initialize default path values:
    target_dir = "coppeliasim_api/env"
    root_dir = os.getcwd()
    copsim_env_path = target_dir

    if not os.path.isdir(target_dir):
        while not os.path.isdir(copsim_env_path):
            copsim_env_path = os.path.join('..', copsim_env_path)
        root_dir = copsim_env_path.replace(target_dir, "")

    # run notebook in root dir and add the required paths to sys.path:
    if  root_dir !=  os.getcwd():
        sys.path.append(root_dir)
        sys.path.append(target_dir)

    print(f"Found root directory: <{root_dir}>")
    
    return root_dir

if __name__ == "__main__":
    
    root_dir = find_root_dir()
    print(f"\tworking directory is now: <{os.getcwd()}>")
