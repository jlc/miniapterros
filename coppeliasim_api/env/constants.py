import pathlib

COPSIM_DIR = pathlib.Path('./CoppeliaSim_Edu_V4_3_0_rev12_Ubuntu20_04/')
COPSIM_APP_MacOS = "/Applications/coppeliaSim.app"

if __name__ == "__main__":
    print(f"COPSIM_DIR: {COPSIM_DIR}")
