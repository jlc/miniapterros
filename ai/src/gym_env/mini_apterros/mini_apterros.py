import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
from collections import deque

Z_QUE_LEN = 40  # length of the queue on Z

def reward(self, action, state, t):
    	raise RuntimeError("This method must be defined by yourself and set as a metadata")

class BasicMiniApterrosEnv(gym.Env):
    ''' The BasicMiniApterrosEnv class defines the Gym Environment for the miniapterros simulations.
    '''
    metadata = {'render.modes': ['human', 'rgb_array'],
                'video.frames_per_second': 10 ,
                'seed': None,
                'total_mass': None,
                'tau': None,
                'z_target': None,
                'reward_f': reward,
                'max_step': None,
                'static_frict': None
                }

    def __init__(self, initProp_input=0):

        # attributes to be set using metadata mechanism:
        self.state        = None    # state = [z, z_dot]
        self.prev_state   = None    # pre_state = [prev_z, prev_z_dot]
        self.seed_value   = None
        self.total_mass   = None    # kg, to be confirmed
        self.tau          = None    # time step in seconds
        self.z_target     = None    # target altitude in meter
        self.stage        = None    # the stage ('takeoff', 'station', 'landing')
        self.max_step     = None    # max number of steps to run
        self.static_frict = None    # static friction force in Newton

        # other attributes:
        self.gravity      = 9.81
        self.min_action   = -1.0  # 'action' comming from PPO is in range [-1, 1] which is
        self.max_action   =  1.0  # scaled later to [0, 1] by taking (action+1)/2
        self.initialized  = False
        self.stateArray   = []
        self.prev_action  = initProp_input
        self.cur_step     = 0
        self.cur_time     = 0       # current physical time, secondes
        
        self.action_space = spaces.Box(low=self.min_action,
                                       high=self.max_action,
                                       shape=(1,))
        
        self.observation_space = spaces.Box(np.array([0, -np.finfo(np.float32).max]),
                                            np.array([np.finfo(np.float32).max, np.finfo(np.float32).max]))
        self.z_que        = deque([], Z_QUE_LEN)    # moving window of 20 successive values of z
        self.viewer = None
        self.steps_beyond_done = None
        self.initialized = False

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def thrust(self, prop_input) :
        """ To converts the input into a force in Newton unit"""
        res = 4*self.gravity*3.71*prop_input #caracterisation 2021

        #res = 4*9.81*(3.09*prop_input) # D'après JLC
        #res = 4*9.81*(3.25e-2*prop_input*100 - 3.09e-1) # D'après étalonnage  de novembre 2019
        #res = 4*9.81*(0.0355*prop_input*100 - 0.52) # version précédente
        if res < 0 : res = 0
        return res

    def stepPhysics(self, thrust):
        
        def Cauchy_F(Z, t, T=thrust, m=self.total_mass, g=self.gravity, friction=self.static_frict):
            '''Cauchy Function:
               Z is vector [z, z_dot], the Cauchy function is f'(Z,t)
               and returns the vector Z_dot = (z_dot, z_dot_dot).
               In our problem, the 2nd order diff. equation is: z_dot_dot = (T-Friction)/m -g.
            '''
            return np.array([Z[1], (T-friction)/m-g])       

        t, dt = self.cur_time, self.tau

        # Z is the vector [z, z_dot]:
        Z  = np.array(self.state)
        K1 = Cauchy_F(Z, t)
        K2 = Cauchy_F(Z+K1*dt/2, t)
        K3 = Cauchy_F(Z+K2*dt/2, t)
        K4 = Cauchy_F(Z+K3*dt, t)
        Z += (K1 + 2*K2 + 2*K3 + K4)*dt/6

        if Z[0] < 0:   #Z must be positive
            Z[0] = 0
            Z[1] = - 0.5* Z[1]

        return Z

    def setAttributesFromMetadata(self): #inutile car on
        self.seed_value   = BasicMiniApterrosEnv.metadata['seed']
        self.total_mass   = BasicMiniApterrosEnv.metadata['total_mass']
        self.z_target     = BasicMiniApterrosEnv.metadata['z_target']
        self.tau          = BasicMiniApterrosEnv.metadata['tau']
        self.stage        = BasicMiniApterrosEnv.metadata['stage']
        self.max_step     = BasicMiniApterrosEnv.metadata['max_step']
        self.static_frict = BasicMiniApterrosEnv.metadata['static_frict']
        self.reward       = BasicMiniApterrosEnv.metadata['reward_f']
        
        print(f"seed        : {self.seed_value}")
        print(f"total_mass  : {self.total_mass}")
        print(f"z_target    : {self.z_target}")
        print(f"tau         : {self.tau}")
        print(f"stage       : {self.stage}")
        print(f"max_step    : {self.max_step}")
        print(f"static_frict: {self.static_frict}")

        self.initialized = True

    def step(self, action):
        if self.initialized is False: self.setAttributesFromMetadata()
    
        # transform 'action' from range [-1,1] to range[0,1]:
        action = (float(action)+1)/2

        # get thrust in Newton from action in range [0,1]:
        thrust = self.thrust(action)

        # compute self.state = [z, z_dot]
        #print(f"\tstate: {self.state}", end="")
        self.state = self.stepPhysics(thrust)
        z, _ = self.state
        self.z_que.append(z)
        #print(f"\tafter step: {self.state}")
       
        if self.cur_step == 0 : print(f" step: {self.cur_step}", end="")
        self.cur_step  += 1        
        self.cur_time  += self.tau
        
        done = False
        if (z > 1.2*self.z_target) or \
           (self.max_step is not None and self.cur_step == self.max_step):
            done   = True
            reward = 0
            print(f" step: {self.cur_step} done by z_target or max_step\n")
        elif len(self.z_que) == Z_QUE_LEN:
            if self.stage == "takeoff":
                if abs(self.z_target - np.array(self.z_que).mean()) <= 0.05*self.z_target:
                    done   = True
                    reward = 1
                    print(f" step: {self.cur_step} done by z_deque\n")
            elif self.stage == "takeoff":
                pass
        
        if not done:
            reward = self.reward(self, action, self.state, self.cur_time)
            self.prev_action = action
        elif self.steps_beyond_done is None:
            # Apterros crashed 
            self.steps_beyond_done = 0
            # reward = 1
        else:
            if self.steps_beyond_done == 0:
                logger.warn("""
You are calling 'step()' even though this environment has already returned
done = True. You should always call 'reset()' once you receive 'done = True'
Any further steps are undefined behavior.
                """)
            self.steps_beyond_done += 1
            reward = 0
            
        self.prev_state = self.state

        return np.array(self.state), reward, done, {'t':self.cur_time, 'z':z, 'PWM':100*action}
    
    def reset(self):
        '''rest is run at each beginning of episode.'''
        print("reset")
        self.seed(self.seed_value)
        if self.initialized is False: self.setAttributesFromMetadata()
        self.state = self.np_random.uniform(low=0.005, high=0.01, size=(2,))
        
        if self.stage == "landing":
            self.state = (self.z_target, 0.) # Départ à une altitude de z_target
        elif self.stage == "takeoff":
            self.state = (0., 0.)
        
        self.steps_beyond_done = None
        self.prev_state = self.state
        self.cur_step = 0
        self.cur_time = 0
        return np.array(self.state)

    def render(self, mode='human'): #P as de rendu mais la méthode doit exister
        pass
        return None
        

    def close(self):
        if self.viewer:
            self.viewer.close()
