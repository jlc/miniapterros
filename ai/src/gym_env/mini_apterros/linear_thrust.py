
# function describing the thrust

def compute_thrust(prop_input) :
    """ To converts the input into a force in Newton unit"""
    res = 4*9.81*(3.09*prop_input) # D'après JLC
    #res = 4*9.81*(3.25e-2*prop_input*100 - 3.09e-1) # D'après étalonnage  de novembre 2019
    #res = 4*9.81*(0.0355*prop_input*100 - 0.52) # version précédente
    if res < 0 : res = 0
    return res
