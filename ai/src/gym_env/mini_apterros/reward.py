#
# Define the method "reward" that will replace the one defined in
# the "BasicApterrosEnv" class in file mini_apterros.py :
#

def reward_takeoff(self, action, state, t):  

    PWM = 100*action
    prevPWM = 100*self.prev_action
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 0

   # penalize too high values of PWM:
    if PWM >= 80: 
        R -= 6
    else:
        R += 3

    # behavior when the target height is not yet reached
    if z <= 0.9*self.z_target:
        
        # reward small variation of PWM, penalize high PWM values:
        if 0 < abs(PWM - prevPWM) <= 5: 
            R += 3            
        else:
            R -= 2
            
        # reward a slow increase in height, penalize high decrease:
        if 0 < z - prev_z <= 0.1:
            R += 3
        else:
            R -= 2
    
    # behavior when the target height is reached:
    else:
        # reward keeping the vehicule at the target height, otherwize penalize
        if abs(self.z_target - z) <= 0.1: 
            R += 11  
        else:
            R -= 11     
    
    return R/14

def reward_landing(self, action, z, t):  
 
    PWM = 100*action
    prevPWM = 100*self.prev_action
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 0

    # reward values of PWM less than 85% :
    if PWM <= 80 : R += 4

    # reward small variation of PWM, penalize high PWM values:
    if abs(prevPWM - PWM) <= 5:
        R += 2
    else:
        R -= 2

    # reward a slow decrease in height, penalize high increase:
    if 0.01 < prev_z - z <= 0.05:
        R += 4
    else:
        R -= 4

    # almost on the ground reward turning off the turbines
    if z <= 0.01:
        if PWM <= 10 : R += 2

    return R/12
