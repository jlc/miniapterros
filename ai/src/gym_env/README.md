# Content

This directory contains Python programs to define :

- the class BasicApteroos derived from the Gym class `Env`: gives a very simplified model of the vehicule.
- the class `ContinuousCartPoleEnv` derived from the Gym class `Env` : a simplified model of the continuous CartPole.
- the function `compute_thrust` used to convert PWM into the thrust in Newton.
