This directory contains Python programs to train and test PPO networks to drive various environments (cartpole, BalancingRobot, MiniApterros...).

- `train_model_gym.py` : to train a neural network to drive a vehicule (continuousCartpole,  miniapterros...) within the native gym simulator (and redering). After computation, the program prints the elapsed training time and the path of the saved model (useful for the test stage). 
```bash
python ai/src/run/train_model_gym.py -h
usage: train_model_gym.py [-h] --config CONFIG

optional arguments:
  -h, --help       show this help message and exit
```

- `train_model_simul.py` : to train a neural network to drive a vehicule (cartpole, balancingrobot...) within a simulator given with the --simulator option (gym, copsim, pybullet...)
```bash
ython ai/src/run/train_model_simul.py -h
pybullet build time: May 20 2022 19:44:17
usage: train_model_simul.py [-h] [--vehicule VEHICULE | --config CONFIG] [--port PORT] [--traindir TRAINDIR] [--simulator SIMULATOR]

optional arguments:
  -h, --help            show this help message and exit
  --vehicule VEHICULE   keyword in (cartpole, balancingrobot, miniapterros)
  --config CONFIG       relative path name of the file '..._ppo_<SIMUL>.yaml'
  --port PORT           coppeliasim port, default is 20000
  --traindir TRAINDIR   Optional, the relative pathname of the training directory
  --simulator SIMULATOR Which simulator use in (gym, copsim, pybullet)
```
Examples:

__to train meural netowrks:__
- to train a PPO network with the Mini-APTERROS vehicule, using the configutaion file `apterros_ppo.yaml` :
```bash
python ai/src/run/train_model_gym.py --config ai/config/apterros_ppo.yaml 
```
- to train a PPO network to drive a CartPole simulated with the CoppeliaSIm simultor :the Mini-APTERROS vehicule, using the configuration file `pterros_ppo.yaml` :
```bash
python ai/src/run/train_model_simul.py --vehicule cartpole --simulator copsim 
```
```bash
python ai/src/run/train_model_simul.py --config  ai/models/cartpole/CartPoleEnv_CopSim_PPO_23-04-10_10-06-04/--simulator copsim
```

__to test trained neural networks__
(uses the 'last' link)


(uses the file 'config_ppo.yaml' of the specified directory 'CartPoleEnv_CopSim_PPO_23-04-10_10-06-04')
