import os, sys, time, shutil, yaml, pathlib

from ai.src.run.constants import EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME

from cartpole.copsim.CartpoleEnv_CopSim import CartPoleEnv_CopSim

from balancing_robot.copsim.BalancingRobotEnv_CopSim import BalancingRobotEnv_CopSim
from balancing_robot.pybullet.BalancingRobotEnv_Gym import BalancingRobotEnv_PyBullet

from stable_baselines3.common.callbacks import CheckpointCallback


import gym
from ai.src.gym_env.mini_apterros.reward import reward_takeoff, reward_landing



#
# Exemples: 
# 
# python ai/src/run/train_model_simul.py --vehicule cartpole --simulator copsim
#
#   lance l'entraîenement du carpole avec CoppeliaSim comme simulateur, en utilisant les fichiers config:
#       - ai/config/cartpole_ppo_copsim.yaml
#       - ai/config/cartpole_copsim/cartpole_copsim.yaml


def train_model(cfg_file: str, training_dir=None, simul_port=20000, ):
    """
    Train a PPO network to drive a given environment described by the "cfg_file" config file,
    and save the results and config for reproductibility.

    Parameters:
      cfg_file    : Path of the yaml configuration file for the agent and the environment.
      training_dir: If None, a training directory will be created, else the parameter is used
                    as the training directory location.
      simul_port  : Some simulators (copeliasim for exemple) require a connexion port number.
    
    Return: None
    """
    # import parameters from config files
    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())

    # Required parameters:
    try:
        environment  = cfg['env']['environment']
        cfg_file_env = cfg['env']['cfg_env']

        out_path     = cfg['train']['output_model_path']
        agent_type   = cfg['train']['agent_type']
        policy       = cfg['train']['policy']
        tot_steps    = cfg['train']['total_timesteps']
        save_freq    = cfg['train']['save_freq']
        nb_steps     = cfg['train']['n_steps']
        b_size       = cfg['train']['batch_size']
        nb_epochs    = cfg['train']['n_epochs']
        seed         = cfg['train']['seed']
        headless     = cfg['train']['headless'] 
    except:
        raise RuntimeError(f"Parameters missing in file <{cfg_file}>")
    
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # import agent
    if  agent_type == 'PPO':
        from stable_baselines3 import PPO as agent
    else:
        raise Exception("Agent <{agent_type}> not implemented")

    # Define the training dir if needed:
    if training_dir is None:
        experiment_time = time.localtime()
        # prepare directory for output
        experiment_id = "_".join([environment, agent_type,
                                 time.strftime("%y-%m-%d_%H-%M-%S", experiment_time)])
        training_dir = pathlib.Path(out_path)/experiment_id
    
    training_dir = pathlib.Path(training_dir)
    training_dir.mkdir(parents=True, exist_ok=True)

    # Create env for training
    if environment == 'CartPoleEnv_CopSim':
        scene     = cfg['env']['scene']
        version   = cfg_env['version'] 
        veloc     = cfg_env['velocity']
        dt        = cfg_env['dt']
        theta_lim = cfg_env['theta_lim']
        x_lim     = cfg_env['x_lim']   
        reward    = cfg_env['reward']    
        
        swing, theta0_deg = False, None
        if  cfg_env.get('swing', False) == True:
            swing = True
            theta0_deg = cfg_env['theta0_deg']
            print(f"running CarPoleEnv_CopSim with swing==True and theta0_deg={theta0_deg}")
        
        env = CartPoleEnv_CopSim(version, scene, dt,
                                 theta_lim=theta_lim,
                                 x_lim=x_lim,
                                 reward=reward,
                                 veloc_mag=veloc, 
                                 swing=swing,
                                 theta0_deg=theta0_deg,
                                 coppelia_sim_port=simul_port,
                                 headless=headless,
                                 verbose=0)

        shutil.copyfile('cartpole/copsim/rewards.py', training_dir/'rewards.py')

    elif environment == 'BalancingRobotEnv_CopSim':
        scene     = cfg['env']['scene']
        version   = cfg_env['version']
        veloc     = cfg_env['velocity']
        dt        = cfg_env['dt']
        diameter  = cfg_env['wheel_diam']  
        theta_lim = cfg_env['theta_lim']
        x_lim     = cfg_env['x_lim']   
        reward    = cfg_env['reward']     
        
        theta0_deg = None
        try:
            theta0_deg = cfg_env['theta0_deg'] 
        except:
            pass
        finally:
            print(f"found parameter 'theta0_deg':{theta0_deg}") 
        
        env = BalancingRobotEnv_CopSim(version, scene, diameter, dt,
                                       theta_lim=theta_lim,
                                       x_lim=x_lim,
                                       theta0_deg=theta0_deg,                                 
                                       reward=reward,
                                       veloc_mag=veloc, 
                                       coppelia_sim_port=simul_port,
                                       headless=headless,
                                       verbose=0)

        shutil.copyfile('balancing_robot/python/rewards.py', training_dir/'rewards.py')

    elif environment == 'BalancingRobotEnv_PyBullet':
        urdf_file = cfg['env']['urdf']
        version   = cfg_env['version']
        veloc     = cfg_env['velocity']
        dt        = cfg_env['dt']
        theta_lim = cfg_env['theta_lim']
        x_lim     = cfg_env['x_lim']   
        reward    = cfg_env['reward']   
        diameter  = cfg_env['wheel_diam']   
                
        theta0_deg = None
        try:
            theta0_deg = cfg_env['theta0_deg'] 
        except:
            pass
        finally:
            print(f"found parameter 'theta0_deg':{theta0_deg}") 

        env = BalancingRobotEnv_PyBullet(version, urdf_file, diameter, dt,
                                         theta_lim=theta_lim,
                                         x_lim=x_lim,
                                         theta0_deg=theta0_deg,                                 
                                         reward=reward,
                                         veloc_mag=veloc, 
                                         headless=headless, 
                                         verbose=0)

        shutil.copyfile('balancing_robot/python/rewards.py', training_dir/'rewards.py')

    elif environment == 'BasicMiniApterrosEnv':

        env = gym.make('MiniApterros-v0')

        env.metadata['total_mass']  = cfg_env['total_mass']
        env.metadata['tau']         = cfg_env['tau']
        env.metadata['z_target']    = cfg_env['z_target']
        env.metadata['stage']       = cfg_env['stage']
        env.metadata['seed']        = seed
        env.metadata['static_frict']= cfg_env['static_frict']
        env.metadata['max_step']    = cfg_env['max_step']
        if cfg_env['stage'] == "takeoff":
            env.metadata['reward_f']   = reward_takeoff    
        elif cfg_env['stage'] == "landing":
            env.metadata['reward_f']   = reward_landing
            
        shutil.copyfile('ai/src/gym_env/mini_apterros/reward.py', training_dir/'reward.py')
        
    else:
        raise Exception("Not implemented environment: <{environment}>")

    print("Using the environnement <{environment}>")
    print("\t type(env.action_space)     :", type(env.action_space))
    print("\t env.action_space           :", env.action_space)
    print("\t env.action_space.high      :", env.action_space.high)
    print("\t env.action_space.low       :", env.action_space.low)

    print("\t type(env.observation_space):", type(env.observation_space))
    print("\t env.observation_space      :", env.observation_space) 
    print("\t env.observation_space.high :", env.observation_space.high)
    print("\t env.observation_space.low  :", env.observation_space.low)

    # Copy precious files in experiment_dir:
    shutil.copyfile(cfg_file, training_dir/EXPERIMENT_CONFIG_FILENAME)
    shutil.copyfile(cfg_file_env, training_dir/ENVIRONMENT_CONFIG_FILENAME)
    
    # Prepare agent for training:
    model = agent(policy, 
                  env, 
                  n_epochs=nb_epochs,
                  n_steps=nb_steps,
                  batch_size=b_size,
                  seed=seed,
                  tensorboard_log=training_dir,
                  verbose=1)

    checkpoint_callback = CheckpointCallback(save_freq=save_freq, 
                                             save_path=training_dir/'ZIP')

    # train agent
    t0 = time.time()

    model.learn(total_timesteps=tot_steps, callback=checkpoint_callback)
    
    t = int(time.time()-t0)
    h = int(t//3600)
    m = int((t - h*3600)//60)
    print(f"Training elapsed time: {h:02d}h {m:02d}m")
  
    # save trained agent
    target_zip = os.path.join(training_dir, 'ZIP', 'model.zip')
    print(f"saving trained model in <{target_zip}>")
    model.save(target_zip)
    env.close()

    os.system(f"cd {out_path} && ln -s -f {os.path.basename(training_dir)} last")

    return training_dir
    

if __name__ == "__main__":
    # bloc main
    import argparse, sys
    parser = argparse.ArgumentParser()
    
    group  = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--vehicule', action="store", dest='vehicule', 
                         help="keyword in (cartpole, balancingrobot, miniapterros)")
    
    group.add_argument('--config', action="store", dest='config', 
                         help="relative path name of the file '..._ppo_<SIMUL>.yaml'")

    parser.add_argument('--port', action="store", dest='port', default="20000", type=int, 
                         help="coppeliasim port, default is 20000")
    
    parser.add_argument('--traindir', action="store", dest='traindir', 
                         help="Optional, the relative pathname of the training directory")
    
    parser.add_argument('--simulator', action="store", dest='simulator', type=str, required=True,
                         help="Which simulator use in (gym, copsim, pybullet)")
    
    args = parser.parse_args()
    assert args.simulator in ("gym", "copsim", "pybullet")
    
    if args.vehicule:
        config_path = f"ai/config/{args.vehicule}_ppo_{args.simulator}.yaml"        
    elif args.config:
        config_path = args.config
    else:
        parser.print_help()
        sys.exit(1)

    traindir = None
    if args.traindir: traindir = args.traindir

    print(f"running: train_model('{config_path}', simul_port={args.port}, training_dir='{traindir}')")

    train_model(config_path, simul_port=args.port, training_dir=traindir)
