#
# In this file we define two functions :
#   test_model     : to run an experiment described in cfg_file, save the results and config for reproducibility
#   evaluate_model : to display and save how a model performs when driving a given environment
#
# If the file runs directly with Python, the "main bloc" at the end is run.
#

import numpy as np
import gym
import os, sys, time, yaml
from os.path import realpath

from stable_baselines3.common.base_class import BaseAlgorithm
from ai.src.run.constants import TEST_DIR, EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME
from ai.src.run.constants import PERFORMANCE_REPORT_FILENAME

# import custom gym envs using CopSim as render 
from cartpole.copsim.CartpoleEnv_CopSim import CartPoleEnv_CopSim
from balancing_robot.copsim.BalancingRobotEnv_CopSim import BalancingRobotEnv_CopSim
from balancing_robot.pybullet.BalancingRobotEnv_Gym import BalancingRobotEnv_PyBullet

# JLC_TODO add apterros env when available

def evaluate_model(model: BaseAlgorithm, 
                   env: gym.Env, 
                   deterministic: bool = True, 
                   steps_nb: int = 1000,
                   verbose=False) -> dict:
    
    """
    Computes how a model performs on a given environment.

    Parameters:
      model: Instance of a stable_baselines3 model (neural network).
      env: Instance of a gym environment which is operated by the model.
      deterministic: Wether a random noise is applied to the model during evaluation or not.
      steps_nb: # max of steps for evaluating the model.
      verbose: Wether to print some debug informations or not.
    
    Returns: 
        A dict of {metric names:metric values}.
    """
    
    # initialisze some objects before the loop:
    env._max_episode_steps = steps_nb+1
    obs = env.reset()
    done, step_count, rewards, actions = False, 0, [], []
    
    if verbose: print(f"total_timesteps: {steps_nb}")

    # at each step the agent predicts the next action and updates the environment consequently
    while step_count < steps_nb:
        action, _ = model.predict(obs, deterministic=deterministic)
        obs, reward, done, info = env.step(action)
        rewards.append(float(reward))
        actions.append(float(action))
        step_count += 1
        time.sleep(env.dt)
        if verbose and (step_count % 10 == 0) : print("\r", step_count, end="")
        if done: break
    if verbose: print(" ...done!")
    if step_count == steps_nb+1: step_count -= 1
    
    return {"actions": actions,
            "mean_abs_actions": float(np.mean(np.abs(actions))),
            "rewards" : rewards,
            "reward_cum": sum(rewards),
            "rewards_mean": float(np.mean(rewards)),
            "rewards_std": float(np.std(rewards)),
            "last_step_count": step_count,
            "percent_completion": step_count/steps_nb}


def test_model(cfg_loc: str=None, 
               simul_port=20000, 
               display_plots: bool=False, 
               always_test=False, 
               verbose=True):
    """
    Run an experiment described in cfg_file, and save the results and config for reproducibility

    Parameters:
      param cfg_loc: Path of the directory that contains the yaml config files.
      simul_port:    May be needed by some simulators (copsim for exemple).
      display_plost: Wether to display the plots or not.
      alway_test:    If True, the test will be done even if the test directory already exists,
                     else the test is skipped.
      verbose:       Wether to print some debug informations or not.
    
    Returns: None
    """

    assert os.path.isdir(cfg_loc)
    real_dir  = realpath(cfg_loc)

    cfg_file = os.path.join(real_dir, EXPERIMENT_CONFIG_FILENAME)

    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())
 
    try:
        environment     = cfg['env']['environment']
        agent_type      = cfg['train']['agent_type']
        total_timesteps = cfg['eval']['total_timesteps']
        headless        = cfg['eval']['headless']
        deterministic   = cfg['eval']['deterministic']
        model_name      = cfg['eval']['model_name']
    except:
        raise RuntimeError(f"Parametres missing in file <{cfg_file}>")
    
    # JLC
    headless = True
    # JLC

    # Build the absolute pathname of the file 'model.zip':
    full_model_name = os.path.join(real_dir, 'ZIP', model_name)
    
    if not os.path.exists(full_model_name):
        # the training stopped before the the last epoch: no link 'model.zip'
        zip_file_list = os.listdir(os.path.join(real_dir, 'ZIP'))
        zip_file_list.sort()
        # we take the last zip file for the trained model:
        full_model_name = os.path.join(real_dir, 'ZIP', zip_file_list[-1])
        print(f"\t 'model.zip' not found, using <{full_model_name}>")
    
    # load the saved environment config file:
    if verbose: print(f"env. parameters loaded from <{os.path.basename(real_dir)}/{ENVIRONMENT_CONFIG_FILENAME }>")
    cfg_file_env = real_dir/ENVIRONMENT_CONFIG_FILENAME         # cfg['env']['cfg_env']
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # create the test directory if needed:
    test_dir = real_dir/TEST_DIR

    if always_test or not test_dir.exists() or (test_dir.exists() and not os.listdir(test_dir)): 
        # import agent
        if  agent_type == 'PPO':
            from stable_baselines3 import PPO as agent
        else:
            raise Exception("Not implemented agent : <{agent_type}>")
        
        # env for evaluation
        if environment == 'CartPoleEnv_CopSim':
            # load the saved parameters:
            scene      = cfg['env']['scene']    
            version    = cfg_env['version']
            veloc      = cfg_env['velocity']
            dt         = cfg_env['dt']
            theta_lim  = cfg_env['theta_lim']
            x_lim      = cfg_env['x_lim']   

            swing, theta0_deg = False, None            
            if  cfg_env.get('swing', False) == True:
                swing = True
                theta0_deg = cfg_env['theta0_deg']
                print(f"running CarPoleEnv_CopSim with swing==True and theta0_deg={theta0_deg}")
                
            reward    = cfg_env['reward']       
            if verbose: print(f"Will run CartPoleEnv_CopSim with veloc:{veloc}, dt:{dt} and version:{version}")
            
            eval_env = CartPoleEnv_CopSim(version, scene, dt,
                                          theta_lim=theta_lim,
                                          x_lim=x_lim,
                                          reward=reward,
                                          veloc_mag=veloc, 
                                          swing=swing,
                                          theta0_deg=theta0_deg,
                                          coppelia_sim_port=simul_port,
                                          headless=headless,
                                          verbose=0)

        elif environment == 'BalancingRobotEnv_CopSim':
            # load the saved parameters:
            scene      = cfg['env']['scene']
            
            veloc      = cfg_env['velocity']
            dt         = cfg_env['dt']
            version    = cfg_env['version']
            diameter   = cfg_env['wheel_diam']   
            theta_lim  = cfg_env['theta_lim']
            x_lim      = cfg_env['x_lim']   
            theta0_deg = cfg_env.get('theta0_deg', default=None)             
            reward     = cfg_env['reward']      
            
            if verbose: print(f"found parameter 'theta0_deg':{theta0_deg}") 

            eval_env = BalancingRobotEnv_CopSim(version, scene, diameter, dt, 
                                                theta_lim = theta_lim,
                                                x_lim = x_lim,
                                                theta0_deg=theta0_deg,                                        
                                                reward=reward,                                                
                                                veloc_mag=veloc,
                                                coppelia_sim_port=simul_port,                                          
                                                headless=headless, 
                                                verbose=0)

        elif environment == 'BalancingRobotEnv_PyBullet':
        
            urdf_file = cfg['env']['urdf']
        
            veloc      = cfg_env['velocity']
            dt         = cfg_env['dt']
            version    = cfg_env['version']
            theta_lim  = cfg_env['theta_lim']
            x_lim      = cfg_env['x_lim']   
            diameter   = cfg_env['wheel_diam']   
            theta0_deg = cfg_env.get('theta0_deg', default=None) 
            reward     = cfg_env['reward']      

            if verbose: print(f"found parameter 'theta0_deg':{theta0_deg}") 
            
            eval_env = BalancingRobotEnv_PyBullet(version, urdf_file, diameter, dt,
                                                  theta_lim,
                                                  x_lim,
                                                  theta0_deg=theta0_deg,                                        
                                                  reward=reward,                                                  
                                                  veloc_mag=veloc, 
                                                  headless=headless,
                                                  verbose=0)

        else:
            raise Exception("Not implemented environment: <{environment}>")
            
        
        if verbose: print(f"Loading model <{model_name}> from directory <{os.path.basename(real_dir)}>")
        model = agent.load(full_model_name)

        if not test_dir.exists(): test_dir.mkdir(parents=True)
    
        # Evaluate the trained agent    
        perf = evaluate_model(model, 
                              eval_env,
                              steps_nb=total_timesteps, 
                              deterministic=deterministic,
                              verbose=verbose)        
        perf["deterministic"] = deterministic
        
        with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'w') as outfile:
            yaml.dump(perf, outfile, default_flow_style=True)

        eval_env.close()

    else:
            if verbose: print(f"TEST directory already exists: reading file <{os.path.basename(real_dir)}/TEST/{PERFORMANCE_REPORT_FILENAME}>")
            with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'r') as f:
                perf = yaml.safe_load(f.read())

    for key, fmt in zip(('deterministic', 'mean_abs_actions', 'reward_cum', 'rewards_mean', 'last_step_count', 'percent_completion'),
                        ('', '.2f','.2f', '.2f', 'd', '.1f')):
        if verbose: print(f"{key:20s}: {perf[key]:{fmt}}")
    
    # plot rewards = f(time_step)
    import matplotlib.pyplot as plt
    actions = perf["actions"]
    rewards = perf['rewards']
    
    plt.figure(real_dir, figsize=(10,6))
    plt.subplots_adjust(hspace=.4)
    plt.subplot(2,1,1)
    plt.title(f"Graph of rewards <{os.path.basename(real_dir)}>")
    plt.plot(range(perf['last_step_count']), rewards, 'b', linewidth=0.4)
    #plt.xlabel("steps")
    plt.ylabel("reward", color='b')
    plt.ylim(0,2.)
    plt.grid()
    plt.twinx()
    
    cum = rewards[0]
    reward_cum = [cum]

    for r in rewards[1:]:
        cum = cum + r
        reward_cum.append(cum)
    plt.plot(range(perf['last_step_count']), reward_cum, 'm')
    plt.ylabel("cumulated reward", color='m')
    plt.ylim(0, perf['last_step_count'])
    

    plt.subplot(2,1,2)
    plt.title("Actions")
    plt.plot(range(perf['last_step_count']), actions, 'g', marker='.', markersize=3, linewidth=0.4)
    plt.xlabel("steps")
    plt.ylabel("network action in [-1. ; 1.]")
    plt.ylim(-1, 1)
    plt.grid()

    plt.savefig(test_dir/"rewards.png")

    if display_plots: plt.show()
    plt.close()
    
    return perf

if __name__ == "__main__":
    # main bloc
    import argparse, sys

    parser = argparse.ArgumentParser()
    group  = parser.add_mutually_exclusive_group()

    group.add_argument('--vehicule', action="store", dest='vehicule', 
                         help="keyword in (cartpole,balancingrobot,miniapterroos)")

    group.add_argument('--configloc', action="store", dest='configloc', 
                         help="location (relative path dir) of the file '..._ppo_copsim.ymaml' to read")

    parser.add_argument('--port', action="store", dest='port', default="20000", type=int, 
                         help="coppeliasim port, default is 20000")

    parser.add_argument('--displayplot', action="store_true", dest='displayplot', default=False, 
                         help="wether to display rewards graph or not")

    parser.add_argument('--quiet', action="store_true", dest='quiet', 
                         help="wether to print informations or not")                         

    parser.add_argument('--alwaystest', action="store_true", dest='alwaystest', default=False, 
                         help="wether to force test computation or not ")                         

    parser.add_argument('--simulator', action="store", dest='simulator', type=str, 
                         help="Which simulator use in (gym, copsim, pybullet)")
    
    args = parser.parse_args()

    if args.vehicule:
        config_path = f"ai/models/{args.vehicule}/last"
    elif args.configloc:
        config_path = args.configloc
    else:
        parser.print_help()
        sys.exit(1)

    displayplot = args.displayplot
    verbose = not args.quiet
    always_test = args.alwaystest

    #debug 
    print(f"running: test_model('{config_path}', display_plots={displayplot}, verbose={verbose})")

    test_model(config_path, display_plots=displayplot, always_test=always_test, verbose=verbose)
