import time
import os, sys, shutil, yaml
from pathlib import Path
from ai.src.run.constants import OUTPUT_DIR, EXPERIMENT_CONFIG_FILENAME
from ai.src.run.constants import ENVIRONMENT_CONFIG_FILENAME

from ai.src.gym_env.continuous_cartpole.continuous_cartpole import ContinuousCartPoleEnv
from ai.src.gym_env.mini_apterros.reward import reward_takeoff, reward_landing
import gym

from stable_baselines3.common.callbacks import CheckpointCallback # to save a checkpoint every N steps

def train_model(cfg_file: str):
    """
    Train a RL network on a given environment as described in cfg_file,
    and save the results and config for reproductibility

    Parameters:
      cfg_file    : Path of the yaml configuration file for the agent and the environment.
    
    Return: None
    """

    # import parameters from config files
    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())

    try:
        environment  = cfg['env']['environment']
        cfg_file_env = cfg['env']['cfg_env']

        out_path     = cfg['train']['output_model_path']
        agent_type   = cfg['train']['agent_type']
        policy       = cfg['train']['policy']
        tot_steps    = cfg['train']['total_timesteps']
        save_freq    = cfg['train']['save_freq']
        nb_steps     = cfg['train']['n_steps']
        b_size       = cfg['train']['batch_size']
        nb_epochs    = cfg['train']['n_epochs']
        seed         = cfg['train']['seed']
        
    except:
        raise RuntimeError("Parametres minesing in file <{cfg_file}>")
 
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # import agent
    if agent_type == 'PPO':
        from stable_baselines3 import PPO as agent
    else:
        raise Exception("Not implemented agent : <{agent_type}>")

    # Define the training directory
    experiment_time = time.localtime()
    experiment_id = "_".join([environment, agent_type,
                              time.strftime("%y-%m-%d_%H-%M-%S", experiment_time)])
    training_dir = Path(out_path)/experiment_id
    training_dir.mkdir(parents=True)

    # Create env for training    
    if environment == 'BasicMiniApterrosEnv':
        env = gym.make('MiniApterros-v0')
        env.metadata['total_mass']  = cfg_env['total_mass']
        env.metadata['tau']         = cfg_env['tau']
        env.metadata['z_target']    = cfg_env['z_target']
        env.metadata['stage']       = cfg_env['stage']
        env.metadata['seed']        = seed
        env.metadata['static_frict']= cfg_env['static_frict']
        env.metadata['max_step']    = cfg_env['max_step']
        if cfg_env['stage'] == "takeoff":
            env.metadata['reward_f']   = reward_takeoff    
        elif cfg_env['stage'] == "landing":
            env.metadata['reward_f']   = reward_landing
        
        shutil.copyfile('ai/src/gym_env/mini_apterros/reward.py', training_dir/'reward.py')

    elif environment == 'ContinuousCartPoleEnv': 
        env = gym.make("CartPole-v2")
        nb_steps   = cfg['train']['n_steps']
        tot_steps  = cfg['train']['total_timesteps']
        save_freq  = cfg['train']['save_freq']

        veloc   = cfg_env['velocity']
        dt      = cfg_env['dt']
        version = cfg_env['version']        
        
    else:
        raise Exception("Not implemented environment :"+cfg['env']['environment'])

    print(f"Using the environnement <{environment}>")
    print("\t type(env.action_space)     :", type(env.action_space))
    print("\t env.action_space           :", env.action_space)
    print("\t env.action_space.high      :", env.action_space.high)
    print("\t env.action_space.low       :", env.action_space.low)

    print("\t type(env.observation_space):", type(env.observation_space))
    print("\t env.observation_space      :", env.observation_space) 
    print("\t env.observation_space.high :", env.observation_space.high)
    print("\t env.observation_space.low  :", env.observation_space.low)

    # copy precious files in exprement_dir
    shutil.copyfile(cfg_file, training_dir/EXPERIMENT_CONFIG_FILENAME)
    shutil.copyfile(cfg_file_env, training_dir/ENVIRONMENT_CONFIG_FILENAME)

    # prepare agent for training
    model = agent(policy, 
                  env, 
                  n_epochs=nb_epochs,
                  n_steps=nb_steps,
                  batch_size=b_size,
                  seed=seed,
                  tensorboard_log=training_dir,
                  verbose=1)

    checkpoint_callback = CheckpointCallback(save_freq=save_freq, save_path=training_dir/'ZIP')

    #
    # train the agent:
    #
    
    t0 = time.time()

    model.learn(total_timesteps=tot_steps, callback=checkpoint_callback)

    t = int(time.time()-t0)
    h = int(t//3600)
    m = int((t - h*3600)//60)
    print(f"Training elapsed time : {h:02d}h {m:02d}m")
  
    # save trained agent
    target_zip = training_dir/'ZIP'/'model.zip'
    print(f"saving trained model in <{target_zip}>")
    model.save(target_zip)
    env.close()

    os.system(f"cd {out_path} && ln -s -f {training_dir.name} last")

    return training_dir

if __name__ == "__main__":
    # bloc main

    import argparse, sys

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', action="store", dest='config', required=True,
                         help="relative path name of the config file <..._ppo_....yaml>")

    args = parser.parse_args()
    config_path = args.config

    print(f"Running: train_model({config_path})")
    train_model(config_path)
