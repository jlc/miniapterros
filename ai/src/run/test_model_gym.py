#
# To train a PPO network to drive a vehicule(continuousCartpole,  miniapterros) 
# with the native gym simulator (and redering).
#
# In this file we define two functions:
#   - test_model    : to run an experiment described in cfg_file, save the results and config files.
#   - evaluate_model: to display and save how a model performs to drive a given environment.
#
# If the file is run directly by the Python interpretor, the "main bloc" at the end is run.
#

import numpy as np
import gym
import os, yaml
from os.path import realpath

from stable_baselines3.common.base_class import BaseAlgorithm
from ai.src.run.constants import TEST_DIR, EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME
from ai.src.run.constants import PERFORMANCE_REPORT_FILENAME

# import custom gym envs
from ai.src.gym_env.mini_apterros.reward import reward_takeoff, reward_landing

def evaluate_model(model: BaseAlgorithm, 
                   env: gym.Env, 
                   out_dir: str,
                   deterministic: bool = True, 
                   steps_nb: int = 1000,
                   verbose=False)  -> dict:
    
    """
    Computes how a model performs on a given environment.

    Parameters:
      model:    Instance of a stable_baselines3 model (neural network).
      env:      Instance of a gym environment which is operated by the model.
      out_dir:  The directory path where to write the ouput of gym Monitor.
      deterministic: Wether a random noise is applied to the model during evaluation or not.
      steps_nb: # of simulation steps for evaluation.
      verbose:  Wether to print some debug informations or not.
      
    returns: 
      A dict of {metrics_name: metrics_values}
    """
    
    # initialisze some objects before the loop:
    env._max_episode_steps = steps_nb
    #env = gym.wrappers.Monitor(env, os.path.join(out_dir,  'GymMonitor'), force=True)
    obs = env.reset()
    done,  step_count, rewards, actions = False, 0, [], []
    env_name = env.unwrapped.spec.id
    plot_value = None
    
    if verbose: print(f"total_timesteps: {steps_nb}")
    
    if env_name == "MiniApterros-v0":
        list_PWM, list_z, list_z_dot, list_t = [], [], [], [] # for plot after the testing
    
    # at each step, will make the agent predict the next action and update the environment consequently
    while step_count < steps_nb:
        action, _ = model.predict(obs, deterministic=deterministic)
        obs, reward, done, info = env.step(action)
        rewards.append(float(reward))
        actions.append(action)
        step_count += 1
        if verbose and (step_count % 10 == 0) : print("\r", step_count, end="")
        if env_name == "MiniApterros-v0":
            t, PWM, z = info['t'], info['PWM'], info['z']
            z_dot = obs[1]
            print(f"t: {t:4.2f}, a: {action[0]:6.3f}, PWM: {PWM:04.1f}, z: {z:5.3f}", flush=True)
            list_PWM.append(PWM) 
            list_z.append(z)
            list_z_dot.append(z_dot)
            list_t.append(t)
        if done: break

    
    if env_name == "MiniApterros-v0":
        plot_value = {"PWM" : list_PWM,
                      "altitude": list_z,
                      "altitude_dot": list_z_dot,
                      "Time": list_t}
                    
    return {"actions": actions,
            "rewards" : rewards,
            "rewards_mean": float(np.mean(rewards)),
            "rewards_std": float(np.std(rewards)),
            "last_step_count": step_count,
            "percent_completion": step_count/steps_nb,
            "value_to_plot" : plot_value}


def test_model(cfg_loc: str,
               step_saved = 0,
               always_test=False, 
               noisy = None,
               verbose=True):
    """
    Run an experiment described in cfg_file, and save the results and config for reproducibility

    Parameters:
      cfg_loc:      Path of the directory that contains the yaml config files.
      step_saved:   step of the saved NN to test
      alway_test:   If True, the test will be done even if the test directory already exists,
                    else the test is skipped.
      noisy:        
      verbose:      Wether to print some debug informations or not.

    Return: None
    """

    print(f"Running test_model with cfg_loc: <{cfg_loc}>")

    assert os.path.isdir(cfg_loc)
    real_dir  = realpath(cfg_loc)
    cfg_file = os.path.join(real_dir, EXPERIMENT_CONFIG_FILENAME)

    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())
    
    try:
        environment     = cfg['env']['environment']
        agent_type      = cfg['train']['agent_type']
        total_timesteps = cfg['eval']['total_timesteps']
        deterministic   = cfg['eval']['deterministic']
        model_name      = cfg['eval']['model_name']
    except:
        raise RuntimeError(f"Parametres missing in file <{cfg_file}>")

    if noisy :
        deterministic = False
    else:
        deterministic = True
        
    # Build the absolute pathname of the file 'model.zip':
    if step_saved == 0:
        model_name = os.path.join('ZIP', model_name)
    else:
        model_name = os.path.join(f'ZIP/rl_model_{step_saved}_steps.zip')
        
    full_model_name = os.path.join(real_dir, model_name)
        
    if verbose: print(f"Looking for model <{model_name}>... ", end="")
    
    if not os.path.exists(full_model_name):
        print(" not found: ", end="")
        zip_dir = os.path.join(real_dir, 'ZIP')
        # the training stopped before the the last epoch: no link 'model.zip'
        zip_file_list = [(f, os.path.getmtime(os.path.join(zip_dir,f))) for f in os.listdir(zip_dir)]
        zip_file_list.sort(key=lambda elem: elem[1])
        # we take the last zip file for the trained model:
        model_name = os.path.join('ZIP', zip_file_list[-1][0])
        full_model_name = os.path.join(real_dir, model_name)
        print(f"using <{model_name}>")
    else:
        print(" found.")

    # load the saved environment config file:
    if verbose: print(f"env. parameters loaded from <{os.path.basename(real_dir)}/{ENVIRONMENT_CONFIG_FILENAME }>")
    cfg_file_env = real_dir/ENVIRONMENT_CONFIG_FILENAME         # cfg['env']['cfg_env']
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # create the test directory if needed:
    test_dir = real_dir/TEST_DIR
    if step_saved:
        test_dir = test_dir/f"rl_model_{step_saved}_steps"

    if always_test or not test_dir.exists() or (test_dir.exists() and not os.listdir(test_dir)): 
        # import agent
        if  agent_type == 'PPO':
            from stable_baselines3 import PPO as agent
        else:
            raise Exception("Not implemented agent : <{agent_type}>")

        # Create env for evaluation
        # WARNING : the syntaxe gym.make('CartPole-v2') or gym.make('MiniApterros-v0') requires the 
        # modification of the directory /.../miniconda3/envs/pyml/lib/python3.x/site-packages/gym/ 
        # according to ai/docs/update_gym_env.pdf.
        
        if environment in ('CartPole-v2', 'ContinuousCartPoleEnv'):
            
            eval_env = gym.make('CartPole-v2')

        elif environment == 'BasicMiniApterrosEnv':

            eval_env = gym.make('MiniApterros-v0')
            eval_env.metadata['total_mass'] = cfg_env['total_mass']
            eval_env.metadata['tau']        = cfg_env['tau']
            eval_env.metadata['z_target']   = cfg_env['z_target']
            stage = cfg_env['stage']
            eval_env.metadata['stage']      = stage
            eval_env.metadata['static_frict']   = cfg_env['static_frict']
            eval_env.metadata['max_step'] = cfg_env['max_step']
            if stage == "takeoff":
                eval_env.metadata['reward_f']   = reward_takeoff    
            elif stage == "landing":
                eval_env.metadata['reward_f']   = reward_landing
        
        else:
            raise Exception("Not implemented environment: <{environment}>")
    
        model = agent.load(full_model_name)

        if not test_dir.exists(): test_dir.mkdir(parents=True)

        # Evaluate the trained agent    
        perf = evaluate_model(model, 
                              eval_env,
                              out_dir=test_dir,
                              steps_nb=total_timesteps, 
                              deterministic=deterministic,
                              verbose=verbose)        
        perf["deterministic"] = deterministic
        
        with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'w') as outfile:
            yaml.dump(perf, outfile, default_flow_style=True)

        eval_env.close()

    else:
        if verbose: print(f"TEST directory already exists: reading file <{os.path.basename(real_dir)}/TEST/{PERFORMANCE_REPORT_FILENAME}>")
        with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'r') as f:
            perf = yaml.unsafe_load(f.read())

    if environment == 'BasicMiniApterrosEnv':
        import matplotlib.pyplot as plt
        plt.figure("Performances du  RN",[18,6])
        title = f"Training from {os.path.basename(cfg_loc)}/ZIP/{os.path.basename(realpath(full_model_name))}"
        title += f" [deterministic={deterministic}]"
        plt.suptitle(title)
        plt.subplots_adjust(hspace=0.4)
        plt.subplot(2,2,1)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['PWM'], '.-' )
        plt.title("PWM(t)")
        plt.ylim(0,100)
        plt.xlabel("time [s]")
        plt.ylabel("percent")
        plt.grid(True)

        plt.subplot(2,2,2)
        plt.plot(perf['value_to_plot']['Time'], perf['rewards'], '.-' )
        plt.title("Reward(t)")
        plt.xlabel("time [s]")
        plt.ylabel("reward")
        plt.grid(True)

        plt.subplot(2,2,3)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['altitude'], '.-' )
        plt.title("z(t)")

        plt.xlabel("time [s]")
        plt.ylabel("distance to ground [m]")
        plt.ylim(0,1.2)
        plt.grid(True)

        plt.subplot(2,2,4)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['altitude_dot'], '.-' )
        plt.title("z_dot(t)")
        plt.xlabel("time [s]")
        #plt.ylim(0,1)
        plt.ylabel("verticale velocity [m/s]")
        plt.grid(True)

        plt.savefig(os.path.join(test_dir, "eval.png"))
        plt.show()


if __name__ == "__main__":
    # main bloc

    import argparse, sys

    parser = argparse.ArgumentParser()
    parser.add_argument('--configdir', action="store", dest='config_dir', required=True,
                         help="directory (relative path) where the config file <..._ppo_....yaml> lives")
    
    parser.add_argument('--stepsaved', action="store", dest='step_saved', type=int, default=0,
                         help="step of the saved NN to test")

    parser.add_argument('--quiet', action="store_true", dest='quiet', 
                         help="wether to print informations or not")        
    
    parser.add_argument('--noisy', action="store_true", dest='noisy', 
                         help="wether to test swith deterministic False or True")              
    
    parser.add_argument('--alwaystest', action="store_true", dest='alwaystest', default=False, 
                         help="wether to force test computation or not ")                         

    args = parser.parse_args()
    config_path = args.config_dir
    verbose = not args.quiet
    always_test = args.alwaystest
    step_saved = args.step_saved
    noisy = args.noisy
    
    print(f"Running: test_model({config_path}, step_saved={step_saved}, always_test={always_test}, noisy={noisy}, verbose={verbose}")

    test_model(config_path, step_saved=step_saved, always_test=always_test, noisy=noisy, verbose=verbose)
