#
# Run this program to quickly tests if you can can train a PPO network from "stable baseline3" 
# with the simulation of the CartPole in CoppeliaSim.
#
# Nota: PPO can give discrete or continuous actions. 
#       For CartPole in CoppeliaSim, continuous action is used .
#
# Two Python programs are used:
#   30_train_save_PPO_CartPole-v2_copsim.py  to train the network and save its weights.
#   31_load_test_CartPole-v2_copsim.py   to load a trained model and test its bahaviour.
#
from time import time
from stable_baselines3 import PPO
from cartpole.copsim.CartpoleEnv_CopSim import CartPoleEnv_CopSim
import yaml

# in this simple test, the path to the scene file is hardcoded:
scene = "cartpole/copsim/CartPole-v3.2_2021.ttt"

# import parameters from cfg file
with open('ai/config/cartpole_copsim/cartpole_copsim.yaml', 'r') as f:
    cfg   = yaml.safe_load(f.read())
veloc     = cfg['velocity']
dt        = cfg['dt']
version   = cfg['version']
reward    = cfg['reward']
theta_lim = cfg['theta_lim']
x_lim     = cfg['x_lim']

print(f"running CartPoleEnv_CopSim with veloc:{veloc}, dt:{dt} and version:{version}")

# import Cart Pole environment from scene in .ttt file
env = CartPoleEnv_CopSim(version, scene, 0, dt,
                         theta_lim=theta_lim,
                         x_lim=x_lim,
                         veloc_mag=veloc,
                         reward=reward,
                         headless=True,
                         verbose=1)

# create and train the AI of the robot using Reinforcment Learning and Proximal Policy Optimisation (PPO)
# default parameters of PPO cosntructor:
#
# learning_rate=0.0003, 
# n_steps=2048, 
# batch_size=64, 
# n_epochs=10

model = PPO('MlpPolicy', 
            env, 
            verbose=1, 
            n_epochs=20,
            batch_size=128,
            n_steps=1024,
            tensorboard_log="ai/models/test_PPO_CartPole-v2_copsim")

# train the network
t0 = time()
model.learn(total_timesteps=100000)

t = int(time()-t0)
h = int(t//3600)
m = int((t - h*3600)//60)
print(f"Training elapsed time : {h:2d}h {m:2d}m")

# save trained agent
model.save('ai/models/test_PPO_CartPole-v2_copsim/model.zip')

env.close()
