# Directory `test` in `<project_root_dir>/ai/src`

This directory contains some simple Python programs that can be used to **quickly test** :

- the configuration of your _Visual Studio EDI_
- your installation of _stable baseline3_
- your installation of _CoppeliaSim_ and its _Python API_.

## 1/ Test PPO with CartPole-v1 (discrete action) in Gym simulator

- `10_train_save_PPO_CartPole-v1.py`  to train the PPO network and save its weights.
- `11_load_test_PPO_CartPole-v1.py`   to load the trained PPO neural wights and test its behavior.

## 2/ Test PPO with CartPole-v2 (continuous action) in Gym simulator

- `20_train_save_PPO_CartPole-v2.py`  to train the PPO network and save its weights.
- `21_load_test_PPO_CartPole-v2.py`   to load the trained PPO neural wights and test its behavior.

## 3/ Test PPO with CartPole-v2 (continuous action) in CoppeliaSim simulator

- `30_train_save_PPO_CartPole-v2_copsim.py`  to train the PPO network and save its weights.
- `31_load_test_PPO_CartPole-v2_copsim.py`   to load the trained PPO neural wights and test its behavior.

## 4/ Test DQN with LunarLander-v2 in Gym simulator

- `50_train_save_DQN_LunarLander-v2.py`  to train the DQN network and save its weights.
- `51_load_test_DQN_LunarLander-v2.py`   to load the trained DQN neural wights and test its behavior.

## 5/ Test PPO with LunarLander-v2 in Gym simulator

- `6à_train_save_PPO_LunarLander-v2.py`  to train the PPO network and save its weights.
- `61_load_test_PPO_LunarLander-v2.py`   to load the trained PPO neural wights and test its behavior.

## 6/ Test PPO with LunarLanderContinuous-v2 in Gym simulator

- `6à_train_save_PPO_LunarLanderContinuous-v2.py`  to train the PPO network and save its weights.
- `61_load_test_PPO_LunarLanderContinuous-v2.py`   to load the trained PPO neural wights and test its behavior.
