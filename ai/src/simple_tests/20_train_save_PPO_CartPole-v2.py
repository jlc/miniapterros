#
# Run this program to quickly tests if you can train a PPO network from "stable baseline3" 
# with the a custom Gym environnement like the "continuous_cartpole"
#
# Nota: PPO can give discrete or continuous actions. 
#       For CartPole-v2 continuous action in [-1, 1] is used .
#
# Two Python programs are used:
#   20_train_save_PPO_CartPole-v2.py  to train the network and save its weights.
#   21_load_test_PPO_CartPole-v2.py   to load a trained model and test its bahaviour.
#

from time import time
import gym
from stable_baselines3 import PPO
from ai.src.visu.gym_render import model_render

# creates the custom cartpole (robot with lateral movement and a stick) environment

# WARNING : the syntaxe "gym.make('CartPole-v2')" requires the modification of your directory 
# ..somewhere../miniconda3/envs/pyml/lib/python3.x/site-packages/gym/ (see <project_root>/README.md)
env = gym.make('CartPole-v2')

# create and train the AI using Deep Reinforcement Learning (DRL) and Proximal Policy Optimisation (PPO)
# see https://stable-baselines3.readthedocs.io/en/master/modules/ppo.html
model = PPO('MlpPolicy', 
            env, 
            verbose=1,
            tensorboard_log="ai/models/test_PPO_CartPole-v2")

# train the network
t0 = time()
model.learn(total_timesteps=100000)

t = int(time()-t0)
h = int(t//3600)
m = int((t - h*3600)//60)
print(f"Training elapsed time : {h:2d}h {m:2d}m")

# save trained model
model.save("ai/models/test_PPO_CartPole-v2/model.zip")
