#
# Run this program to quickly load and test a trained PPO network from "stable baseline3" 
# with a simple Gym environnement like "CartPole-v2". this program will eender graphically 
# with CoppeliaSim the behaviour of a trained PPO stable_baselines3 model.
#
# Two Python programs are used:
#   30_train_save_PPO_CartPole-v2_copsim.py  to train the network and save its weights.
#   31_load-test_PPO_CartPole-v2_copsim.py   to load a trained model and test its bahaviour.
#

import yaml, os
from stable_baselines3 import PPO
from cartpole.copsim.CartpoleEnv_CopSim import CartPoleEnv_CopSim
from ai.src.visu.copsim_render import model_render

# import parameters from cfg file
with open('ai/config/cartpole_copsim/cartpole_copsim.yaml', 'r') as f:
    cfg   = yaml.safe_load(f.read())
veloc     = cfg['velocity']
dt        = cfg['dt']
version   = cfg['version']
reward    = cfg['reward']
theta_lim = cfg['theta_lim']
x_lim     = cfg['x_lim']

print(f"running CartPoleEnv_CopSim with veloc:{veloc}, dt:{dt} and version:{version}")

# import trained PPO agent: # load the file <mdel_name>.zip that is in the directorory <model_dir>
model_name = "model.zip"
model_path = "ai/models/test_PPO_CartPole-v2_copsim"
model_path_name = os.path.join(model_path, model_name)
print(f"loading model <{model_name}> form directory <{model_path}>")
model = PPO.load(model_path_name)

# in this simple test, the path to the scene file is hardcoded, relative to
# the <root_project_dir>:
scene = "cartpole/copsim/CartPole-v3.2_2021.ttt"
env = CartPoleEnv_CopSim(version, scene, 0, dt,
                         theta_lim=theta_lim,
                         x_lim=x_lim,
                         veloc_mag=veloc,
                         reward=reward,
                         headless=False,
                         verbose=0)

# display the trained model in Coppelia Sim
env._max_episode_steps = None

# display how the trained model performs and save it as a video:
model_render(model, env, output_dir="ai/out/quick_tests/test_PPO_CartPole-v2_copsim", steps_nb=300, deterministic=False)

