#
# Run this program to quickly load and test a trained DQN network from "stable baseline3" 
# with a simple Gym environnement like "LunarLander-v2". 

# Two Python programs are used:
#   50_train_save_DQN_LunarLander-v2.py  to train the network and save its weights.
#   51_load_test_DQN_LunarLander-v2.py   to load a trained model and test its bahaviour.
#

import gym
from stable_baselines3 import PPO
from ai.src.visu.gym_render import model_render

# Create environment
env = gym.make('LunarLander-v2')

# Load the trained agent
model = PPO.load("ai/models/test_PPO_LunarLander-v2/model.zip")

# display how the trained model performs and save it as a video in ./out
model_render(model, env,  output_dir="ai/out/quick_tests/test_PPO_LunarLander-v2", step_nb=10000)

