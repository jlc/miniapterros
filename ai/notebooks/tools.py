import yaml, multiprocessing, subprocess, pathlib, datetime, time, os

from ai.src.run.constants import CONFIG_DIR, MODEL_DIR

def build_name_with_params(run, seed, theta_lim, x_lim, velocity, reward):
    '''
    Build a name with parameters value : <param1_param2_param3...>
    '''
    name = f"{run:02d}"
    name += f"_{seed}"
    name += f"_{theta_lim:02d}"
    name += f"_{x_lim:0.2f}"
    name += f"_{velocity:0.2f}"
    name += f"_{reward}"
    return name
    

def build_yaml_files_and_arguments_list(vehicule: str, simul:str, params:dict, nb_run:int = 1, start_port:int = 20000):
    ''' 
    Copy the content of the template "config env" file in a custom filename prefixed by the port number.
    Builds the list of the Python arguments to run the PPO training with the corresponding yaml 
    configuration files.
          
    Parameters:
      vehicule: a key in ('balancingrobot', 'cartpol')
      simul: the name of the simulator in ('copsim', 'pybullet')
      params: the dictionnary pf the parameters ('seeds', 'theta_lim', 'x_lim'...)
      nb_run: the number of trainings to run with the same parameters to do some statistics...
      start_port: used by coppeliaSim or to build uniq file names.
        
    Return: the list of python commandes.
    '''    
    port = start_port
    
    config_env_name = f"{vehicule}_{simul}.yaml"
    config_ppo_name = f"{vehicule}_ppo_{simul}.yaml"

    config_ppo_path = os.path.join(CONFIG_DIR, config_ppo_name)
    config_env_path = os.path.join(CONFIG_DIR, f"{vehicule}_{simul}", config_env_name)

    path_to_train_python_script = "ai/src/run/train_model_simul.py"
    
    uniq_name = time.strftime("%y-%m-%d_%H-%M-%S", time.localtime())

    cmd_list = []

    for run in range(1,nb_run+1):
        for seed in params['seeds']:
            for theta in params['theta_lim']:
                for x in params['x_lim']:
                    for veloc in params['velocity']:
                        for reward in params['rewards']:

                            # Open the <ppo config yaml> file, change the name of the <env config ymal> file
                            # and write it back with a custom name:
                            
                            #print(f"reading <{config_ppo_path}>")
                            with open(config_ppo_path, 'r') as f: 
                                ppo_cfg = yaml.safe_load(f.read())
                            
                            custom_cfg_env_name = f"{port}_{vehicule}_{simul}.yaml"
                            custom_cfg_env_path = os.path.join(CONFIG_DIR, f"{vehicule}_{simul}", custom_cfg_env_name)

                            #print(f"custom_cfg_env_path: <{custom_cfg_env_path}>")
                            ppo_cfg['env']['cfg_env'] = custom_cfg_env_path
                            ppo_cfg['train']['seed']  = seed
                            custom_cfg_ppo_name = f"{port}_{vehicule}_ppo_{simul}.yaml"
                            custom_cfg_ppo_path = os.path.join(CONFIG_DIR, custom_cfg_ppo_name)

                            #print(f"writing  <{custom_cfg_ppo_path}>")
                            with open(custom_cfg_ppo_path, "w", encoding="utf8") as f: 
                                yaml.dump(ppo_cfg, f, default_flow_style=False)
                            
                            # Now modify the env parameters dictionnary and write the custom <config env yaml> file:
                            #print(f"reading <{config_env_path}>"):
                            with open(config_env_path) as f: 
                                env = yaml.safe_load(f.read())                        
                            env["theta_lim"] = theta
                            env["x_lim"]     = x
                            env["velocity"]  = veloc
                            env["reward"]    = reward       
                            #print(f"writing <{custom_cfg_env_path}>")
                            with open(custom_cfg_env_path, "w", encoding="utf8") as f: 
                                yaml.dump(env, f, default_flow_style=False)

                            env_agent_uniq_name = "_".join([ppo_cfg['env']['environment'], ppo_cfg['train']['agent_type'], uniq_name])
                            training_dir = os.path.join(MODEL_DIR, vehicule, env_agent_uniq_name, build_name_with_params(run, seed, theta, x, veloc, reward))
                            args = ['python', 
                                    path_to_train_python_script,
                                    '--config', custom_cfg_ppo_path,
                                    '--simulator', simul,
                                    '--traindir', training_dir,
                                    '--port',  str(port)]               
                            cmd_list.append(args)
                            
                            port += 1

    return cmd_list

def run_training_multithread(vehicule:str, simul:str, cmd_list:list, nb_max_proc:int = None):
    ''' 
    Run python processes to train neural network on the different threads of the computer CPU.
    As many processes as the number of threads are launched, and each time a process finishes a new one is launched.
       
    Parameters:
      vehicule:    a key in ('balancingrobot', 'cartpol')
      simul:       the name of the simulator in ('copsim', 'pybullet')
      cmd_list:    the list of python commnads to run
      nb_max_proc: if given, overrides the total number of simultaneaous processes deduced from 
                   multiprocessing.cpu_count()
        
      Return: None.
    '''

    if nb_max_proc is None:
        nb_cores = multiprocessing.cpu_count()
        print(f"number of cores : {nb_cores}")
        nb_subprocess = nb_cores - 1
    else:
        print(f"number of processes requested: {nb_max_proc}")
        nb_subprocess = nb_max_proc

    #import sys; sys.exit()
    
    process_list  = [0]*nb_subprocess   # The running processes queue
    proc_dict = {}                      # The process parameters dictionnary

    s, still_working, t0 = 0, False, int(time.time())

    # some constants needed to build paths:
    config_ppo_name = f"{vehicule}_ppo_{simul}.yaml"
    config_ppo_path = os.path.join(CONFIG_DIR, config_ppo_name)
    with open(config_ppo_path, 'r') as f: 
        ppo_cfg = yaml.safe_load(f.read())
    
    environment  = ppo_cfg['env']['environment']
    agent_type   = ppo_cfg['train']['agent_type']
    out_path     = ppo_cfg['train']['output_model_path']

    # the loop to launch each training process in a thread:
    while s < len(cmd_list) or still_working:
        still_working = False
        #
        # Scan the processList queue to see wether to create a new process or 
        # to wait for an existing process termination:
        #
        for i, sub_proc in enumerate(process_list):
            if sub_proc == 0:  
                # Processing a new process:
                if s < len(cmd_list):
                    
                    # get the python command for this process
                    cmd = cmd_list[s]

                    # check whether the option '--traindir' is in cmd, and get the directory path.
                    # If not, build a custom directory name. 
                    training_dir = None
                    try:
                        index_training_dir_option = cmd.index('--traindir')
                        training_dir_str = cmd[index_training_dir_option + 1]
                        training_dir = pathlib.Path(training_dir_str)
                    except:                    
                        # if no training directory is in cmd, build one:
                        experiment_time = time.localtime()
                        experiment_id = "_".join([environment, agent_type,
                                                time.strftime("%y-%m-%d_%H-%M-%S", experiment_time)])
                        training_dir = pathlib.Path(out_path)/experiment_id
                    
                        training_dir_str = str(training_dir)
                        cmd.extend(['--traindir', training_dir_str])

                    training_dir.mkdir(parents=True, exist_ok=True)
                    redirection_file = open(os.path.join(training_dir_str, "training.log"), 'w')
                    
                    # Preapre the subprocess:
                    proc = subprocess.Popen(cmd, 
                                            stdout=redirection_file, 
                                            stderr=redirection_file,
                                            env=dict(os.environ, PYTHONPATH=f'{os.getcwd()}:$PYTHONPATH'))
                    print(f"process <{i}:{proc.pid}> starts in <{training_dir_str}>")
                    process_list[i] = proc
                    s += 1
                    still_working = True
                    proc_dict[proc.pid] = [training_dir, redirection_file, int(time.time()), cmd]
                    time.sleep(1)
            else: 
                # Processing an existing process:
                if sub_proc.poll() != None:
                    # the process has finished:
                    # retrieve info associated to the process:
                    training_dir, redirection_file, tStart, cmd = proc_dict.get(sub_proc.pid, [None, None, None, None])
                    assert(training_dir is not None)
                    duration=  str(datetime.timedelta(seconds=int(time.time())-tStart))
                    # move or close some files, as needed:
                    redirection_file.close()
                    print(f"process {sub_proc.pid} has finished (duration: {duration} sec")
                    
                    # remove remaining unused files:
                    custom_cfg_ppo_path = cmd[3]
                    if custom_cfg_ppo_path.endswith(f'ppo_{simul}.yaml') and os.path.exists(custom_cfg_ppo_path):
                        with open(custom_cfg_ppo_path, 'r') as f: 
                                ppo_cfg = yaml.safe_load(f.read())
                        custom_cfg_env_path = ppo_cfg['env']['cfg_env']
                        os.remove(custom_cfg_ppo_path)
                        os.remove(custom_cfg_env_path)
                            
                    # mark the process as finished in list processList:
                    process_list[i] = 0
                else:
                    still_working = True

        time.sleep(10)

    totDuration=str(datetime.timedelta(seconds=int(time.time())-t0))
    print(f"End of trainings! total duration: {totDuration}")
    
    
if __name__ == "__main__":
    
    print(build_name_with_params(1,12345, 7, 10, 3, 'reward_1'))