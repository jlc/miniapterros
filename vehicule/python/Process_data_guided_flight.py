import sys, os
import numpy as np 
import matplotlib.pyplot as plt

# read data from text files:

data_dir = input("path to data files: ")
text_file = [f for f in sorted(os.listdir(data_dir)) if f.endswith(".txt")]
for i, file in enumerate(text_file):
    print(f'{i+1:3d} {file}')

rep = input("Chose file [Q to quit] ... ")
rep = int(rep)-1

DATA = []
file = os.path.join(data_dir, text_file[rep])
with open(file, "r", encoding="utf8") as F:
    for line in F:
        line = line.strip()
        if line == "": continue
        print(repr(line))
        try:
            data = [float(x) for x in line.split()]
        except:
            continue
        DATA.append(data)
DATA = np.array(DATA)

time = DATA[:, 0]  # time in secondes
PWM  = DATA[:, 1]  # PWM in percent
z    = DATA[:, 2]  # z in meters

file = os.path.basename(file)

plt.figure()
plt.suptitle(f"File <{file}>", size=14)
plt.subplots_adjust(hspace=0.4, top=0.84)

plt.subplot(2,1,1)
plt.title("WM turbine")
plt.plot(time, PWM, "-ob", markersize=1.)
plt.ylabel("PWM [%]")
plt.ylim(0, 80)
plt.grid()

plt.subplot(2,1,2)
plt.title("Elevation z ")
plt.plot(time, z, "-og", markersize=1.)
plt.xlabel("Time [sec]")
plt.ylabel("z [m]")
ymax = 0.5 if z.max() <= 0.5 else 1.
plt.ylim(0, ymax)
plt.grid()

img_file = file.replace(".txt", ".png")
plt.savefig(os.path.join(data_dir, img_file))

plt.show()      # fait apparaître la fenêtre surgissante
