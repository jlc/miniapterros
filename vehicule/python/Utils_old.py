import os
import numpy as np
import matplotlib.pyplot as plt
from select import select

def plot_file(fileName):
    f_out = open(fileName, "r", encoding="utf8")
    data = []
    for line in f_out:
        try:
            datas = [float(x) for x in line.split()]
            data.append(datas)
        except:
            print("skiping <{}>".format(line.strip()))
            
    data = np.array(data)    
    plot_Z_PWM(data[:,0], data[:,-2], data[:,1], data[:,-1], fileName)
    return data
    
def plot_Z_PWM(T, Zvalues, PWM, Ztvalues, fileName=None):
    T, Zvalues, PWM, Ztvalues = np.array(T), np.array(Zvalues), np.array(PWM), np.array(Ztvalues)
    plt.figure("Flight 1A",  figsize = (10,6) )
    plt.subplots_adjust(left=0.12, bottom=0.11,
                        right=0.90, top=.88,
                        wspace=0.2, hspace=0.4)
    plt.subplot(211)
    plt.title("Position Z versus time")
    plt.plot(T,Zvalues,"b.-",label="Altitude(t)")
    plt.plot(T,Ztvalues,"r.-",label ="Zt(t)")
    plt.ylabel("Position $z$ [$m$]")
    plt.xlabel("Time [sec]")
    M1 = Ztvalues.max()
    M2 = Zvalues.max()
    M3 = max(M1,M2)
    plt.ylim(0,1.1*M3)
    plt.grid()  
    plt.subplot(212)
    plt.title("PWM turbines command versus time")
    plt.plot(T,PWM,"r.-",label ="PWM(t)")
    plt.ylabel("PWM [%]")
    plt.xlabel("Time [sec]")
    plt.ylim(0,100)
    plt.grid()
    if fileName is not None:
        imageName = fileName.replace(".txt",".png")
        print("saving plot in file <{}>".format(imageName))
        plt.savefig(imageName)
    plt.show()
    
        
def get_Zc(ZcMIN,ZcMAX) :
    Zc = -1
    m1 = ">>> Enter the Zc in range [{} m ; {} m ]  ?  "\
         .format(ZcMIN, ZcMAX)
    while Zc < ZcMIN or Zc > ZcMAX:
        Zc = float(input(m1))
    return Zc

def get_Long(LongMIN,LongMAX) :
    Long = -1
    m1 = ">>> Enter the duration of ramp in range [{} s ; {} s ]  ?  "\
         .format(LongMIN, LongMAX)
    while Long < LongMIN or Long > LongMAX:
        Long = float(input(m1))
    return Long

def get_pwm_eq(pwm_eqMIN,pwm_eqMAX) :
    pwm_eq = -1
    m1 = ">>> Enter the pwm_eq in range [{} % ; {} % ]  ?  "\
         .format(pwm_eqMIN, pwm_eqMAX)
    while pwm_eq < pwm_eqMIN or pwm_eq > pwm_eqMAX:
        pwm_eq = float(input(m1))
    return pwm_eq

def get_Gain() :
    Gain = -1
    m1 = ">>> Enter the Gain "
    Gain = float(input(m1))
    return Gain

def get_delayLoop(delayLoopMIN,delayLoopMAX) :
    delayLoop = -1
    m1 = ">>> Enter the delayLoop in range [{} s ; {} s ]  ?  "\
         .format(delayLoopMIN, delayLoopMAX)
    while delayLoop < delayLoopMIN or delayLoop > delayLoopMAX:
        delayLoop = float(input(m1))
    return delayLoop
    
    
def get_pwmLimit(pwmMIN, pwmMAX):
    pwmLimit = -1
    m1 = "\n*******************************************************************\n"
    m1 += "Turbine PWM values are in range [{} % ; {} % ].\n"\
          .format(pwmMIN, pwmMAX)
    m2 = ">>> Enter PWM limit in range [{} % ; {} %]  ?  "\
         .format(pwmMIN, pwmMAX)
    print(m1)
    while pwmLimit < pwmMIN or pwmLimit > pwmMAX:
        pwmLimit = int(input(m2))
    return pwmLimit

def get_pwmStep(pwmStepMIN, pwmStepMAX):
    pwmStep = -1
    m1 = ">>> Enter the PWM ramp step in range [{} % ; {} % ]  ?  "\
         .format(pwmStepMIN, pwmStepMAX)
    while pwmStep < pwmStepMIN or pwmStep > pwmStepMAX:
        pwmStep = float(input(m1))
    return pwmStep 

def get_pulseWidthLimit(pulseWidthMIN, pulseWidthMAX):
    pulseWidthLimit = 0
    m1 = "\n*******************************************************************\n"
    m1 += "Turbine STOP and MAX speed PWM values are {} and {} micro-sec.\n"\
          .format(pulseWidthMIN, pulseWidthMAX)
    m2 = ">>> Enter pulse-width to reach in range [{};{}] micro-sec ?  "\
         .format(pulseWidthMIN, pulseWidthMAX)
    print(m1)
    while pulseWidthLimit < pulseWidthMIN or pulseWidthLimit > pulseWidthMAX:
        pulseWidthLimit = int(input(m2))
    return pulseWidthLimit

def get_pulseWidthStep(pulseStepMIN, pulseStepMAX):
    pulseWidthStep = 0
    m1 = ">>> Enter the ramp step in range [{};{}] micro-sec ?  "\
         .format(pulseStepMIN, pulseStepMAX)
    while pulseWidthStep < pulseStepMIN or pulseWidthStep > pulseStepMAX:
        pulseWidthStep = int(input(m1))
    return pulseWidthStep 

def get_KeyPressed():
    '''Wait for a key pressed and returns it.'''

    key_pressed = "="
    
    if os.name == 'nt':
        import msvcrt
        key_ressed = msvcrt.getch()
    else:
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            key_pressed = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
                
    return key_pressed

if __name__ == "__main__":

    list_files = [ f for f in os.listdir() if f.endswith(".txt")]
    list_files.sort()
    for i, f in enumerate(list_files, 1):
        print("{:02d} -> {}".format(i,f))
    file_rank = int(input("File to plot ? "))    
    try:
        file_name = list_files[file_rank-1]
        print("plot of file {} :".format(file_name))
        data = plot_file(file_name)
    except:
        pass
    
