//
// test_lidar : to get data measured by the Lidar
// v1.0   2021.09.01
//

#include <Wire.h>
#include <VL53L0X.h>

// Customize lidar sensor behaviour
//#define LONG_RANGE
#define HIGH_SPEED
//#define HIGH_ACCURACY

///////////////////////////////////////////////////
///////      Pins Layout          
///////////////////////////////////////////////////

///// PWM output to drive the turbines //////
#define pinPWM_Turbine  9

///////////////////////////////////////////////////
///////      Global variables
///////////////////////////////////////////////////
bool debug = false;            

// Create the lidar Sensor
VL53L0X sensor;

void setup() 
{

  // Open serial communications and wait for port to open:
  Serial.begin(115200);   // set Serial port baud rate

  Wire.begin();

  //sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }
 
#if defined LONG_RANGE
  // lower the return signal rate limit (default is 0.25 MCPS)
  sensor.setSignalRateLimit(0.1);
  // increase laser pulse periods (defaults are 14 and 10 PCLKs)
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
#endif

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif

  //////////////////////////////////////////////////
  ////    Serial line configuration             ////
  //////////////////////////////////////////////////

  // wait for serial port to connect. Needed for native USB
   while (!Serial) {
      ; 
    }

  // Empty USB buffer to begin with a clean buffer...
  emptyUSB();
  delay(500);
  
  // Send hello to the RPi:
  Serial.println("Arduino OK"); Serial.flush();
  
  // now we can enter the loop of communication...
}

void loop() 
{ 
    float z = sensor.readRangeSingleMillimeters()
    Serial.println(z, 2);
  }
}

inline void emptyUSB()
{
  // empty USB buffer, if needed:f_ou = 
  if (Serial.available())   // Data are available on the serial line (USB)
  {
    while (Serial.available() > 0) 
    {
      Serial.read();
    }    
  }
}