//
// flight_1A : The Arduino receives PWM requests given by a PID controller 
//             programmed in Python on the RPi card (or PC laptop).
//
// v1.0   2019-12-11  Erwan CHENEAU & Julien LANGLAIS
//
// v1.1   2019/10/29 JLC
//        Modify emergency stop: normaly HIGH, fall down to LOW if button is spushed or cable destroyed ;-)
//


// The Servo library is used to drive the PWM signals
#include <Servo.h>  

#include <Wire.h>
#include <VL53L0X.h>


// Customize lidar sensor behaviour
//#define LONG_RANGE
#define HIGH_SPEED
//#define HIGH_ACCURACY

///////////////////////////////////////////////////
///////      Pins Layout          
///////////////////////////////////////////////////

///// Emergency STOP button //////
#define pinEmergencyStop 2

///// PWM output to drive the turbines //////
#define pinPWM_Turbine  9

///// PWM output to drive the actuators //////
#define pinLeftActuator  5
#define pinRightActuator 6

///// Connexion to the distance sensor //////
#define TRIGGER_PIN  12
#define ECHO_PIN     11
#define MAX_DISTANCE 200


///////////////////////////////////////////////////
///////      Global variables
///////////////////////////////////////////////////
bool debug = false;
bool emergencyStopRequired = false;

// the Servo object to drive the MiniApterros ESC:
Servo esc;                

// Create the lidar Sensor
VL53L0X sensor;

const int pulseWidthMIN = 1000;   // micro-sec ; stops the turbine
const int pulseWidthMAX = 2000;   // micro-sec ; turbine rotates at max speed
const int pulseStepMIN  = 10;     // micro-sec ; smallest pulse width step allowed
const int pulseStepMAX  = 100;    // micro-sec ; biggest pulse width step allowed
const float pwmMIN      = 0;      // % ; smallest PWM value
const float pwmMAX      = 100;    // % ; biggest PWM value
const float pwmStepMIN  = 0.1;    // % ; smallest PWM step value
const float pwmStepMAX  = 10.;    // % ; biggest PWM step value
const float pwm2musec   = (pulseWidthMAX-pulseWidthMIN)/(pwmMAX-pwmMIN); // used to convert PWM to microseconde
const float musec2pwm   = (pwmMAX-pwmMIN)/(pulseWidthMAX-pulseStepMIN);  // used to convert microseconde  to PWM

float pwmLimit;
float pwmStep;

// All PWM values in range [0-100] (in percent)
int PWM_TU,   // PWM value to drive the turbines
    PWM_LA,   // PWM value to drive Left Actuator
    PWM_RA;   // PWM value to drive Right Actuator

String data = "";   // buffer for serial line input

float getAltitude_cm()
{
  //
  // To get the MiniApterros Z altitude, measured with the lidar distance sensor.
  //
  float s=0.;
  const float N=2.;
  for(int i=0; i < N; i++){
    float z = sensor.readRangeSingleMillimeters();
    s+=z;
  }
  return s/(10*N);
}

void driveTurbines(float PWMvalue)
{
  //
  // To drive the MiniApterros turbines with the PWM output connected to the ESC of 
  // the 4 turbines.
  //
  const int pulseWidth = int(pulseWidthMIN + PWMvalue*pwm2musec);
  esc.writeMicroseconds(pulseWidth);
}

void driveLeftActuator(int PWMvalue)
{
  // ***** To be completed by the team "Arduino".... ****
  //
  // To drive the MiniApterros left actuator to rotate the turbines' plate.
  //

}

void driveRightActuator(int PWMvalue)
{
  // ***** To be completed by the team "Arduino".... ****
  //
  // To drive the MiniApterros left actuator to rotate the turbines' plate.
  //

}

void stopMiniApterros()
{
  //
  // This function is called when the emergency-stop button is pressed.
  // It should stop the turbine and reset the actuator to their initial length.
  
  driveTurbines(0);         
  driveLeftActuator(0);
  driveRightActuator(0);  
}

void emergencyStop() 
{
  stopMiniApterros();
  PWM_TU = pwmMIN;
  Serial.println("EMERGENCY-STOP required");
  emergencyStopRequired = true;
}

void readDataFromSerial(int nbChars)
{
  //
  // to read nbChars from USB line. 
  // Characters read are appended to the 'data' variable.
  //
  data = "";
  // wait for nbChars received in serial buffer:
  while (Serial.available() < nbChars)
  { ; }

  // then read all of them:
  while (Serial.available() > 0) 
    {
      int inChar = Serial.read();
      //if (inChar == int("\n")) 
      //  break;
      data += (char)inChar; 
    }
}

int decodePWM(const String & data, String & ack)
{
  //
  // To decode the request string emitted by RPi to drive the MiniApterros turbines.
  //
  // Arguments:
  //  data -> [IN] const reference to the request String
  //  ack  -> [OUT]contains the  acknowlegde string
  //
  // Returned value:
  //  0   -> OK
  // -1   -> malformed data string. No character '%' found !
  // -2   -> malformed data string. No character ':' found !
  // -3   -> PWM label unknown (must be 'PWM_TU', 'PWM_LA' or 'PWM_RA'
    
  ack = String("");   // empty the ack string
  int start = 0;      // start processing data at rank 0
  
  int percent_rank = data.indexOf("%", start);
  
  if (percent_rank == -1)
  {
    ack = "ERROR: malformed data string. No character '%' found !";
    return -1;
  }
  
  while (percent_rank != -1)
  {
    const String & bloc = data.substring(start, percent_rank);
    int colon_rank = data.indexOf(":", start);
    
    if (colon_rank == -1)
    {
      ack = "ERROR: malformed data string. No character ':' found !";
      return -2;
    }
    else
    {
      const String & pwm_label = data.substring(start, colon_rank);
      const String & pwm_value = data.substring(colon_rank+1, percent_rank);

      // Process labels:
      if (pwm_label == "PWM_TU")
      {
        PWM_TU = pwm_value.toInt();
        ack += "PMW_TU:" + pwm_value;
      }
      else if (pwm_label == "PWM_LA")
      {
        PWM_LA = pwm_value.toInt();
        ack += "%PMW_LA:" + pwm_value;
      }
      else if (pwm_label == "PWM_RA")
      {
        PWM_RA = pwm_value.toInt();
        ack += "%PMW_RA:" + pwm_value;
        ack += "%";
      }
      else 
      {
        ack = "ERROR: PWM label unknown !";
        return -3;
      }
    }
    start = percent_rank+1;
    percent_rank = data.indexOf("%", start);
  }
  return 0;
}

void setup() 
{

  // Open serial communications and wait for port to open:
  Serial.begin(115200);   // set Serial port baud rate

  //////////////////////////////////////////////////
  ////    turbine input/output configuration    ////
  //////////////////////////////////////////////////
  esc.attach(pinPWM_Turbine);     // to attach the sec to pinPWM_Turbine
  driveTurbines(0.);              // to stop the turbines

  //////////////////////////////////////////////////
  ////    Emergency stop button                 ////
  //////////////////////////////////////////////////

  // Since september 14, the PinUrgence is pulled down to zero volt using an external resistor of 10 kOhm.
  // The Emergency stop button (normally closed) applies the 5 V to the PinUrgence: when the emregnecy button
  // is pressed, or when the cable is cut, the input level on PinUrgence falls from 5 V to 0 V.
  // That's why the attachInterrupt() trigers the EmergencyStop function if the level of PinUrgence goes "LOW"!!!
 
  pinMode(pinEmergencyStop, INPUT);  

  // Configure "low level" signal on pinEmergencyStop to emit an interrupt
  // associated with function emergencyStop:
  attachInterrupt(digitalPinToInterrupt(pinEmergencyStop), emergencyStop, LOW);  

  if (digitalRead(pinEmergencyStop) == LOW)
  {
    Serial.println("EMERGENCY STOP ANOMALY : release Emergency Button, or check cables....");
    emergencyStopRequired = true;
  }

  
  //////////////////////////////////////////////////
  ////    left & right actuators configuration  ////
  //////////////////////////////////////////////////
  

  //////////////////////////////////////////////////
  ////    lidar distance sensor  configuration  ////
  //////////////////////////////////////////////////

  Wire.begin();

  sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }
 
#if defined LONG_RANGE
  // lower the return signal rate limit (default is 0.25 MCPS)
  sensor.setSignalRateLimit(0.1);
  // increase laser pulse periods (defaults are 14 and 10 PCLKs)
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
#endif
#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif

  //////////////////////////////////////////////////
  ////    Serial line configuration             ////
  //////////////////////////////////////////////////

  // wait for serial port to connect. Needed for native USB
   while (!Serial) {
      ; 
    }

  // Empty USB buffer to begin with a clean buffer...
  emptyUSB();
  delay(500);
  
  // Send hello to the RPi:
  Serial.println("Arduino OK"); Serial.flush();
  
  // now we can enter the loop of communication...
}

void loop() 
{ 
  if (emergencyStopRequired) return; // get out loop if emergency stop requied
  
  // read nbChars charcaters from the USB line.
  // present format of the RPi request : 
  // 'PWM_TU:xx%PWM_LA:yy%PWM_RA:zz%' => 30 caracters.
  
  String m="PWM_TU:xx%PWM_LA:yy%PWM_RA:zz%";
  readDataFromSerial(m.length());  

  if (data == "EMERGENCY-STOP")   // End of transmission
  {
    emergencyStopRequired = true;
    stopMiniApterros();
    PWM_TU = pwmMIN;
    while (true) {;}         // never returns
  }
  else
  {
    //
    // process received data:
    //
    String ack;
    String answer("");
    
    int ret_decodePWM = decodePWM(data, ack);     

    if (ret_decodePWM == 0)
    {
      //
      // if decodePWM was successfull : drive the actuators.
      //
      driveTurbines(PWM_TU);
      driveLeftActuator(PWM_LA);
      driveRightActuator(PWM_RA);
    }

    //
    //  1. Send acknowledge string
    //
    Serial.println(ack);

    //
    //  2. Send Z data
    //  
    String Zmessage("Z:");
    Zmessage += String(getAltitude_cm()*1.e-2, 3);
    Zmessage += ("m"); 
    Serial.println(Zmessage);    
  }
}

inline void emptyUSB()
{
  // empty USB buffer, if needed:f_ou = 
  if (Serial.available())   // Data are available on the serial line (USB)
  {
    while (Serial.available() > 0) 
    {
      Serial.read();
    }    
  }
}

void printDebug(const String & message)
{
  if (debug) {Serial.println("DEBUG "+message);Serial.flush();}
}

void printDebug(const String & label, const int & value)
{
  if (debug) {Serial.print("DEBUG "+label); Serial.println(value);Serial.flush();};
}

void printDebug(const String & mess1, const String &mess2)
{
  if (debug) {Serial.print("DEBUG "+mess1); Serial.println(mess2);Serial.flush();};
}

void printDebug(const String & mess1, const String &mess2, const String &mess3)
{
  if (debug) {Serial.print("DEBUG "+mess1); Serial.print(mess2); Serial.println(mess3);Serial.flush();};
}
