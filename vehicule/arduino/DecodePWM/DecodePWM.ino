//
// JLC created this programm.
//
// This program can be run on the ARDUINO Uno alone.
// It creates a command string following the format used for the exchange protocol
// RPi <-> Arduino.
// The command string is sent to the fucntion decodePWM to test it.
//

bool debug = false;

int PWM_TU,   // PWM value to drive the turbines    - values in range [0-100] in percent
    PWM_LA,   // PWM value to drive Left Actuator   - values in range [0-100] in percent
    PWM_RA;   // PWM value to drive Right Actuator  - values in range [0-100] in percent

int decodePWM(const String & data, String & ack)
{
  //
  // To decode the request string emitted by RPi to drive the MiniApterros turbines.
  //
  // Arguments:
  //  data -> const reference to the request String
  //  ack  -> the  acknowlegde string gieven in return
  //
  // Returned value:
  //  0   -> OK
  // -1   -> malformed data string. No character '%' found !
  // -2   -> malformed data string. No character ':' found !
  // -3   -> PWM label unknown (must be 'PWM_TU', 'PWM_LA' or 'PWM_RA'
    
  ack = String("");   // empty the ack string
  int start = 0;      // start processing data at rank 0
  
  int percent_rank = data.indexOf("%", start);
  printDebug("% rank-> ", percent_rank);
  
  if (percent_rank == -1)
  {
    Serial.println("ERROR: malformed data string. No character '%' found !");
    return -1;
  }
  
  while (percent_rank != -1)
  {
    const String & bloc = data.substring(start, percent_rank);
    int colon_rank = data.indexOf(":", start);
    printDebug(": rank-> ", colon_rank);
    
    if (colon_rank == -1)
    {
      Serial.println("ERROR: malformed data string. No character ':' found !");
      return -2;
    }
    else
    {
      const String & pwm_label = data.substring(start, colon_rank);
      const String & pwm_value = data.substring(colon_rank+1, percent_rank);
      printDebug("pwm_label: ", pwm_label);
      printDebug("pwm_value: ", pwm_value); 
      Serial.println("\n");

      // Process labels:
      if (pwm_label == "PWM_TU")
      {
        PWM_TU = pwm_value.toInt();
        ack += "PWM_TU:" + pwm_value + "%";
      }
      else if (pwm_label == "PWM_LA")
      {
        PWM_LA = pwm_value.toInt();
        ack += "PWM_LA:" + pwm_value + "%";
      }
      else if (pwm_label == "PWM_RA")
      {
        PWM_RA = pwm_value.toInt();
        ack += "PWM_RA:" + pwm_value + "%";
      }
      else 
      {
        Serial.println("ERROR: PWM label unknown !");
       return -3;
      }
    }
    start = percent_rank+1;
    percent_rank = data.indexOf("%", start);
    printDebug("% rank-> ", percent_rank);
  }
  return 0;
}

void setup() 
{
  Serial.begin(9600);

  // Choose the string to desnd:
  //String D("PWM_TU:00%PWM_LA:00%PWM_RA:00%");
  String D("PWM_TU:55%PWM_LA:00%PWM_RA:00%");
  
  debug = true;
  String ack;
  
  int ret_code = decodePWM(D, ack);
  
  if (ret_code != 0)
  {
    Serial.print("Error in decodePWM, return code is "); Serial.println(ret_code);
  }
  else
  {
    // build the acknowledge string
    Serial.println("Acknowledge string: ");
    Serial.println(ack);
    Serial.println(D);
    if (ack.equals(D))
    {
      Serial.println("Acknlowledge string is OK!");
    }
    else
    {
      Serial.println("Acknlowledge string is not OK!");
    }
  }
}

void loop() 
{
  // nothing to do ;-)
  delay(1e9);
}

void printDebug(const String & message)
{
  if (debug) {Serial.println("DEBUG "+message);};
}

void printDebug(const String & label, const int & value)
{
  if (debug) {Serial.print("DEBUG "+label); Serial.println(value);};
}

void printDebug(const String & mess1, const String &mess2)
{
  if (debug) {Serial.print("DEBUG "+mess1); Serial.println(mess2);};
}
