import numpy as np
import matplotlib.pyplot as plt



def extract_data(path):
    f = open(path,'r',encoding='utf8')
    T, PWMs, zvalues, zdotvalues = [], [], [], []

    for ligne in f.readlines():
        if ligne.startswith("time") or ligne.startswith("Flight"): continue
        t, PWM, z, zdot = ligne.split('\t')
        T.append(float(t))
        PWMs.append(float(PWM))
        zvalues.append(float(z))
        zdotvalues.append(float(zdot))
    return np.array([np.array(T), np.array(PWMs), np.array(zvalues), np.array(zdotvalues)])


def plot_data(Data, path):
    plt.figure("Données de vol",[18,6])
    plt.subplot(2,2,1)
    plt.plot(Data[0], Data[1], '.-' )
    plt.title("PWM = f(t)")
    plt.grid(True)

    plt.subplot(2,2,2)
    plt.plot(Data[0], Data[2], '.-' )
    plt.title("z = f(t)")
    plt.grid(True)

    plt.subplot(2,2,3)
    plt.plot(Data[0], Data[3], '.-' )
    plt.title("zdot = f(t)")
    plt.grid(True)
    filename = "/home/aro/Documents/Vol_RN_21_06_2/" + path.split('/')[-1][:-4]
    
    plt.savefig("{}.png".format(filename))
    plt.show()



if __name__ == "__main__" :

    data_1 = "/home/aro/Documents/Vol_RN_21_06_2/flight_NN-2021_05_28_17_10.txt"
    #plot_data(extract_data(data_1), data_1)
    data_2 = "/home/aro/Documents/Vol_RN_21_06_2/flight_NN-2021_05_28_17_15.txt"
    #plot_data(extract_data(data_2), data_2)
    data_3 = "/home/aro/Documents/Vol_RN_21_06_2/flight_NN-2021_05_28_17_34.txt"
    plot_data(extract_data(data_3), data_3)

