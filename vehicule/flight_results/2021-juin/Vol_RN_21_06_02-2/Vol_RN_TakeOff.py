import os, sys
from time import sleep, time, strftime
from serial import Serial
import numpy as np
import matplotlib.pyplot as plt

stop_message = "EMERGENCY-STOP resquested from ARDUINO, Tchao !"

def plot_Z_PWM(T, Zvalues, PWM, fileName=None):
    T, Zvalues, PWM = np.array(T), np.array(Zvalues), np.array(PWM)
    plt.figure("Flight 1A",  figsize = (10,6) )
    plt.subplots_adjust(left=0.12, bottom=0.11,
                        right=0.90, top=.88,
                        wspace=0.2, hspace=0.4)
    plt.subplot(211)
    plt.title("Position Z versus time")
    plt.plot(T, Zvalues, "b.-", label="Altitude(t)")
    plt.ylabel("Position $z$ [$m$]")
    plt.xlabel("Time [sec]")
    plt.ylim(0,1.1*Zvalues.max())
    plt.grid()  
    plt.subplot(212)
    plt.title("PWM turbines command versus time")
    plt.plot(T, PWM, "r.-", label ="PWM(t)")
    plt.ylabel("PWM [%]")
    plt.xlabel("Time [sec]")
    plt.ylim(0,100)
    plt.grid()
    if fileName is not None:
        imageName = fileName.replace(".txt",".png")
        print("saving plot in file <{}>".format(imageName))
        plt.savefig(imageName)
    plt.show()

def readArduino(serialPort, Zorigin):
    data_m = serialPort.readline()          # read data measurement
    data_m = data_m.decode().strip()        # clean data
    #print("RPi read-2: <{}>\n".format(data_m))# debug only
    if data_m.startswith("ERROR") \
       or data_m.startswith("DEBUG") \
       or data_m.startswith("INFO"):
        return "passMessage"
    elif "EMERGENCY-STOP" in data_m:
        print(stop_message)
        return "breakMessage"
    else:        
        Z = float(data_m.split(':')[1][:-1])
        Z -= Zorigin # Zorigin is the initial offset
        if Z <= 0: Z = 0        
        return Z

def sendArduino(serialPort, PWM_TU, PWM_LA, PWM_RA):
    # send PWM_T... to Arduino
    req = "PWM_TU:{:02d}%PWM_LA:{:02d}%PWM_RA:{:02d}%"
    req = str(req.format(PWM_TU,PWM_LA, PWM_RA))
    #print('RPi write : <{}>'.format(req))
    data_w = bytes(req, encoding="ascii")
    serialPort.write(data_w)
    serialPort.flush()
    
    # 2 steps read : 1/ acknowledge, 2/ then measured data
    # 1/ read Arduino acknowledge:
    data_a = serialPort.readline()          # read data acknowledge
    data_a = data_a.decode().strip()        # clean data
    #print("RPi read-1: <{}>".format(data_a))# for debug only
    if "EMERGENCY-STOP" in data_a:
        print(stop_message)
        return "breakMessage"
    return "passMessage"

def convertPWM(action): #convertion de l'action du NN en PWM
    return action * 100

def computeZdot(Z, previousZ):
    return (Z - previousZ)/dt

if __name__ == "__main__":

	### Section Main

    #
    # Les listes, pour le tracé des courbes
    #
	Zvalues    = [0] # list of Z values
	Zdotvalues = [0] # list of Z dot values
	PWM        = [0] # list of PWM values
	T          = [0] # list of time values

	#
	# Fichier de sortie ASCII
	#
	fileName  = 'flight_NN-'+strftime("%Y_%m_%d_%H_%M")+'.txt'
	fileOut   = open(fileName, "w")

	data_format = "{:8.2f}\t{:8.2f}\t{:4.2f}\t{:4.2f}\n"    
	header      = "time [s]|PWM|Z [m]|Zdot [m/s]\n"
	fileOut.write(header)

	#
	# Initialisation Arduino
	#
	listUSBports = ["/dev/ttyACM0", "/dev/ttyACM1",
		            "COM1","COM2", "COM3", "COM4","COM9","COM10","COM13"]
	serialPort = None
	for port in listUSBports:
	    try:
		    print(port)
		    serialPort = Serial(port, baudrate=115200, timeout=None)
		    break
	    except:
    		continue
	print(serialPort)    

	# Open serial serial if needed:
	sleep(0.5)
	if not serialPort.is_open :
	    serialPort.open()
	sleep(0.5)

	# wait for Arduino ready:
	print("Waiting for ARDUINO ... ", end="")
	data = b""

	while "Arduino OK" not in data.decode().strip():
	    data = serialPort.readline()
	    print(data.decode().strip())
	    if "ERROR" in data.decode().strip():
    		sys.exit()
	print("Found <Arduino OK> : good !", flush=True)


	## Variables
	Z          = 0     # current altitude 
	previous_Z = 0
	Zorigin    = 0.131 # offset (hauteur du capteur Lidar par rapport au sol)
	dt         = 0.1
	PWM_LA, PWM_RA = 0, 0

	# instancier un PPO et charger le fichier des poids du RN
	from stable_baselines3 import PPO as agent
	model_takeoff = agent.load("BasicMiniApterrosEnv_PPO_21-06-01_23-40-48.zip")
	model_landing = agent.load("BasicMiniApterrosEnv_PPO_21-06-02_14-03-44.zip")
	print("models loaded")
	t0  = time()
	
	for pwm in (30, 40, 50):
		ret = sendArduino(serialPort, pwm, PWM_LA, PWM_RA)
		Z = readArduino(serialPort, Zorigin)
		Zvalues.append(Z)
		PWM.append(pwm)
		Zdotvalues.append(computeZdot(Z,previous_Z))
		T.append(time()-t0)
		previous_Z = Z
		sleep(0.5)

	pwm = 60
	ret = sendArduino(serialPort, pwm, PWM_LA, PWM_RA)
	sleep(0.5)
	landing = False
	while True: ## Boucle principale
		t00 = time()
		ret = readArduino(serialPort, Zorigin) #Get Z from arduino
		if ret == "breakMessage": 
			break
		elif ret == "passMessage":
			pass
		else :
			Z = ret

		if abs(Z-previous_Z) > 0.6:
			Z = previous_Z

		Zdot = computeZdot(Z, previous_Z)

		# Compute NN action from [Z, Zdot] state:
		if not landing:
			if T[-1] > 10 and Z>=1:#landing stage
				model = model_landing
				landing = True
			else:#takeoff
				model = model_takeoff
				
		action,_ = model.predict(np.array((Z, Zdot)), deterministic = True)
		action = float(action[0])
		action = (action+1)/2.
		
		pwm = convertPWM(action) #Convert action into PWM
		
		PWM_TU = round(pwm)
		PWM_LA, PWM_RA = 0, 0
		
		ret = sendArduino(serialPort, PWM_TU, PWM_LA, PWM_RA) #Send PWM to arduino
		
		if ret == "breakMessage":
		    break
		elif ret == "passMessage":
		    pass

		previous_Z = Z
		PWM.append(PWM_TU)
		Zvalues.append(Z)
		Zdotvalues.append(Zdot)
		T.append(time()-t0)
		t_restant = dt-(time()-t00)
		if t_restant>=0: sleep(t_restant)
	    

	print("\n     >>> End of transmission")

	for t, pwm, z, zdot in zip(T, PWM, Zvalues, Zdotvalues):
	    fileOut.write(data_format.format(t, pwm, z, zdot))


	# clean serial port before closing
	serialPort.flushOutput()
	sleep(0.5)

	# close serial
	serialPort.close()

	# close data file:
	t1 = time()-t0
	fileOut.write("Flight duration: {:.1f} s\n".format(round(t1,1)))
	fileOut.close()

	#plot_Z_PWM(T, Zvalues, PWM, Ztvalues)
