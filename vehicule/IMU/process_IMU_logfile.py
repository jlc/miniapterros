
#  Copyright (c) 2003-2021 Xsens Technologies B.V. or subsidiaries worldwide.
#  All rights reserved.
#  
#  Redistribution and use in source and binary forms, with or without modification,
#  are permitted provided that the following conditions are met:
#  
#  1.   Redistributions of source code must retain the above copyright notice,
#       this list of conditions, and the following disclaimer.
#  
#  2.   Redistributions in binary form must reproduce the above copyright notice,
#       this list of conditions, and the following disclaimer in the documentation
#       and/or other materials provided with the distribution.
#  
#  3.   Neither the names of the copyright holders nor the names of their contributors
#       may be used to endorse or promote products derived from this software without
#       specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
#  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
#  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
#  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS 
#  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES 
#  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE 
#  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
#  

import xsensdeviceapi as xda
import time, os, sys
import numpy as np
from threading import Lock

# Added JLC 2022-05-11: pass the acquisation as an argument
import argparse
# end JLC

class XdaCallback(xda.XsCallback):
    def __init__(self):
        xda.XsCallback.__init__(self)
        self.m_progress = 0
        self.m_lock = Lock()

    def progress(self):
        return self.m_progress

    def onProgressUpdated(self, dev, current, total, identifier):
        self.m_lock.acquire()
        self.m_progress = current
        self.m_lock.release()


if __name__ == '__main__':

    # Added JLC 2022-05-11: add arguments to the command line
    parser = argparse.ArgumentParser()
    parser.add_argument("--log_file_name", type=str, default="")
    args = parser.parse_args()
    logfileName = vars(args)["log_file_name"]
    # end JLC
        
    print("Creating XsControl object...")
    control = xda.XsControl_construct()
    assert(control != 0)

    xdaVersion = xda.XsVersion()
    xda.xdaVersion(xdaVersion)
    print("Using XDA version %s" % xdaVersion.toXsString())
    
    if logfileName == "":
        while True:
            log_dir = input("Enter the path of the directory to scan for log files [or Q for quit]... ")
            if log_dir.upper() == "Q": sys.exit()
            
            if not os.path.isdir(log_dir):
                print(f"Sorry <{log_dir}> in not a valid path, please retry...")
            else:
                break
        list_logfiles = [ f for f in os.listdir(log_dir) if f.endswith('mtb')]
        list_logfiles.sort()
        for i,file in enumerate(list_logfiles, 1):
            print(f"{i}\t{file}")
        choice = input("Which log file number to process [Q for quit]... ")
        if choice.upper() == "Q": 
            sys.exit(0)
         
        logfileName = list_logfiles[int(choice)-1]
        logfileName = os.path.join(log_dir, logfileName)

    print(f"Opening log file <{logfileName}>...")

    try:
        if not control.openLogFile(logfileName):
            raise RuntimeError("Failed to open log file. Aborting.")

        deviceIdArray = control.mainDeviceIds()
        for i in range(deviceIdArray.size()):
            if deviceIdArray[i].isMti() or deviceIdArray[i].isMtig():
                mtDevice = deviceIdArray[i]
                break

        if not mtDevice:
            raise RuntimeError("No MTi device found. Aborting.")

        # Get the device object
        device = control.device(mtDevice)
        assert(device != 0)

        print("Device: %s, with ID: %s found in file" % (device.productCode(), device.deviceId().toXsString()))

        # Create and attach callback handler to device
        callback = XdaCallback()
        device.addCallbackHandler(callback)

        # By default XDA does not retain data for reading it back.
        # By enabling this option XDA keeps the buffered data in a cache so it can be accessed 
        # through XsDevice::getDataPacketByIndex or XsDevice::takeFirstDataPacketInQueue
        device.setOptions(xda.XSO_RetainBufferedData, xda.XSO_None);

        # Load the log file and wait until it is loaded
        # Wait for logfile to be fully loaded, there are three ways to do this:
        # - callback: Demonstrated here, which has loading progress information
        # - waitForLoadLogFileDone: Blocking function, returning when file is loaded
        # - isLoadLogFileInProgress: Query function, used to query the device if the loading is done
        #
        # The callback option is used here.

        print("Loading the file...")
        device.loadLogFile()
        while callback.progress() != 100:
            time.sleep(0)
        print("File is fully loaded")


        # Get total number of samples
        packetCount = device.getDataPacketCount()

        # Export the data
        print("Exporting the data...")
        s = ''
        index = 0
        FullData = []
        while index < packetCount:
            # Retrieve a packet
            packet = device.getDataPacketByIndex(index)
            data = []
            if packet.containsCalibratedData():
                acc = packet.calibratedAcceleration()
                s += "Acc X:{:5.2f}, Y:{:5.2f}, Z:{:5.2f}".format(*acc)
                data += acc.tolist()
                
                gyr = packet.calibratedGyroscopeData()
                s += "|Gyr X:{:5.2f}, Y:{:5.2f}, Z:{:5.2f}".format(*gyr)
                data += gyr.tolist()
                
                mag = packet.calibratedMagneticField()
                s += "|Mag X:{:5.2f}, Y:{:5.2f}, Z:{:5.2f}".format(*mag)
                data += mag.tolist()
                
            if packet.containsOrientation():
                quaternion = packet.orientationQuaternion()
                s += "q0:{:5.2f}, q1:{:5.2f}, q2:{:5.2f}, q3:{:5.2f}".format(*quaternion)
                data += quaternion.tolist()
                
                euler = packet.orientationEuler()
                s += f"|Roll:{euler.x():5.2f}, Pitch:{euler.y():5.2f}, Yaw:{euler.z():5.2f}"
                data += [euler.x(), euler.y(), euler.z()]

            if packet.containsLatitudeLongitude():
                latlon = packet.latitudeLongitude()
                s += " |Lat: %7.2f" % latlon[0] + ", Lon: %7.2f " % latlon[1]

            if packet.containsAltitude():
                s += " |Alt: %7.2f " % packet.altitude()

            if packet.containsVelocity():
                vel = packet.velocity(xda.XDI_CoordSysEnu)
                s += " |E: %7.2f" % vel[0] + ", N: %7.2f" % vel[1] + ", U: %7.2f " % vel[2]
                
            s += "\n"
            if index >= 1:
                data = np.array(data)
                FullData.append(data)
            
            print(f"\r{index:6d}", end="")
            index += 1
        
        Data = np.array(FullData)
        print(f"\nShape of Data: {Data.shape}, {Data.dtype}")
        
        exportFileName = "exportfile.txt"
        with open(exportFileName, "w") as outfile:
            outfile.write(s)
        print("File is exported to: %s" % exportFileName)

        print("Removing callback handler...")
        device.removeCallbackHandler(callback)

        print("Closing XsControl object...")
        control.close()

    except RuntimeError as error:
        print(error)
    except:
        print("An unknown fatal error has occured. Aborting.")
    else:
        print("Successful exit.")
        
        
    # Plot Acc, Gyr and Mag
    #
    
    import matplotlib.pyplot as plt
    from matplotlib import ticker

    fig, axes = plt.subplots(4,3, sharex='col')
    plt.subplots_adjust(left=0.07, right=0.9, hspace=0.3, top=0.9, bottom=0.065)
    fig.set_size_inches((15,9))
    fig.suptitle(f"MTi-30 data from file <{logfileName}>", fontsize=16)
    
    # Plot Accqq
    #
    Acc, Gyr, Mag, Quat, Eul = Data[:,0:3], Data[:,3:6], Data[:,6:9], Data[:,9:13], Data[:,13:16]
    sampling_rate = 100
    T_sec = np.arange(Data.shape[0])/sampling_rate
    
    labels = [["Acc X", "Acc Y", "Acc Z"], 
              ["Gyr X", "Gyr Y", "Gir Z"], 
              ["Mag X", "Mag Y", "Mag Z"], 
              ["Roll", "Pitch", "Yaw"]]
    
    colors = ['r', 'g', 'b']
    ymin, ymax = Acc.min(), Acc.max()
    for i, (axe, acc, label, color) in enumerate(zip(axes[0], Acc.T, labels[0], colors)):
        axe.plot(T_sec, acc,'-'+color, markersize=0.2, linewidth=1.5)
        axe.set_ylim(ymin-.1*abs(ymin), ymax+.1*abs(ymax))
        axe.set_title(label)
        axe.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:6.1f}"))
        if i == 0: axe.set_ylabel("Accelaration [m/s^2]")
        axe.grid(True)
        
    ymin, ymax = Gyr.min(), Gyr.max()    
    for i, (axe, gyr, label, color) in enumerate(zip(axes[1], Gyr.T, labels[1], colors)):
        axe.plot(T_sec, gyr,'-'+color, markersize=0.2, linewidth=1.5)
        axe.set_ylim(ymin-.1*abs(ymin), ymax+.1*abs(ymax))
        axe.set_title(label)
        axe.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:6.1f}"))
        if i == 0: axe.set_ylabel("Rotation speed [rd/s]")
        if abs(gyr.max()) < 1: axe.set_ylim(-1,1)
        axe.grid(True)
    
    ymin, ymax = Mag.min(), Mag.max()
    for i, (axe, mag, label, color) in enumerate(zip(axes[2], Mag.T, labels[2], colors)):
        axe.plot(T_sec, mag,'-'+color, markersize=0.2, linewidth=1.5)
        axe.set_ylim(ymin-.1*abs(ymin), ymax+.1*abs(ymax))
        axe.set_title(label)
        axe.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:6.1f}"))
        if i == 0: axe.set_ylabel("Magnetic field\n[arbitrary unit]")
        axe.grid(True)

    ymax =  20 if Eul.max() <  10 else Eul.max()
    ymin = -20 if Eul.min() > -10 else Eul.min()
    for i, (axe, eul, label) in enumerate(zip(axes[3], Eul.T, labels[3])):
        axe.plot(T_sec, eul,'-m', markersize=0.2, linewidth=1.5)
        axe.set_ylim(ymin-.1*abs(ymin), ymax+.1*abs(ymax))
        axe.set_title(label+f" [min: {eul.min():.1f}, max: {eul.max():.1f}, mean: {eul.mean():.1f}]")
        axe.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:6.1f}"))
        if i == 0: axe.set_ylabel("Angle [degrees]")
        axe.grid(True)
        axe.set_xlabel("Time [s]")

    #
    plt.savefig(logfileName.replace('.mtb','.png'))
    plt.show()
    
    
    '''
    a.u. (arbitrary units; normalized to earth field strength at the location the MFM is performed)
    '''
    
