\documentclass[11pt,french,fleqno]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose,tmargin=1.8cm,bmargin=1.6cm,lmargin=1.8cm,rmargin=1.8cm}
\usepackage{fancyhdr}
\pagestyle{fancy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{babel}
\usepackage{psfrag}
\usepackage{tabls}
\usepackage{color}
\usepackage{colortbl}
\usepackage{amsfonts, amssymb, amsmath}
\usepackage{graphics}
\usepackage{lastpage}
\usepackage{lmodern}
\usepackage[procnames]{listings} 
\usepackage{listingsutf8}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage[tikz]{bclogo}
\usepackage[tworuled, vlined, french]{algorithm2e} % présentation d'algorithmes

\input{colors}

\setlength{\parindent}{0pt}

\lhead{MiniApterros}
\chead{Saison 3}
\rhead{\small v1.1 -- 2019/11/30}
\lfoot{}
\cfoot{}
\rfoot{\thepage~/~\pageref{LastPage}}

\renewcommand{\footrulewidth}{0.3pt}
\renewcommand{\headrulewidth}{0.3pt}

\begin{document}
\begin{center}
{\Large\bf Entraînement d'un réseau de Neurones DDPG \\pour le pilotage de mini-apterros}
\end{center}

\section{Simulateur simple du véhicule pour \texttt{Open/AI gym}}
\subsection{Modèle simplifié}
On utilise un simulateur simple pour l'entraînement DDPG dans l'environnement {\em Open/AI gym} : le véhicule est simplement modélisé comme une masse ponctuelle $m$ soumise à 2 forces antagonistes:
\begin{itemize}
\item le poids du véhicule $\overrightarrow{p} = m \overrightarrow{g}$, appliqué au centre de gravité du véhicule ;
\item la poussée des turbines, modélisée par un vecteur $\overrightarrow{T}(t)$ appliqué au centre de gravité du véhicule.
\end{itemize}

L'équation qui régit le mouvement du véhicule s'écrit : 
\[
m \overrightarrow{\gamma}_z = \overrightarrow{T}(t) - m \overrightarrow{g}
\]
En projettant sur l'axe $z$, on obtient l'EDO (Équation Différentielle Ordinaire) du deuxième ordre :
\begin{equation}
  \ddot{z}(t) = \frac{T(t)}{m}-g \label{eq1}
\end{equation}

\subsection{Résolution numérique}
L'évolution de la poussée $\overrightarrow{T}(t)$ en fonction du temps étant complexe, on n'a pas de solution analytique de l'équation (\ref{eq1}).
Une solution numérique peut être obtenue par la méthode de Runge-Kuttta, à condition de mettre l'équation du mouvement sous la forme d'un problème de Cauchy (EDO d'ordre 1)
dont la formulation générale est :
$$
\left\{
\begin{array}{rcl}
    y'(t) & = & f(y(t), t)\\
    y(0)  & = & y_0
\end{array}
\right.
$$
où $f$ est la fonction de Cauchy, qui donne l'expression de la dérivée de la fonction cherchée $y$ en fonction uniquement de $y$ et de $t$.

\smallskip
Dans ce cas, la méthode de Runge-kutta s'écrit :

\fcolorbox{Chocolate}{LightGray}{\parbox{.95\textwidth}{\small
$\bullet$ Le temps $\mathtt{t}$ est discrétisé en $\mathtt{t_i = i\,dt}$, où $\mathtt{dt}$ est le pas de discrétisation temporelle.\\
$\bullet$ Les valeurs de $\mathtt{y}$ sont approchées aux temps discrétisée $\mathtt{t_i}$ par : $\mathtt{y_i \approx y(t_i)}$ et $\mathtt{y(0) = y_0}$.\\
$\bullet$ Connaissant $\mathtt{y_i}$, $\mathtt{y(t_{i+1})}$ est approchée par : $\mathtt{y_{i+1} \gets y_i +(k1 + 2*k2 + 2*k3 + k4)*dt/6}$\\
\phantom{$\bullet$} avec : $\mathtt{k1 \gets f(y_i, t_i)}$\\
\phantom{$\bullet$ avec :} $\mathtt{k2 \gets f(y_i + k1*dt/2, t_i)}$\\
\phantom{$\bullet$ avec :} $\mathtt{k3 \gets f(y_i + k2*dt/2, t_i)}$\\
\phantom{$\bullet$ avec :} $\mathtt{k4 \gets f(y_i + k3*dt,   t_i)}$\\
}}


\medskip
Pour ramener l'EDO \ref{eq1} à un problème de Cauchy, on définit le vecteur $Z = \left[ \begin{array}{c} z(t) \\ \dot{z}(t) \end{array} \right]$.

On peut alors reformuler (1) :
\begin{equation}
\dot{Z}(t) = F(Z(t), t) \label{eq2}
\end{equation}
où $F$ est la fonction vectorielle définie par :
\[
Z = \left[\begin{array}{c} z(t) \\ \dot{z}(t) \end{array} \right], t
\longmapsto F(Z, t) = 
\left[
\begin{array}{l}
    \dot{z}(t)\\
    T(t)/m-g
\end{array}
\right]
\]

Le calcul des valeurs de $Z$ et $\dot{Z}$ en appliquant Runge-Kutta au problème de Cauchy formulé en (\ref{eq2}) donne alors :

\fcolorbox{Chocolate}{LightGray}{\parbox{.95\textwidth}{\small
    $\bullet$ $\mathtt{t_i}$ est le temps discrétisé : $\mathtt{t_i = i\,dt}$\\
    $\bullet$ $\mathtt{T_i}$ est la valeur de la poussée calculée au temps $\mathtt{t_i}$\\
    $\bullet$ $\mathtt{Z_i}$ est le vecteur $[\mathtt{z_i}, \dot{\mathtt{z_i}}]$ avec $\mathtt{Z_0} = [\mathtt{z_0}, \dot{\mathtt{z_0}}]$ \\
    $\bullet$ La fonction $\mathtt{F(Z_i, t_i)}$ renvoie le vecteur $[\dot{\mathtt{z_i}}, \mathtt{T_i / m - g}]$ \\
    $\bullet$ Connaissant $\mathtt{Z_i}$, $\mathtt{Z(t_{i+1})}$ est approchée par : $\mathtt{Z_{i+1} \gets Z_i +(K1 + 2*K2 + 2*K3 + K4)*dt/6}$\\
    \phantom{$\bullet$} avec : $\mathtt{K1 \gets F(Z_i, t_i)}$\\
    \phantom{$\bullet$ avec :} $\mathtt{K2 \gets F(Z_i + K1*dt/2, t_i)}$\\
    \phantom{$\bullet$ avec :} $\mathtt{K3 \gets F(Z_i + K2*dt/2, t_i)}$\\
    \phantom{$\bullet$ avec :} $\mathtt{K4 \gets F(Z_i + K3*dt,   t_i)}$\\
}}

\end{document}
