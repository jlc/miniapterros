Github
- always commit before pulling then pull 
- before pushing : 
    - first commit your last changes
    - then pull t
    - (eventually, if any) merge conflicts, test if it is still working and commit again. 
    - Now you can push !

Code
- Follow naming and typing regulations (pep 8 by default in pycharm), to ensures your code is clear. 
I would recommend :
    
    - do not write long lines.
    - imports are always at the start of files
    - global constants in majuscule (ex: MAX_STEPS)  
    - do not use spaces in any names (including filenames, directory names,...)
    - separate names in variables, function, files with underscore to improve readibility (ex : my_variable = 5, def my_function():)
    - class names are in camel case (MyClass)
    - specify input and output of every method:
        
        def myfunction(var1:int, var2:str) -> str
    
    This allows your IDE to automatically show availables methods, documentations and suggestions
    
    - use meaningfull names for function and variables.
        Do not do : a = 10, b = "toto" but age = 10, name = "toto"    
    - one python file by class. "my_class.py" should only contain "MyClass"
        
- do not use long methods, parse it in sub-method. It allows better readibility,
easier modifications and reusabilities of part of the code.
- if you see multiple time nearly identical code, you should make a function for it. It will avoid
copy-paste errors, and when you will have to modify it, you will only need to modify it a single time !
- do not use "magic constant" everywhere in the code. Put all your constants either as parameters in the yaml 
file if you think you may need to modify them for experiments, or in a dedicated python file
like i did in src/run/constants.py. So you will know where they are and how to modify it.
- Do not put too many things in a notebook. Avoid having multiple long processings in a single notebook. Create a
notebook by processing instead, and sort your notebooks in different directories.
 